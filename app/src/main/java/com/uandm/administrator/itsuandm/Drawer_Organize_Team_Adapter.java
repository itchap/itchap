package com.uandm.administrator.itsuandm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Created by u on 2016-12-27.
 */
public class Drawer_Organize_Team_Adapter extends RecyclerView.Adapter<Drawer_Organize_Team_Adapter.ViewHolder> {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList
    private ArrayList<Drawer_Organize_Team_Item> listViewItemList;
    private Context context;
    int type = -1;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String selected_nick;
    private Util_Dialog dialog;


    String myJSON = null;
    JSONArray team_list = null;
    private static final String TAG_RESULTS = "result";
    private static final String TAG_NICK = "nick";
    private static final String TAG_PHOTO_MAIN = "photo_main";

    private Util_ImageView team_2, team_3, team_4;
    private ImageView team_21, team_31, team_41;

    String result2 = null;
    private Boolean addispossible = false;


    public Drawer_Organize_Team_Adapter(ArrayList<Drawer_Organize_Team_Item> item, Context context) {
        this.context = context;
        this.listViewItemList = item;

    }

    public Drawer_Organize_Team_Adapter(ArrayList<Drawer_Organize_Team_Item> item, Context context, Util_ImageView team_2, Util_ImageView team_3, Util_ImageView team_4, ImageView bb, ImageView cc, ImageView dd) {
        this.context = context;
        this.listViewItemList = item;
        this.team_2 = team_2;
        this.team_3 = team_3;
        this.team_4 = team_4;
        this.team_21 = bb;
        this.team_31 = cc;
        this.team_41 = dd;

    }

    @Override
    public int getItemViewType(int position) {
        if (listViewItemList.get(position).getType() == 1) {
            this.type = 1;
            return 1;
        } else
            this.type = 2;
        return 2;
    }

    /**
     * 레이아웃을 만들어서 Holer에 저장
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public Drawer_Organize_Team_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        pref = context.getSharedPreferences("login", 0);
        editor = pref.edit();

        View view;
        if (viewType == 1) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.d_organize_item, viewGroup, false);
            return new Drawer_Organize_Team_Adapter.ViewHolder(view, 1);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.d_organize_no_item, viewGroup, false);
            return new Drawer_Organize_Team_Adapter.ViewHolder(view, 2);
        }


    }


    @Override
    public void onBindViewHolder(final Drawer_Organize_Team_Adapter.ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);

        Glide.with(context).load(listViewItemList.get(position).getUri()).asBitmap().into(viewHolder.iv_1);
        viewHolder.tv_nick.setText(listViewItemList.get(position).getNick());

        viewHolder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_nick = listViewItemList.get(position).getNick();
                //String result = getData("http://175.126.38.139/php/getdata_my_status.php");
                //Log.e("result",""+result);

                // if(result.equals("NO_TEAM_LIST")){
                //  Log.e("NO_TEAM_LIST","");
                // //   addispossible=true;
                // }else if (result.equals("0")){
                //     addispossible=true;
                //  }else if(result.equals("10")){
                //       addispossible=true;
                //   }else if(result.equals("2")){
                //       Log.e("ERROR","");
                //  }else if(result.matches(".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*")){
                //   int aa= result.length();
                //   String sub = result.substring (aa-1);
                //     if(sub.equals("1")){
                //        dialog = new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_lastpage), "팀 생성 중입니다.", "확인", "dismiss");
                //        dialog.show();
                //    }else{
                ///        dialog = new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_lastpage), "팀 수락/거절을 먼저 해주세요", "확인", "dismiss");
                //      dialog.show();
                //   }
                //  }//else {
                /// //     Log.e("ERROR","");
                //  }

               getData("http://175.126.38.139/php/check_team_status.php");
            }
        });


        //친구 리스트 지우기
        viewHolder.del.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                selected_nick = listViewItemList.get(position).getNick();
                String result = getData("http://175.126.38.139/php/del_team_list.php");
                if (result.equals("good")) {
                    listViewItemList.remove(position);
                    notifyDataSetChanged();
                } else {
                    dialog = new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_lastpage), "삭제할 수 없습니다.", "확인", "dismiss");
                    dialog.show();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }

    /**
     * 뷰 재활용을 위한 viewHolder`
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        protected Util_ImageView iv_1;
        protected TextView tv_nick;
        protected TextView add, del;

        public ViewHolder(View itemView, int type) {
            super(itemView);
            iv_1 = (Util_ImageView) itemView.findViewById(R.id.profile);
            iv_1.setType(11);
            tv_nick = (TextView) itemView.findViewById(R.id.nickname);
            add = (TextView) itemView.findViewById(R.id.add);
            del = (TextView) itemView.findViewById(R.id.del);
        }

    }


    public String getData(String url) {
        class GetDataJSON extends AsyncTask<String, Void, String> {
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";

            @Override
            protected String doInBackground(String... params) {
                DataOutputStream os;
                String uri = params[0];
                BufferedReader bufferedReader = null;
                StringBuilder sb = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Connection", "Keep-Alive");
                    con.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    os = new DataOutputStream(con.getOutputStream());

                    String my_nick = pref.getString("nick", "");
                    os.writeBytes(twoHyphens + boundary + lineEnd);
                    os.writeBytes("Content-Disposition: form-data; name=\"my_nick\"" + lineEnd);
                    os.writeBytes(lineEnd);
                    os.write(my_nick.getBytes());
                    os.writeBytes(lineEnd);

                    os.writeBytes(twoHyphens + boundary + lineEnd);
                    os.writeBytes("Content-Disposition: form-data; name=\"nick\"" + lineEnd);
                    os.writeBytes(lineEnd);
                    os.write(selected_nick.getBytes());
                    os.writeBytes(lineEnd);

                    os.flush();
                    os.close();

                    BufferedReader rd = null;
                    rd = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                    sb = new StringBuilder();
                    String json;
                    while ((json = rd.readLine()) != null) {
                        sb.append(json + "\n");
                    }
                    return sb.toString().trim();

                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }


            @Override
            protected void onPostExecute(String result) {
            }
        }


        String result2 = null;
        GetDataJSON g = new GetDataJSON();
        try {
            result2 = g.execute(url).get();
            Log.e("ERRORㄴㄴㄴㄴ", "" + result2);
            if (result2.equals("ALREADY") ) {
                dialog = new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_lastpage), "이미 다른 팀의 소속인 회원입니다.", "확인", "dismiss");
                dialog.show();
            } else if (result2.equals("WAITING")) {
                dialog = new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_lastpage), "수락 대기 중인 회원입니다.", "확인", "dismiss");
                dialog.show();
            } else {
                add_team(result2);
            }
        } catch (Exception e) {
            result2 = null;
        }
        return result2;
    }

    protected void add_team(String aa) {
        try {
            JSONObject jsonObj = new JSONObject(aa);
            team_list = jsonObj.getJSONArray(TAG_RESULTS);
            JSONObject c = team_list.getJSONObject(0);
            Log.i("@@@@@@@@@@@@@@@@", "" + team_list);
            String uri = c.getString("photo_main");
            Uri str_to_uri = Uri.parse(uri);
            if (team_2.getVisibility() == INVISIBLE) {
                team_2.setVisibility(VISIBLE);
                team_21.setVisibility(VISIBLE);
                Glide.with(context).load(str_to_uri).asBitmap().into(team_2);
                Glide.with(context).load("http://175.126.38.139/img/icon_team_waiting.png").asBitmap().into(team_21);
                Drawer_Organize_Team.temp_team_list[0]=c.getString("nick");
                Drawer_Organize_Team.temp_team_list_photo[0]=c.getString("photo_main");
            } else if (team_3.getVisibility() == INVISIBLE) {
                if(Drawer_Organize_Team.temp_team_list[0].equals(c.getString("nick"))){
                    dialog = new Util_Dialog(context,context.getResources().getDrawable(R.drawable.icon_lastpage),"이미 추가 되었습니다","확인","dismiss");
                    dialog.show();
                }else {
                    team_3.setVisibility(VISIBLE);
                    team_31.setVisibility(VISIBLE);
                    Glide.with(context).load(str_to_uri).asBitmap().into(team_3);
                    Glide.with(context).load("http://175.126.38.139/img/icon_team_waiting.png").asBitmap().into(team_31);
                    Drawer_Organize_Team.temp_team_list[1]=c.getString("nick");
                    Drawer_Organize_Team.temp_team_list_photo[1]=c.getString("photo_main");
                }
            } else if (team_4.getVisibility() == INVISIBLE) {
                if(Drawer_Organize_Team.temp_team_list[0].equals(c.getString("nick")) || Drawer_Organize_Team.temp_team_list[1].equals(c.getString("nick"))){
                    dialog = new Util_Dialog(context,context.getResources().getDrawable(R.drawable.icon_lastpage),"이미 추가 되었습니다","확인","dismiss");
                    dialog.show();
                }else{
                    team_4.setVisibility(VISIBLE);
                    team_41.setVisibility(VISIBLE);
                    Glide.with(context).load(str_to_uri).asBitmap().into(team_4);
                    Glide.with(context).load("http://175.126.38.139/img/icon_team_waiting.png").asBitmap().into(team_41);
                    Drawer_Organize_Team.temp_team_list[2]=c.getString("nick");
                    Drawer_Organize_Team.temp_team_list_photo[2]=c.getString("photo_main");
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}

