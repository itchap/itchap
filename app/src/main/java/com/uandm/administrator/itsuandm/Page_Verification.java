package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.*;
import com.tsengvn.typekit.TypekitContextWrapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class Page_Verification extends AppCompatActivity {

    private JSONArray jsonArray;
    private Struct_Join struct_join;
    private WebView webView;
    private static String URL = "http://175.126.38.139/php/";
    private WebViewClient_ webViewClient_ ;
    private String name, gender, phone, nation, age, error, M_age;

    private static final String ERROR_CODE_0 = "fail to loading module";
    private static final String ERROR_CODE_1 = "irregular access time termination";
    private static final String ERROR_CODE_2 = "irregular access ip mismatch";
    private Intent send,intent;
    private int type;
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_verification);


        Intent intent_ = getIntent();
        struct_join = (Struct_Join)intent_.getSerializableExtra("tt");
        type = intent_.getIntExtra("type", -1);

        if(type == 0){
            send = new Intent("VERICAION");
            send.putExtra("from","veri");
        }else if(type == 1 ){
            intent = new Intent(Page_Verification.this, Page_ID_PW2.class);

        }

        Findid();
        webView.setWebViewClient(webViewClient_);
        webView.loadUrl("http://175.126.38.139/php/join_test.php");
        //webView.loadUrl("http://175.126.38.139/php/veri_test.php");
        webView.addJavascriptInterface(webViewClient_, "getdata");

        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                new AlertDialog.Builder(Page_Verification.this)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok,
                                new AlertDialog.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        result.confirm();
                                    }
                                })
                        .setCancelable(false)
                        .create()
                        .show();
                return true;
            }
        });




    }

    void Findid(){
        webViewClient_ = new WebViewClient_();
        webView = (WebView)findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        //자바 스크립트 enable


    }

    private class WebViewClient_ extends WebViewClient{
        private final Handler handler = new Handler();

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            Log.i("WebView", "History: " + url );
            super.doUpdateVisitedHistory(view, url, isReload);

        }

        @JavascriptInterface
        public void setdata(final String data) { // must be final
            handler.post(new Runnable() {
                public void run() {

                    try {
                        JSONObject jsonObj = new JSONObject(data);
                        jsonArray = jsonObj.getJSONArray("data");
                        JSONObject c = jsonArray.getJSONObject(0);

                        Log.e("verification", "" + jsonArray + " "+ jsonArray.length()+ " "+ c.length());

                        if(c.length() == 1 ){
                            error = c.getString("error");
                            if(error.equals("0")){
                                Log.e("ERROR_CODE_0", ERROR_CODE_0);
                                send.putExtra("result", ERROR_CODE_0);
                                intent.putExtra("result", ERROR_CODE_0);
                            }else if(error.equals("1")){
                                Log.e("ERROR_CODE_1", ERROR_CODE_1);
                                send.putExtra("result", ERROR_CODE_1);
                                intent.putExtra("result", ERROR_CODE_1);
                            }else if(error.equals("2")){
                                Log.e("ERROR_CODE_2", ERROR_CODE_2);
                                send.putExtra("result", ERROR_CODE_2);
                                intent.putExtra("result", ERROR_CODE_2);
                            }

                            if(type == 0){
                                sendBroadcast(send);
                            }else if(type == 1){
                                startActivity(intent);

                            }
                        }else{
                            gender = c.getString("gender");
                            nation = c.getString("nation");
                            name = c.getString("name");
                            phone = c.getString("phone");
                            age = c.getString("age");

                            if(type == 0) {
                                struct_join.setGender(Integer.parseInt(gender));
                                struct_join.setName(name);
                                struct_join.setNation(Integer.parseInt(nation));
                                struct_join.setPhone(phone);
                                struct_join.setAge(Integer.parseInt(age));
                                send.putExtra("result", "success");
                                send.putExtra("all_struct", struct_join);
                                send.putExtra("from","vreir");
                                sendBroadcast(send);
                                finish();
                            }else if(type == 1){
                                intent.putExtra("result","success");
                                intent.putExtra("name",name);
                                intent.putExtra("phone",phone);
                                startActivity(intent);
                                finish();
                            }


                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                }
            });
        }
    }
}
