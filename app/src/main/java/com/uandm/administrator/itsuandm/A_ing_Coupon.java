package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.tsengvn.typekit.TypekitContextWrapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class A_ing_Coupon extends AppCompatActivity {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private RelativeLayout back;
    private RecyclerView recycler;
    private A_ing_Coupon_Adapter adapter;
    private ArrayList<A_ing_Coupon_Item> item;

    private LinearLayout no_item;
    private String json;
    JSONArray jsonArray = null;
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_ing_coupon);
        pref = getSharedPreferences("login", 0);
        editor = pref.edit();

        back = (RelativeLayout) findViewById(R.id.back_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        item = new ArrayList<>();
        no_item = (LinearLayout) findViewById(R.id.no_item);
        recycler = (RecyclerView) findViewById(R.id.recyclerView_coupon);
        try {
            json = new Util_PHP(A_ing_Coupon.this).execute(pref.getString("nick", ""), "ing_coupon", "http://175.126.38.139/php/coupon.php").get();
        } catch (Exception e) {
            e.getStackTrace();
        }

        if (json.contains("no_list")) {
        } else if (json.contains("error") || json.contains("over_seven")) {
        } else {
            no_item.setVisibility(View.GONE);
            recycler.setVisibility(View.VISIBLE);
            rendering(json);
        }

        adapter = new A_ing_Coupon_Adapter(item, A_ing_Coupon.this);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(adapter);
    }




    protected void rendering(String json) {
        try {
            JSONObject jsonObj2 = new JSONObject(json);
            jsonArray = jsonObj2.getJSONArray("result");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject c = jsonArray.getJSONObject(i);
                item.add(new A_ing_Coupon_Item(A_ing_Coupon.this, c.getString("store_name"), c.getString("enroll_date"),  c.getString("type_text"),  c.getString("discount"),  c.getString("discount_sub"), c.getString("coupon_no"),Integer.parseInt(c.getString("type")), c.getString("sp_date")));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}