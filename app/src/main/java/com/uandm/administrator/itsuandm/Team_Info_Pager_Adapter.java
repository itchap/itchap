package com.uandm.administrator.itsuandm;

/**
 * Created by young on 2016-11-21.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;


public class Team_Info_Pager_Adapter extends PagerAdapter {

    private Context mContext;
    private String[] mResources;
    private RequestManager requestManager;

    public Team_Info_Pager_Adapter(Context context, String[] img) {
        Log.e("내가 호출된다","");
        this.mContext = context;
        this.mResources = img;
        this.requestManager = Glide.with(context);

    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.l_team_info_item_item, container, false);
        

        ImageView imageView = (ImageView) itemView.findViewById(R.id.team_info_img_item);
        Log.e("내가 호출된다","");
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)imageView.getLayoutParams();
        DisplayMetrics dm = mContext.getApplicationContext().getResources().getDisplayMetrics();

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        params.width = width;
        params.height = width;

        imageView.setLayoutParams(params);

       requestManager.load(mResources[position]).asBitmap().into(imageView);

        container.addView(itemView);

    return itemView;
}
//
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }
}