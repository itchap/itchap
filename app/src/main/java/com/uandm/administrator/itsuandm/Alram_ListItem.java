package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by young on 2016-11-22.
 */

public class Alram_ListItem {

    Context context;
    Drawable icon;
    String img[] = new String[4];

    String time;
    String cmt;
    String sender_nick;
    String alram_type;
    String alram_status;


    String alram_id;

    int img_type;

    int selected_time, current_time;

    Alram_ListItem(Context context, String img[], Drawable icon, String time, String cmt, String sender_nick, String alram_status, String alram_type, int img_type,String alram_id) {
        this.context = context;
        this.icon = icon;
        this.cmt = cmt;
        this.img_type = img_type;
        this.alram_type = alram_type;
        this.alram_id = alram_id;

        this.sender_nick = sender_nick;
        this.alram_status = alram_status;

        for (int i = 0; i < img.length; i++) {
            if (img[i] != null)
                this.img[i] = img[i];
            else
                this.img[i] = null;
        }
        this.time = convert_time(time);
    }

    public String convert_time(String time) {
        Calendar calendar = Calendar.getInstance();
        String today = time.substring(0, 8);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
        Date currentTime = new Date();
        String dTime = formatter.format(currentTime);

        String aa = time.substring(4, 6);
        String bb = time.substring(6, 8);
        String cc = time.substring(8, 10);
        String dd = time.substring(10, 12);

        selected_time = ((Integer.parseInt(cc) * 60) + (Integer.parseInt(dd)));
        current_time = ((calendar.get(Calendar.HOUR_OF_DAY) * 60) + calendar.get(Calendar.MINUTE));
        if (!today.equals(dTime)) {
            time = aa + "월 " + bb + "일 " + cc + ":" + dd;
        } else if (current_time - selected_time <= 0) {
            time = "지금";
        } else if (current_time - selected_time <= 59 && current_time - selected_time >= 1) {
            time = current_time - selected_time + "분 전";
        } else if (current_time - selected_time >= 60) {
            time = ((current_time - selected_time) / 60) + "시간 전";
        } else {
            Log.e("123", "");
        }

        return time;
    }

    public String getTime() {
        return time;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getImg_type() {
        return img_type;
    }

    public void setImg_type(int img_type) {
        this.img_type = img_type;
    }


    public String getAlram_id() {
        return alram_id;
    }

    public void setAlram_id(String alram_id) {
        this.alram_id = alram_id;
    }
    public String getAlram_type() {
        return alram_type;
    }

    public void setAlram_type(String alram_type) {
        this.alram_type = alram_type;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String[] getImg() {
        return img;
    }

    public void setImg(String[] img) {
        this.img = img;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCmt() {
        return cmt;
    }

    public void setCmt(String cmt) {
        this.cmt = cmt;
    }

    public String getSender_nick() {
        return sender_nick;
    }

    public void setSender_nick(String sender_nick) {
        this.sender_nick = sender_nick;
    }

    public String getAlram_status() {
        return alram_status;
    }

    public void setAlram_status(String alram_status) {
        this.alram_status = alram_status;
    }

    public int getSelected_time() {
        return selected_time;
    }

    public void setSelected_time(int selected_time) {
        this.selected_time = selected_time;
    }

    public int getCurrent_time() {
        return current_time;
    }

    public void setCurrent_time(int current_time) {
        this.current_time = current_time;
    }


}

