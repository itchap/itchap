package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.bumptech.glide.util.Util;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;
import com.tsengvn.typekit.TypekitContextWrapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Team_Info extends AppCompatActivity {

    private ViewPager viewpager;

    private LinearLayout indicator;
    private int dotsCount;
    private ImageView[] dots;


    private RecyclerViewPager recyclerView;
    RelativeLayout d_content;
    private Util_Dialog dialog;
    private  Dialog cancel_dialog;
    private EditText edit_txt;
    private TextView dialog_cancel, btn_submit_text;
    private  LinearLayout dialog_confirm;
    private RelativeLayout btn_back, btn_store, btn_submit;

    private ArrayList<Team_info_ListItem> item;
    private Team_Info_Recycler_Adapter adapter;

    private Intent intent;
    private int team_count;
    private String team_owner;
    private String date_time;
    private String json;
    private String img[], like[];
    private String nick, age, job, height, weight, store_id;
    private JSONArray jsonArray;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private boolean init = false;
    private String get_intent_type;

    private String json_like, json_cancel;
    private Util_Dialog custom_dial;

    private LinearLayout cancel_main;
    private TextView cancel_cancel, cancel_submit;
    private EditText cancel_edit;
    private String cancel_json,json_like_com;

    private String type;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_team_info);

        pref = this.getSharedPreferences("login", 0);
        editor = pref.edit();

        init = pref.getBoolean("intro", false);

        if (!init) {
            Intent intent1 = new Intent(Team_Info.this, Team_Info_Intro.class);
            startActivity(intent1);
        }

        intent = getIntent();

        team_count = intent.getExtras().getInt("teammate_count");
        team_owner = intent.getExtras().getString("owner_nick");
        date_time = intent.getExtras().getString("date_time");
        get_intent_type = intent.getExtras().getString("get_intent_type");
        Log.e("$$$$$", "" + get_intent_type);
        Log.e("$$$$$", "" + team_owner);
        Log.e("$$$$$", "" + date_time);
        Log.e("$$$$$", "" + team_count);
        findid();
        if (get_intent_type.equals("main")) {
            //매칭카드
            type="0";
        } else if (get_intent_type.equals("status_cancel")) {
            //보낸미팅
            type="1";
            btn_submit_text.setText("신청취소");
        } else if (get_intent_type.equals("status_complete")) {
            //받은미팅
            type="2";
            btn_submit_text.setText("신청수락");
        } else if (get_intent_type.equals("complete")) {
            //완료된미팅
            type=intent.getExtras().getString("store_id");
            Log.e("store_id",type);
            btn_submit_text.setText("미팅취소");

        }
        /* IF 팀원이 3명이면
         팀원 수많큼 item.add;
         각 item.add 는 1명의 아이템리스트 추가
         */
        item = new ArrayList<>();
        try {
            Log.e("store_id",type);
            json = new Util_PHP(Team_Info.this).execute(pref.getString("nick", ""),team_owner, Integer.toString(team_count), date_time,type ,"getdata_team_info", "http://175.126.38.139/php/getdata_team_info.php").get();

        } catch (Exception e) {
            e.printStackTrace();
        }

        parse(json);


        adapter = new Team_Info_Recycler_Adapter(item, this);
        LinearLayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layout);
        recyclerView.setAdapter(adapter);


    }

    void hide_keyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit_txt.getWindowToken(), 0);

    }


    private void parse(String data) {
        try {
            JSONObject jsonObj = new JSONObject(data);
            jsonArray = jsonObj.getJSONArray("result");
            Log.e("team_info", "" + jsonArray + "  " + jsonArray.length());

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject c = jsonArray.getJSONObject(i);
                nick = c.getString("nick");
                age = c.getString("age");
                job = c.getString("job");
                height = c.getString("height");
                weight = c.getString("weight");

                like = new String[3];
                like[0] = c.getString("like1");
                like[1] = c.getString("like2");
                like[2] = c.getString("like3");

                img = new String[3];
                img[0] = c.getString("photo1");
                img[1] = c.getString("photo2");
                img[2] = c.getString("photo3");
//                String uri = c.getString("photo_main");
//                String uri2 = c.getString("photo_2");
////                String uri3 = c.getString("photo_3");
//                Uri str_to_uri = Uri.parse(img[0]);
//                Uri str_to_uri2 = Uri.parse(img[1]);
//                Uri str_to_uri3 = Uri.parse(img[2]);
//                Uri[] mImageResources = {str_to_uri, str_to_uri2, str_to_uri3};
//                mAdapter = new Pager_Enroll_Store_Adapter(getApplicationContext(), mImageResources);
//                intro_images.setAdapter(mAdapter);
//                intro_images.setCurrentItem(0);
//                intro_images.setOnPageChangeListener(this);
//                setUiPageViewController();
                item.add(new Team_info_ListItem(Team_Info.this, nick, age, job, height, weight, like, img, team_count));
                store_id = c.getString("store_id");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void findid() {
        btn_back = (RelativeLayout) findViewById(R.id.team_info_back_btn);
        btn_back.setOnClickListener(listener);
        btn_store = (RelativeLayout) findViewById(R.id.team_store_info_layout);
        btn_store.setOnClickListener(listener);
        btn_submit = (RelativeLayout) findViewById(R.id.team_submit_layout);
        btn_submit_text = (TextView) findViewById(R.id.matching_btn_enroll);
        btn_submit.setOnClickListener(listener);
        recyclerView = (RecyclerViewPager) findViewById(R.id.team_info_recycler);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.team_info_back_btn:
                    finish();
                    break;
                case R.id.team_store_info_layout:
                    intent = new Intent(Team_Info.this, Enroll_Store_Detail.class);
                    intent.putExtra("id", store_id);
                    intent.putExtra("date_time", date_time);
                    intent.putExtra("type", "1");
                    startActivity(intent);
                    break;
                case R.id.team_submit_layout:
                    Log.e("버튼타입", "" + get_intent_type);
                    //메인에서@@
                    if (get_intent_type.equals("main")) {
                        Log.e("버튼타입", "" + get_intent_type);
                        try {
                            json_like = new Util_PHP(Team_Info.this).execute(pref.getString("nick", ""), team_owner, Integer.toString(team_count), date_time, "like_check", "http://175.126.38.139/php/like_check0.php").get();
                            if (json_like.equals("already_like")) {
                                custom_dial = new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "이미 미팅신청을 했습니다.", "확인", "dismiss");
                                custom_dial.show();
                            } else if (json_like.equals("no_team")) {
                                custom_dial = new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "팀을 아직 만들지 않으셨네요 \n 팀을 먼저 만들어 주세요", "확인", "dismiss");
                                custom_dial.show();
                            } else if (json_like.equals("different_team_no")) {
                                custom_dial = new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "팀 수가 달라 신청이 불가능 합니다.", "확인", "dismiss");
                                custom_dial.show();
                            } else if (json_like.equals("error")) {
                                custom_dial = new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "error", "확인", "to_main");
                                custom_dial.show();
                            } else if (json_like.equals("credit_is_scarce")) {
                                custom_dial = new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "코인이 부족합니다.", "취소", "코인 충전하러가기", "to_credit");
                                custom_dial.show();
                            } else if (json_like.contains("success")) {
                                final Dialog dialog = new Dialog(Team_Info.this);

                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.l_team_info_dialog);

                                dialog_cancel = (TextView) dialog.findViewById(R.id.btn_1);
                                dialog_confirm = (LinearLayout) dialog.findViewById(R.id.btn_2);
                                edit_txt = (EditText) dialog.findViewById(R.id.submit_edit);

                                d_content = (RelativeLayout) dialog.findViewById(R.id.submit_content);
                                d_content.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        hide_keyboard();
                                    }
                                });
                                dialog_cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog_confirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (edit_txt.getText().toString().equals("") || edit_txt.getText().toString() == null) {
                                            edit_txt.setHint("한마디를 입력해주세요.");
                                        } else {
                                            try {
                                                json_like = new Util_PHP(Team_Info.this).execute(pref.getString("nick", ""), team_owner, Integer.toString(team_count), date_time, edit_txt.getText().toString(), "like_check1", "http://175.126.38.139/php/like_check.php").get();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            if (json_like.contains("success")) {
                                                new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "매칭 신청이 완료되었습니다.", "확인", "dismiss").show();
                                                dialog.dismiss();

                                            } else {
                                                new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "매칭신청에 실패하였습니다. \n 잠시후 다시 시도해주세요", "확인", "to_main").show();
                                                dialog.dismiss();
                                            }
                                        }

                                    }
                                });
                                dialog.show();
                            } else {
                                Log.e("aaa######", "" + "aaa######");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //Ststus에서
                    } else if (get_intent_type.equals("status_cancel")) {
                        try {
                            json_cancel = new Util_PHP(Team_Info.this).execute(pref.getString("nick", ""), team_owner, date_time, Integer.toString(team_count), "status_cancel", "http://175.126.38.139/php/like_cancel.php").get();
                            if (json_cancel.equals("success")) {
                                new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "보낸미팅이 취소되었습니다.", "확인", "to_back").show();
                                dialog.put_data=Integer.toString(team_count);
                                //finish();
                                dialog.dismiss();
                            } else if (json_cancel.equals("ing")) {
                                new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "이미 진행중인 미팅입니다. \n 진행현황에서 취소할 수 있습니다", "확인", "to_ing").show();
                                dialog.dismiss();
                            } else {
                                new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "실패하였습니다. 잠시후 다시 시도해주세요 ", "확인", "to_main").show();
                                dialog.dismiss();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (get_intent_type.equals("status_complete")) {
                        Log.e("json_like",""+json_like_com);
                        try {
                            json_like_com = new Util_PHP(Team_Info.this).execute(pref.getString("nick", ""), team_owner, date_time, Integer.toString(team_count), "status_complete", "http://175.126.38.139/php/like_complete.php").get();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.e("json_like2",""+json_like_com);
                        if (json_like_com.equals("already_matched_1")) {
                                dialog = new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "해당 날에 이미 다른 미팅이 성사 되었습니다.", "확인", "dismiss");
                                dialog.show();
                            } else if (json_like_com.equals("already_matched_2")) {
                                dialog = new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "해당 날에 상대방이 다른 미팅이 성사 되었습니다.", "확인", "dismiss");
                                dialog.show();
                            } else if (json_like_com.contains("success")) {
                                dialog = new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "미팅이 성사되었습니다", "확인", "to_back");
                                dialog.show();
                            } else if (json_like_com.equals("I_am_not_team_owner")) {
                                dialog = new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "팀장만 미팅 수락을 할 수있습니다.", "확인", "dismiss");
                                dialog.show();
                            } else if (json_like_com.contains("error_team_cnt")) {
                                dialog = new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "실패하였습니다. 잠시후 다시 시도해주세요 ", "확인", "to_main");
                                dialog.show();
                            } else {
                                dialog = new Util_Dialog(Team_Info.this, getResources().getDrawable(R.drawable.icon_lastpage), "실패하였습니당. 잠시후 다시 시도해주세요 ", "확인", "to_main");
                                dialog.show();
                            }

                    } else if(get_intent_type.equals("complete")) {
                        cancel_dialog = new Dialog(Team_Info.this);
                        cancel_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        cancel_dialog.setContentView(R.layout.l_ing_dialog_cancel);
                        cancel_dialog.setCancelable(false);
                        cancel_dialog.show();

                        cancel_main = (LinearLayout) cancel_dialog.findViewById(R.id.cancel_main);
                        cancel_edit = (EditText) cancel_dialog.findViewById(R.id.cancel_edit);
                        cancel_cancel = (TextView) cancel_dialog.findViewById(R.id.cancel_cancel);
                        cancel_submit = (TextView) cancel_dialog.findViewById(R.id.cancel_submit);

                        cancel_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                cancel_dialog.dismiss();
                            }
                        });

                        cancel_submit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (cancel_edit.getText().toString().equals("") || cancel_edit.getText().toString() == null) {
                                    dialog = new Util_Dialog(Team_Info.this, Team_Info.this.getResources().getDrawable(R.drawable.icon_lastpage), "내용을 입력해주세요", "확인", "dismiss");
                                    dialog.show();
                                } else {
                                    try {
                                        cancel_json = new Util_PHP(Team_Info.this).execute(pref.getString("nick", ""), team_owner, date_time, Integer.toString(team_count), cancel_edit.getText().toString(), "com_cancel", "http://175.126.38.139/php/like_complete_cancel.php").get();
                                        if (cancel_json.contains("success")) {
                                            dialog = new Util_Dialog(Team_Info.this, Team_Info.this.getResources().getDrawable(R.drawable.icon_check), "미팅이 취소되었습니다.", "확인", "to_ing");
                                            dialog.show();
                                            cancel_dialog.dismiss();
                                        } else if (cancel_json.contains("error")) {
                                            dialog = new Util_Dialog(Team_Info.this, Team_Info.this.getResources().getDrawable(R.drawable.icon_check), "미팅 취소에 실패했습니다. \n 다시 시도해주세요", "확인", "to_main");
                                            dialog.show();
                                            cancel_dialog.dismiss();
                                        } else if (cancel_json.equals("only_team_owner")) {
                                            dialog = new Util_Dialog(Team_Info.this, Team_Info.this.getResources().getDrawable(R.drawable.icon_check), "팀장만 취소할 수 있습니다.", "확인", "dismiss");
                                            dialog.show();
                                            cancel_dialog.dismiss();
                                        } else if (cancel_json.equals("time")) {
                                            dialog = new Util_Dialog(Team_Info.this, Team_Info.this.getResources().getDrawable(R.drawable.icon_check), "미팅시작 30분전에는 취소가 불가능 합니다.", "확인", "dismiss");
                                            dialog.show();
                                            cancel_dialog.dismiss();
                                        }
                                    } catch (Exception e) {
                                        cancel_dialog.dismiss();
                                    }
                                }

                            }
                        });
                    }
                    break;
            }

        }
    };


}