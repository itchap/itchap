package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;

import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

import me.leolin.shortcutbadger.ShortcutBadger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by u on 2017-01-17.
 */


public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private static final String TAG = "FirebaseMsgService";
    private String type = null;
    private String type2 = null;
    private String noti = null;
    private String time = null;
    private String img = null;
    private String cmp_id = null;
    private String nick = null;
    private String json;
    private String push_y_n = "0";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    public static int bagde_count = 0;
    public static int bagde_count_chat = 0;
    public static int bagde_count_total = bagde_count + bagde_count_chat;

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        pref = this.getSharedPreferences("login", 0);
        editor = pref.edit();

        noti = remoteMessage.getData().get("message");
        type = remoteMessage.getData().get("type");
        type2 = remoteMessage.getData().get("type2");
        time = remoteMessage.getData().get("time");
        img = remoteMessage.getData().get("img");
        nick = remoteMessage.getData().get("nick");
        cmp_id = remoteMessage.getData().get("cmp_id");


        if (type.equals("session")) {
            Log.e("session","");
        }else if(type.equals("chatting")) {
            Log.e("chatting","");
        } else {
            //푸시 오는 순간 가장먼저 푸시(소리+노티)를 받는지 받지않는지 체크
            try {
                json = getData("http://175.126.38.139/php/get_push_y_n.php");
                Log.e("push_y_n", "" + json);
                if (json.equals("0")) {
                    push_y_n = "0";
                } else {
                    push_y_n = "1";
                }
            } catch (Exception e) {
                e.getStackTrace();
            }

        }

        //푸시O
        if (type.equals("alram")) {
            sendNotification_for_alram(noti);

            //푸시x
        } else if (type.equals("chatting")) {
            sendNotification(noti, type, img);

            //푸시O
        } else if (type.equals("session")) {
            sendNotification(noti, type, null);

            //푸시O
        } else if (type.equals("open_chat")) {
            sendNotification(noti, type, null);

        } else if(type.equals("")||type==null) {
            sendNotification_console(noti);
        }
    }

    private void sendNotification(String messageBody, String type, String img) {
        if (type.equals("chatting")) {
            Intent send = new Intent("CHATTING");
            send.putExtra("result", "success");
            send.putExtra("img", img);
            send.putExtra("msg", messageBody);
            send.putExtra("from_nick", nick);
            send.putExtra("sendtime", time);
            send.putExtra("cmp_id", cmp_id);
            sendBroadcast(send);

            //뱃지설정
            bagde_count_chat++;
            bagde_count_total += 1;
            ShortcutBadger.applyCount(this, bagde_count_total);

        } else if (type.equals("session")) {

            Intent send = new Intent("session");
            send.putExtra("result", "logout");
            send.putExtra("msg", messageBody);
            sendBroadcast(send);

            pref = getSharedPreferences("login", Activity.MODE_PRIVATE);
            editor = pref.edit();

            FirebaseMessagingService.bagde_count_chat=0;
            FirebaseMessagingService.bagde_count_chat=0;
            FirebaseMessagingService.bagde_count_total=0;
            editor.remove("id");
            editor.remove("pw");
            editor.putBoolean("autologin", false);
            editor.remove("intro");
            editor.commit();

//            Intent intent = new Intent(this, Page_Splash.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.putExtra("logout", true);
//            editor.putBoolean("autologin", false);
//            editor.commit();

//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.push_icon)
                    .setContentTitle("거기서 만나")
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);
//                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());


        } else if (type.equals("open_chat")) {

            if (push_y_n.equals("0")) {
                Intent intent = new Intent(this, Alram.class);
//                Intent intent = new Intent(this, Page_Main.class);
                intent.putExtra("from", "push");
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.push_icon)
                        .setContentTitle("거기서 만나")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
            }


            bagde_count++;
            bagde_count_total += 1;
            ShortcutBadger.applyCount(this, bagde_count_total);
        }

    }


    private void sendNotification_for_alram(String messageBody) {
        //0이면 푸시(노티)를 받는다.
        if (push_y_n.equals("0")) {
            if (messageBody.contains("모두 수락")) {

            } else {
                Intent intent = new Intent(this, Alram.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                Intent intent = new Intent(this, Alram.class);
//                intent.putExtra("from","push");
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.push_icon)
                        .setContentTitle("거기서 만나")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
            }
        } else {

        }
        Intent send = new Intent("make_team");
        send.putExtra("result", "success");
        send.putExtra("msg", messageBody);
        sendBroadcast(send);

        bagde_count++;
        bagde_count_total += 1;
        ShortcutBadger.applyCount(this, bagde_count_total);

    }

    private void sendNotification_console(String messageBody) {
        //0이면 푸시(노티)를 받는다.
        if (push_y_n.equals("0")) {

            Intent intent = new Intent(this, Page_Main.class);
            intent.putExtra("from", "pupushsh");
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.push_icon)
                    .setContentTitle("거기서 만나")
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }

        bagde_count++;
        bagde_count_total += 1;
        ShortcutBadger.applyCount(this, bagde_count_total);

    }

    public String getData(String url) {
        class GetDataJSON extends AsyncTask<String, Void, String> {
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";

            @Override
            protected String doInBackground(String... params) {
                DataOutputStream os;
                String uri = params[0];
                BufferedReader bufferedReader = null;
                StringBuilder sb = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Connection", "Keep-Alive");
                    con.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    os = new DataOutputStream(con.getOutputStream());

                    String nick = pref.getString("nick", "");
                    os.writeBytes(twoHyphens + boundary + lineEnd);
                    os.writeBytes("Content-Disposition: form-data; name=\"nick\"" + lineEnd);
                    os.writeBytes(lineEnd);
                    os.write(nick.getBytes());
                    os.writeBytes(lineEnd);

                    os.flush();
                    os.close();

                    BufferedReader rd = null;
                    rd = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                    sb = new StringBuilder();
                    String json;
                    while ((json = rd.readLine()) != null) {
                        sb.append(json + "\n");
                    }
                    Log.e("fire", sb.toString().trim());
                    return sb.toString().trim();
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }


            @Override
            protected void onPostExecute(String result) {
            }
        }
        String result2 = null;
        GetDataJSON g = new GetDataJSON();
        try {
            result2 = g.execute(url).get();
        } catch (Exception e) {
        }
        return result2;
    }
}



