package com.uandm.administrator.itsuandm;

import android.content.*;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;
import com.nhn.android.maps.NMapView;
import com.tsengvn.typekit.TypekitContextWrapper;

import me.leolin.shortcutbadger.ShortcutBadger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class A_ing extends AppCompatActivity {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private RequestManager requestManager;
    private RelativeLayout back;
    private TextView coupon;
    private RecyclerView recycler;
    private A_ing_Adapter adapter;
    private ArrayList<A_ing_Item> item;

    private LinearLayout no_item;
    private String json;
    private String img[];

    JSONArray jsonArray = null;
    private BroadcastReceiver receiver_chatting;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver_chatting);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_ing);
        FirebaseMessagingService.bagde_count_chat = 0;

        pref = getSharedPreferences("login", 0);
        editor = pref.edit();

        back = (RelativeLayout) findViewById(R.id.back_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        coupon = (TextView) findViewById(R.id.coupon);

        coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(A_ing.this, A_ing_Coupon.class);
                startActivity(intent1);
            }
        });

        item = new ArrayList<>();

        no_item = (LinearLayout) findViewById(R.id.no_item);
        recycler = (RecyclerView) findViewById(R.id.recyclerview_ing);
        try {
            json = new Util_PHP(A_ing.this).execute(pref.getString("nick", ""), "ing_rendering", "http://175.126.38.139/php/ing_rendering.php").get();
        } catch (Exception e) {
            e.getStackTrace();
        }

        if (json.contains("no_list")) {
        } else if (json.contains("error") || json.contains("over_seven")) {
        } else {
            no_item.setVisibility(View.GONE);
            recycler.setVisibility(View.VISIBLE);
            rendering(json);
        }

        requestManager = Glide.with(A_ing.this);
        adapter = new A_ing_Adapter(item, A_ing.this, requestManager);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(adapter);

        IntentFilter filter_chatting = new IntentFilter();
        filter_chatting.addAction("CHATTING");
        receiver_chatting = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String sendT = intent.getStringExtra("sendtime");
                Log.e("chat result", "" + " " + sendT);
                String cmp_id = intent.getStringExtra("cmp_id");
                Log.e("cmp_id", "" + cmp_id);
                Log.e("item.size", "" + item.size());
                for (int i = 0; i < item.size(); i++) {
                    if (item.get(i).getCmp_id() == Integer.parseInt(cmp_id)) {
                        int aa = Integer.parseInt(item.get(i).getAl_cnt()) + 1;
                        if (!item.get(i).getAl_cnt().equals("0")) {
                            item.get(i).setAl_cnt(Integer.toString(aa));
                        }
                        Log.e("same", "" + aa);
                    }

                }
                adapter.notifyDataSetChanged();
            }
        };

        registerReceiver(receiver_chatting, filter_chatting);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }


    protected void rendering(String json) {
        try {
            JSONObject jsonObj2 = new JSONObject(json);
            jsonArray = jsonObj2.getJSONArray("result");

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject c = jsonArray.getJSONObject(i);

                img = new String[Integer.parseInt(c.getString("img_cnt"))];

                switch (Integer.parseInt(c.getString("img_cnt"))) {
                    case 1:
                        img[0] = c.getString("img1");
                        break;
                    case 2:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        break;
                    case 3:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        img[2] = c.getString("img3");
                        break;
                    case 4:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        img[2] = c.getString("img3");
                        img[3] = c.getString("img4");
                        break;
                }
                //FirebaseMessagingService.bagde_count_chat += Integer.parseInt(c.getString("al_cnt"));
                item.add(new A_ing_Item(A_ing.this, img, c.getString("title"), Integer.parseInt(c.getString("img_cnt")), Integer.parseInt(c.getString("cmp_id")), c.getString("ad"), c.getDouble("ad_loc_1"), c.getDouble(
                        "ad_loc_2"), c.getString("tel"), c.getString("name"), c.getString("date"), c.getString("time"), c.getString("full_date_time"), c.getString("team_owner"), c.getString("store_id"), c.getString("al_cnt")));

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}