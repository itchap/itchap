





package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.*;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Page_Login extends AppCompatActivity {
    private TextView btn_join, btn_idpw;
    static TextView btn_login;
    private ImageView logo;
    private EditText edit_id, edit_pw;
    private RelativeLayout content;
    private RelativeLayout tmp;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private String id, pw;
    private Util_Dialog custom_dial;
    private Handler handler = new Handler();
    private boolean is_logout;
    private boolean setid, setpw;
    private String token;
    private String session;
    private String result_login = null, result_nick;

    ///
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("onstart","dd");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("oncreate","dd");
        setContentView(R.layout.l_login_form);

        Intent logout_intent = getIntent();


        pref = getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();

        is_logout = logout_intent.getBooleanExtra("logout", false);
        Log.e("is_logout", is_logout + "");

        if (is_logout) {
            editor.remove("id");
            editor.remove("pw");
            editor.remove("autologin");
            editor.remove("intro");
            editor.commit();

        } else if (pref.getBoolean("autologin", false)) {
            Intent intent = new Intent(Page_Login.this, Page_Main.class);
            intent.putExtra("from","login");
            startActivity(intent);
            finish();
        }

        findid();


        boolean hasMenuKey = ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        // 소프트키 있다면
        if (!hasMenuKey && !hasBackKey) {

        }

    }

    View.OnClickListener btnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.btn_join:
                    intent = new Intent(Page_Login.this, Page_Agreement.class);
                    startActivity(intent);
                    break;
                case R.id.btn_idpw:
                    intent = new Intent(Page_Login.this, Page_ID_PW.class);
                    startActivity(intent);
                    break;
                case R.id.btn_login:
                    btn_login.setEnabled(false);
                    id = edit_id.getText().toString();
                    pw = edit_pw.getText().toString();
                    if (id == null || id.equals("")) {
                        custom_dial = new Util_Dialog(Page_Login.this, getResources().getDrawable(R.drawable.icon_lastpage), "아이디를 입력해 주세요.", "확인", "dismiss");
                        custom_dial.show();
                        btn_login.setEnabled(true);
                    } else if (pw == null || pw.equals("")) {
                        custom_dial = new Util_Dialog(Page_Login.this, getResources().getDrawable(R.drawable.icon_lastpage), "비밀번호를 입력해 주세요.", "확인", "dismiss");
                        custom_dial.show();
                        btn_login.setEnabled(true);
                    } else {
                        try {
                            String login = new Util_PHP(Page_Login.this).execute(id, pw, "login", "http://175.126.38.139/php/check_login.php").get();

                            if (login.contains("login_success")) {
                                result_nick = login.substring(13);
                                Log.e("nick",""+result_nick );
                                editor.putString("id", id);
                                editor.putString("pw", pw);
                                editor.putBoolean("autologin", true);
                                editor.putBoolean("appFrist", true);
                                editor.commit();

                                session = new Util_PHP(Page_Login.this).execute(id, "session_check", "http://175.126.38.139/php/check_session.php").get();
                                String test = new Util_PHP(Page_Login.this).execute(result_nick, FirebaseInstanceId.getInstance().getToken(), "Insert_token", "http://175.126.38.139/php/fcm/register.php").get();

                                intent = new Intent(Page_Login.this, Page_Main.class);
                                intent.putExtra("from", "login");
                                startActivity(intent);
                                finish();

                            } else {
                                custom_dial = new Util_Dialog(Page_Login.this, getResources().getDrawable(R.drawable.icon_lastpage), "일치하는 정보가 없습니다", "확인", "dismiss");
                                custom_dial.show();
                                btn_login.setEnabled(true);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    break;
                case R.id.content:
                    hide_keyboard();
                    edit_id.clearFocus();
                    edit_pw.clearFocus();
                    break;
            }
        }
    };


    void findid() {

        tmp = (RelativeLayout) findViewById(R.id.join_findidpw);
        content = (RelativeLayout) findViewById(R.id.content);
        content.setOnClickListener(btnlistener);
        findViewById(R.id.btn_join).setOnClickListener(btnlistener);
        findViewById(R.id.btn_idpw).setOnClickListener(btnlistener);
        btn_login = (TextView) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(btnlistener);
        edit_id = (EditText) findViewById(R.id.idinput);
        edit_id.setOnClickListener(btnlistener);
        edit_id.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    edit_id.setHint("");

                } else {
                    edit_id.setHint("아이디");
                    if (!edit_id.getText().toString().equals("") || edit_id.getText().toString() != null) {
                        edit_id.setBackgroundResource(R.drawable.login_input_id);
                        setid = true;
                    }
                }


            }
        });
        edit_pw = (EditText) findViewById(R.id.pwinput);
        edit_pw.setOnClickListener(btnlistener);
        edit_pw.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    edit_pw.setHint("");


                } else {
                    edit_pw.setHint("비밀번호");
                    if (!edit_pw.getText().toString().equals("") || edit_pw.getText().toString() != null) {
                        edit_pw.setBackgroundResource(R.drawable.login_input_pw);
                        setpw = true;
                    }
                }
            }
        });
        logo = (ImageView) findViewById(R.id.logo);


    }

    void hide_keyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit_id.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(edit_pw.getWindowToken(), 0);
    }

}






