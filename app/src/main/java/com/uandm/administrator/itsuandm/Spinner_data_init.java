package com.uandm.administrator.itsuandm;

/**
 * Created by its on 2017-01-10.
 */
public class Spinner_data_init {

    public String[] init_height() {
        String[] tmp = new String[53];
        tmp[0] = "키";
        tmp[1] = "140이하";
        tmp[52] = "190이상";
        for (int i = 2; i < 52; i++) {
            tmp[i] = Integer.toString(i + 140);
            tmp[i] += "cm";
        }

        return tmp;

    }

    public String[] init_weight() {
        String[] tmp = new String[6];
        tmp[0] = "체형";
        tmp[1] = "마름";
        tmp[2] = "조금마름";
        tmp[3] = "보통";
        tmp[4] = "조금통통";
        tmp[5] = "통통";
        return tmp;

    }
}
