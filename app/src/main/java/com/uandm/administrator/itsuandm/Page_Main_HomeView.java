package com.uandm.administrator.itsuandm;


import android.content.Context;
import android.content.Intent;

import android.media.MediaPlayer;
import android.net.Uri;

import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.RemoteInput;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;

import android.view.View;
import android.view.ViewGroup;

import android.widget.*;
import com.bumptech.glide.Glide;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;


public class Page_Main_HomeView extends Fragment implements ViewPager.OnPageChangeListener {

    //    implements ViewPager.OnPageChangeListener
    protected View view;
    private ViewPager intro_images;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private Main_Event_Pager_Adapter mAdapter;
    private  RelativeLayout li_2;
    private TextView youneedtoreadnow;
    private RelativeLayout more;
    private ImageView best1, best2, best3;

    private String json, json2;
    private JSONArray event_list, best_list;
    private ArrayList<Main_Event_Pager_item> items;
    private LinearLayout scrollView;


    @Override
    public void onStart() {
        super.onStart();
    }

    ////
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        items = new ArrayList<>();
        view = inflater.inflate(R.layout.l_main_homeview, container, false);

        scrollView = (LinearLayout)view.findViewById(R.id.scrollbar);
        scrollView.setVerticalScrollBarEnabled(false);
        intro_images = (ViewPager) view.findViewById(R.id.pager_introduction);
        pager_indicator = (LinearLayout) view.findViewById(R.id.viewPagerCountDots);
        youneedtoreadnow = (TextView) view.findViewById(R.id.youneedtoreannow);
        li_2 = (RelativeLayout)view.findViewById(R.id.li_2);
        more = (RelativeLayout) view.findViewById(R.id.more);
        best1 = (ImageView) view.findViewById(R.id.review_img_1);
        best2 = (ImageView) view.findViewById(R.id.review_img_2);
        best3 = (ImageView) view.findViewById(R.id.review_img_3);
        //event_items = new ArrayList<>();
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getContext(), Page_Review_More.class);
                startActivity(intent1);
            }
        });
        li_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getContext(), Page_Usage.class);
                startActivity(intent1);
            }

        });
        //
        try {
            json = new Util_PHP(getContext()).execute("home_event", "http://175.126.38.139/php/getdata_main_event.php").get();
            json2 = new Util_PHP(getContext()).execute("review_best", "http://175.126.38.139/php/getdata_review_best.php").get();
            rendering_best(json2);
            if (json.equals("no_event")) {
                //진행중인 이벤트가 없습니다.
            } else {
                rendering_event(json);
            }
        } catch (Exception e) {

        }

        return view;

    }

    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_round));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_round));
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_round));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_round));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    protected void rendering_event(String json) {
        try {
            JSONObject jsonObj2 = new JSONObject(json);
            event_list = jsonObj2.getJSONArray("result");
            for(int i=0;i<event_list.length();i++) {
                JSONObject c = event_list.getJSONObject(i);
                items.add(new Main_Event_Pager_item(getContext(), c.getString("event_type"), c.getString("event_img"),c.getString("event_link")));
                mAdapter = new Main_Event_Pager_Adapter(getContext(), items);
            }
            intro_images.setAdapter(mAdapter);
            intro_images.setCurrentItem(0);
            intro_images.setOnPageChangeListener(this);
            setUiPageViewController();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    protected void rendering_best(String json) {
        try {
            JSONObject jsonObj = new JSONObject(json);
            best_list = jsonObj.getJSONArray("result");
            for (int i = 0; i < best_list.length(); i++) {
                JSONObject c = best_list.getJSONObject(i);
                if (best1.getVisibility() == INVISIBLE) {
                    best1.setVisibility(VISIBLE);
                    Glide.with(getContext()).load(c.getString("img")).asBitmap().into(best1);
                } else if (best2.getVisibility() == INVISIBLE) {
                    best2.setVisibility(VISIBLE);
                    Glide.with(getContext()).load(c.getString("img")).asBitmap().into(best2);
                } else {
                    best3.setVisibility(VISIBLE);
                    Glide.with(getContext()).load(c.getString("img")).asBitmap().into(best3);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}







