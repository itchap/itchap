package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.tsengvn.typekit.TypekitContextWrapper;

public class A_Status_Type extends AppCompatActivity {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private RelativeLayout personal, group, btn_back;
    private ImageView personal_img, group_img;
    private  String intent_type;
    private Util_Dialog dialog;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_status_type);
        pref = this.getSharedPreferences("login", 0);
        editor = pref.edit();

        personal = (RelativeLayout) findViewById(R.id.re_personal);
        personal_img = (ImageView) findViewById(R.id.status_personal);
        group = (RelativeLayout) findViewById(R.id.re_group);
        group_img = (ImageView) findViewById(R.id.status_group);

        btn_back = (RelativeLayout)findViewById(R.id.status_back_btn);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent1 = getIntent();
        intent_type = intent1.getStringExtra("type");

        personal.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        personal.setBackground(getResources().getDrawable(R.drawable.box_round_main_color));
                        personal_img.setBackground(getResources().getDrawable(R.drawable.personal_ch));
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP: {
                        personal.setBackground(getResources().getDrawable(R.drawable.box_round));
                        personal_img.setBackground(getResources().getDrawable(R.drawable.personal_unch));
                        break;
                    }
                    default: {
                        break;
                    }
                }
                return false;
            }
        });
        group.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        group.setBackground(getResources().getDrawable(R.drawable.box_round_main_color));
                        group_img.setBackground(getResources().getDrawable(R.drawable.group_ch));
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP: {
                        group.setBackground(getResources().getDrawable(R.drawable.box_round));
                        group_img.setBackground(getResources().getDrawable(R.drawable.group_unch));
                        break;
                    }
                    default: {
                        break;
                    }
                }
                return false;
            }
        });


        personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(intent_type.equals("to_status")){
                    Intent intent = new Intent(A_Status_Type.this, A_Status.class);
                    intent.putExtra("type", "personal");
                    startActivity(intent);
                }else if(intent_type.equals("to_ing")){
                    Intent intent = new Intent(A_Status_Type.this, A_ing.class);
                    intent.putExtra("type", "personal");
                    startActivity(intent);
                }else {
                    Log.e("error","");
                }
            }
        });
        group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String do_you_have_a_team = new Util_PHP(A_Status_Type.this).execute(pref.getString("nick", ""), "do_you_have_a_team", "http://175.126.38.139/php/do_you_have_a_team.php").get();
                    Log.e("doyouhaveateam?",do_you_have_a_team);
                    if (do_you_have_a_team.equals("0")) {
                        dialog = new Util_Dialog(A_Status_Type.this, getResources().getDrawable(R.drawable.icon_lastpage), "현재 팀이 없습니다. \n 팀을 먼저 만들어 주세요", "확인", "dismiss");
                        dialog.show();
                    } else if (do_you_have_a_team.equals("2")) {
                        if(intent_type.equals("to_status")){
                            Intent intent = new Intent(A_Status_Type.this, A_Status.class);
                            intent.putExtra("type", "group");
                            startActivity(intent);
                        }else if(intent_type.equals("to_ing")){
                            Intent intent = new Intent(A_Status_Type.this, A_ing.class);
                            intent.putExtra("type", "group");
                            startActivity(intent);
                        }else {
                            Log.e("error","");
                        }

                    } else {
                        Log.e("ERROR", "");
                    }
                } catch (Exception e) {
                    e.getStackTrace();
                }
            }
        });
    }
}
