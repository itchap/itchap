package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by young on 2017-01-22.
 */

public class Page_Credit_Adapter extends RecyclerView.Adapter<Page_Credit_Adapter.ViewHolder> {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList//
    private ArrayList<Page_Credit_Item> listViewItemList;
    private Context context;
    ////
    private Util_Dialog dialog;
    public static final int ITEM_TYPE_ONE = 1;
    public static final int ITEM_TYPE_TWO = 2;

    int type;

    public Page_Credit_Adapter(ArrayList<Page_Credit_Item> item, Context context) {
        this.context = context;
        this.listViewItemList = item;
    }

    @Override
    public int getItemViewType(int position) {
        return ITEM_TYPE_ONE;
    }

    @Override
    public Page_Credit_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_credit_charge_item, viewGroup, false);
        return new Page_Credit_Adapter.ViewHolder(view, 1);
    }


    /**
     * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(final Page_Credit_Adapter.ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);

        viewHolder.credit_no.setText(listViewItemList.get(position).getCredit_no());
        viewHolder.credit_no_plus.setText(listViewItemList.get(position).getCredit_no_plus());
        viewHolder.price.setText(listViewItemList.get(position).getPrice());
    }


    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }


    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView credit_no, credit_no_plus, price;
        protected RelativeLayout bg;

        public ViewHolder(View itemView, int type) {
            super(itemView);
            credit_no = (TextView) itemView.findViewById(R.id.coin_no);
            credit_no_plus = (TextView) itemView.findViewById(R.id.coin_no_plus);
            price = (TextView) itemView.findViewById(R.id.price);
            bg = (RelativeLayout)itemView.findViewById(R.id.bg);
        }
    }
}



