package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;

import com.tsengvn.typekit.TypekitContextWrapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class A_Status_Send extends Fragment {

    private Util_Dialog dialog;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private LinearLayout no_item;
    private RecyclerView recyclerview;

    private ArrayList<A_Status_ltem> item;
    private String type;
    private A_Status_Adapter adapter;
    protected View view;
    private String img[];
    String json_personal, json_group;
    JSONArray jsonArray_personal, jsonArray_group = null;

    private RequestManager requestManager;
    private static final String TAG_RESULTS = "result";


    private LinearLayout more;
    private int post_index = 0;

    public static A_Status_Send newInstance(int page, String type) {
        A_Status_Send send = new A_Status_Send();
        Bundle arg = new Bundle();
        arg.putInt("page", page);
        arg.putString("type", type);
        send.setArguments(arg);
        return send;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getString("type");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.l_status_send, container, false);


        no_item = (LinearLayout) view.findViewById(R.id.no_list);

        recyclerview = (RecyclerView) view.findViewById(R.id.status_send_recycler);
        more = (LinearLayout) view.findViewById(R.id.more);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        recyclerview.setOverScrollMode(View.OVER_SCROLL_NEVER);
        recyclerview.setNestedScrollingEnabled(false);
        //LinearLayoutManager layout = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(layoutManager);

        recyclerview.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(10), true));
        recyclerview.setItemAnimator(new DefaultItemAnimator());


        item = new ArrayList<>();

        requestManager = Glide.with(getContext());

        adapter = new A_Status_Adapter(item, getContext(), requestManager);

        pref = getContext().getSharedPreferences("login", 0);
        editor = pref.edit();

        try {
            json_personal = new Util_PHP(getContext()).execute(type, pref.getString("nick", ""),Integer.toString(post_index),"sent_by_me_personal", "http://175.126.38.139/php/like_sent_by_me.php").get();
            if (json_personal.contains("no_list")) {
            } else {
                no_item.setVisibility(GONE);
                recyclerview.setVisibility(VISIBLE);
                rendering_personal(json_personal);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        recyclerview.setAdapter(adapter);

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.e("모어", "" + post_index);
                    json_personal = new Util_PHP(getContext()).execute(type, pref.getString("nick", ""), Integer.toString(post_index), "sent_by_me_personal", "http://175.126.38.139/php/like_sent_by_me.php").get();

                    if(json_personal.contains("no_list")){
                        Log.e("끝", "");
                    }else {
                        rendering_personal(json_personal);
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        });
        return view;
    }

    protected void rendering_personal(String json_personal) {


        try {
            JSONObject jsonObj2 = new JSONObject(json_personal);
            jsonArray_personal = jsonObj2.getJSONArray(TAG_RESULTS);
            post_index = post_index + jsonArray_personal.length();
            if (jsonArray_personal.length() == 9) {
                more.setVisibility(VISIBLE);
            } else {
                more.setVisibility(GONE);
            }
            for (int i = 0; i < jsonArray_personal.length(); i++) {

                JSONObject c = jsonArray_personal.getJSONObject(i);


                img = new String[Integer.parseInt(c.getString("img_cnt"))];

                switch (Integer.parseInt(c.getString("img_cnt"))) {
                    case 1:
                        img[0] = c.getString("img1");
                        break;
                    case 2:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        break;
                    case 3:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        img[2] = c.getString("img3");
                        break;
                    case 4:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        img[2] = c.getString("img3");
                        img[3] = c.getString("img4");
                        break;
                }
                item.add(new A_Status_ltem(getContext(), img, c.getString("title"), c.getString("date"), c.getString("owner"), Integer.parseInt(c.getString("img_cnt")), "status_cancel"));
                Log.e("cnt", c.getString("img_cnt"));
            }
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    protected String diffOfDate(String begin, String end) throws Exception {
//
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
//
//        Date beginDate = formatter.parse(begin);
//        Date endDate = formatter.parse(end);
//
//        long diff = beginDate.getTime() - endDate.getTime();
//        long diffDays = diff / (24 * 60 * 60 * 1000);
//        String date;
//        if(diffDays > 0 ) {
//            date = "D-" + diffDays;
//        }else {
//            //이미 지난 것들
//            date = "D+" + (-diffDays);
//        }
//        return date;
//    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}