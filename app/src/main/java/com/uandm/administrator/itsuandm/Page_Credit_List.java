package com.uandm.administrator.itsuandm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class Page_Credit_List extends Fragment {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Util_Dialog dialog;
    private LinearLayout no_item;
    private RecyclerView recyclerview;

    private ArrayList<Credit_List_Item> item;
    private Credit_List_Adapter adapter;


    String json;
    JSONArray jsonArray= null;
    private static final String TAG_RESULTS = "result";
    protected View view;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.l_credit_list, container, false);


        no_item = (LinearLayout) view.findViewById(R.id.no_list);
        recyclerview = (RecyclerView) view.findViewById(R.id.recycler_credit_list);

        recyclerview.setOverScrollMode(View.OVER_SCROLL_NEVER);
        recyclerview.setNestedScrollingEnabled(false);
        LinearLayoutManager layout = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(layout);

        item = new ArrayList<>();

        pref = getContext().getSharedPreferences("login", 0);
        editor = pref.edit();

        try {
            json = new Util_PHP(getContext()).execute(pref.getString("nick", ""), "credit_list", "http://175.126.38.139/php/credit_list.php").get();
            if (json.contains("no_list")) {
                no_item.setVisibility(VISIBLE);
                recyclerview.setVisibility(GONE);
            }else{
                rendering_date(json);
                no_item.setVisibility(GONE);
                recyclerview.setVisibility(VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        adapter = new Credit_List_Adapter(item, getContext());
        recyclerview.setAdapter(adapter);

        return view;
    }

    protected void rendering_date(String json) {
        try {
            JSONObject jsonObj2 = new JSONObject(json);
            jsonArray = jsonObj2.getJSONArray("result");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject c = jsonArray.getJSONObject(i);
                item.add(new Credit_List_Item(getContext(), c.getString("p_m"), c.getString("total"), c.getString("type"),c.getString("date")));
            }
        }catch (Exception e){

        }

    }



}