package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.bumptech.glide.util.Util;
import com.nhn.android.maps.*;

import android.graphics.Color;
import android.graphics.RadialGradient;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import java.util.*;

import android.support.v7.widget.RecyclerView;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;
import com.nhn.android.maps.NMapContext;
import com.nhn.android.maps.NMapView;
import com.nhn.android.maps.maplib.NGPoint;
import com.nhn.android.maps.maplib.NGeoPoint;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager;

import me.leolin.shortcutbadger.ShortcutBadger;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.List;

import com.nhn.android.maps.NMapContext;
import com.nhn.android.maps.NMapView;

/**
 * Created by u on 2017-02-01.
 */
public class A_ing_Adapter extends RecyclerView.Adapter<A_ing_Adapter.ViewHolder> {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList//
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private ArrayList<A_ing_Item> listViewItemList;
    private Context context;

    public static final int ITEM_TYPE_ONE = 1;
    public static final int ITEM_TYPE_TWO = 2;
    public static final int ITEM_TYPE_THREE = 3;
    public static final int ITEM_TYPE_FOUR = 4;

    private int index = 1;

    private RequestManager requestManager;
    private Dialog dialogg, cancel_dialog;
    private Util_Dialog dialog;
    private InputMethodManager imm;
    private String nick;

    int type;
    private boolean[] selected = new boolean[31];

    public A_ing_Adapter(ArrayList<A_ing_Item> item, Context context, RequestManager requestManager) {
        this.context = context;
        this.listViewItemList = item;
        this.requestManager = requestManager;
    }

    @Override
    public int getItemViewType(int position) {

        if (listViewItemList.get(position).getType() == 1) {
            this.type = 1;
            return ITEM_TYPE_ONE;
        } else if (listViewItemList.get(position).getType() == 2) {
            this.type = 2;
            return ITEM_TYPE_TWO;
        } else if (listViewItemList.get(position).getType() == 3) {
            this.type = 3;
            return ITEM_TYPE_THREE;
        } else {
            this.type = 4;
            return ITEM_TYPE_FOUR;
        }


    }

    /**
     * 레이아웃을 만들어서 Holer에 저장
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public A_ing_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;
        pref = context.getSharedPreferences("login", 0);
        editor = pref.edit();
        nick = pref.getString("nick", "");


        if (viewType == ITEM_TYPE_ONE) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_ing_parent, viewGroup, false);
            return new A_ing_Adapter.ViewHolder(view, ITEM_TYPE_ONE);
        } else if (viewType == ITEM_TYPE_TWO) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_ing_parent_2, viewGroup, false);
            return new A_ing_Adapter.ViewHolder(view, ITEM_TYPE_TWO);
        } else if (viewType == ITEM_TYPE_THREE) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_ing_parent_3, viewGroup, false);
            return new A_ing_Adapter.ViewHolder(view, ITEM_TYPE_THREE);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_ing_parent_4, viewGroup, false);
            return new A_ing_Adapter.ViewHolder(view, ITEM_TYPE_FOUR);
        }

    }


    /**
     * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(final A_ing_Adapter.ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);

        if (itemtype == ITEM_TYPE_ONE) {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
        } else if (itemtype == ITEM_TYPE_TWO) {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
        } else if (itemtype == ITEM_TYPE_THREE) {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
            requestManager.load(listViewItemList.get(position).getImg()[2]).asBitmap().into(viewHolder.iv_3);
        } else {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
            requestManager.load(listViewItemList.get(position).getImg()[2]).asBitmap().into(viewHolder.iv_3);
            requestManager.load(listViewItemList.get(position).getImg()[3]).asBitmap().into(viewHolder.iv_4);
        }
        requestManager.load("http://175.126.38.139/img/more_profile.png").asBitmap().into(viewHolder.more_profile);

        viewHolder.title.setText(listViewItemList.get(position).getTitle());
        viewHolder.date.setText(listViewItemList.get(position).getDate());
        viewHolder.date2.setText(listViewItemList.get(position).getDate2());
        viewHolder.time.setText(listViewItemList.get(position).getTime());
        viewHolder.name.setText(listViewItemList.get(position).getName());


        //0
        ShortcutBadger.removeCount(context);
        //알람
        FirebaseMessagingService.bagde_count_total=0;
        FirebaseMessagingService.bagde_count_chat=0;
        FirebaseMessagingService.bagde_count_total += FirebaseMessagingService.bagde_count;
        //
        FirebaseMessagingService.bagde_count_chat += Integer.parseInt(listViewItemList.get(position).getAl_cnt());
        FirebaseMessagingService.bagde_count_total += FirebaseMessagingService.bagde_count_chat;
        ShortcutBadger.applyCount(context, FirebaseMessagingService.bagde_count_total);

        if (listViewItemList.get(position).getAl_cnt().equals("0")) {
            viewHolder.chat_al.setVisibility(View.GONE);
        } else {
            viewHolder.chat_al.setVisibility(View.VISIBLE);
            viewHolder.chat_al.setText(listViewItemList.get(position).getAl_cnt());
        }


        viewHolder.chatting.setOnClickListener(new View.OnClickListener() {
            private TextView dialog_cancel, dialog_confirm;
            private LinearLayout opppp;
            private String json, json_;

            @Override
            public void onClick(View v) {
                try {
                    json = new Util_PHP(context).execute(nick, Integer.toString(listViewItemList.get(position).getType())
                            , Integer.toString(listViewItemList.get(position).getCmp_id()), "check_chatting", "http://175.126.38.139/php/check_chatting_open.php").get();
                } catch (Exception e) {

                }
                if (json.equals("able")) {
                    //만남시간 시간 10분전 이므로 가능
                    //chat 알림 있으면 0set
                    if (!listViewItemList.get(position).getAl_cnt().equals("0")) {
                        try {
                            json_ = new Util_PHP(context).execute(nick, Integer.toString(listViewItemList.get(position).getCmp_id()), Integer.toString(listViewItemList.get(position).getType())
                                    , "update_chat_al_zero", "http://175.126.38.139/php/update_zero_chat.php").get();

                            //0
                            ShortcutBadger.removeCount(context);
                            //알람
                            FirebaseMessagingService.bagde_count_total = 0;
                            FirebaseMessagingService.bagde_count_total += FirebaseMessagingService.bagde_count;
                            FirebaseMessagingService.bagde_count_chat -= Integer.parseInt(listViewItemList.get(position).getAl_cnt());
                            FirebaseMessagingService.bagde_count_total += FirebaseMessagingService.bagde_count_chat;
                            ShortcutBadger.applyCount(context, FirebaseMessagingService.bagde_count_total);

                            listViewItemList.get(position).setAl_cnt("0");
                            notifyDataSetChanged();
                        } catch (Exception e) {

                        }
                    }
                    Intent chatting = new Intent(context, Page_Chatting.class);
                    chatting.putExtra("team_count", listViewItemList.get(position).getType());
                    chatting.putExtra("cmp_no", listViewItemList.get(position).getCmp_id());
                    chatting.putExtra("other", listViewItemList.get(position).getTeam_owner());
                    context.startActivity(chatting);

                } else if (json.equals("disable")) {
                    // 만남 시간 10분 보다 많이 남음
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.l_chatting_dialog);
                    dialog.setCancelable(false);

                    opppp = (LinearLayout) dialog.findViewById(R.id.opppp);

                    opppp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                json_ = new Util_PHP(context).execute(nick, Integer.toString(listViewItemList.get(position).getType())
                                        , Integer.toString(listViewItemList.get(position).getCmp_id())
                                        , "update_chatting_open", "http://175.126.38.139/php/open_chatting.php").get();
                            } catch (Exception e) {

                            }

                            if (json_.equals("able")) {
                                Intent chatting = new Intent(context, Page_Chatting.class);
                                chatting.putExtra("team_count", listViewItemList.get(position).getType());
                                chatting.putExtra("cmp_no", listViewItemList.get(position).getCmp_id());
                                chatting.putExtra("other", listViewItemList.get(position).getTeam_owner());
                                context.startActivity(chatting);

                                dialog.dismiss();

                            } else if (json_.equals("disable")) {
                                new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_warning)
                                        , "코인이 부족합니다. 충전 후 이용해주세요.", "확인", "dismiss").show();
                            } else if (json_.equals("server_error")) {
                                new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_warning)
                                        , "미안해용~~~~~~", "확인", "dismiss").show();
                            }


                        }
                    });
                    dialog_cancel = (TextView) dialog.findViewById(R.id.btn_2);
                    dialog_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else if (json.equals("server_error")) {
                    new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_warning)
                            , "미안해용~~~~~", "확인", "dismiss").show();
                }


            }
        });

        //프로필 더보기
        viewHolder.more_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(context, Team_Info.class);

                intent1.putExtra("date_time", listViewItemList.get(position).getFull_date());
                intent1.putExtra("get_intent_type", "complete");
                intent1.putExtra("teammate_count", listViewItemList.get(position).getType());
                intent1.putExtra("owner_nick", listViewItemList.get(position).getTeam_owner());
                intent1.putExtra("store_id", listViewItemList.get(position).getStore_id());
                context.startActivity(intent1);
            }
        });
    }


    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }


    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView ad, time, title, date, date2, name, btn_more_store;
        protected Util_ImageView iv_1, iv_2, iv_3, iv_4;
        private ImageView more_profile, btn;
        protected LinearLayout child;
        protected RelativeLayout main1;
        protected RelativeLayout report, cancel, chatting;
        protected TextView chat_al;


        public ViewHolder(View itemView, int type) {
            super(itemView);


            if (type == 1) {
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                iv_1.setType(1);
                iv_2 = null;
                iv_3 = null;
                iv_4 = null;
            } else if (type == 2) {
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                iv_1.setType(2);
                iv_2 = (Util_ImageView) itemView.findViewById(R.id.img2);
                iv_2.setType(3);
                iv_3 = null;
                iv_4 = null;
            } else if (type == 3) {
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                iv_1.setType(4);
                iv_2 = (Util_ImageView) itemView.findViewById(R.id.img2);
                iv_2.setType(5);
                iv_3 = (Util_ImageView) itemView.findViewById(R.id.img3);
                iv_3.setType(6);
                iv_4 = null;
            } else if (type == 4) {
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                iv_1.setType(7);
                iv_2 = (Util_ImageView) itemView.findViewById(R.id.img2);
                iv_2.setType(9);
                iv_3 = (Util_ImageView) itemView.findViewById(R.id.img3);
                iv_3.setType(8);
                iv_4 = (Util_ImageView) itemView.findViewById(R.id.img4);
                iv_4.setType(10);

            } else {

            }
            more_profile = (ImageView) itemView.findViewById(R.id.more_profile);


            main1 = (RelativeLayout) itemView.findViewById(R.id.main_1);
            //상단 date D-n
            date = (TextView) itemView.findViewById(R.id.date);
            title = (TextView) itemView.findViewById(R.id.title);
            //하단 date
            date2 = (TextView) itemView.findViewById(R.id.date2);
            time = (TextView) itemView.findViewById(R.id.time);
            ad = (TextView) itemView.findViewById(R.id.ad);
            name = (TextView) itemView.findViewById(R.id.name);
            chatting = (RelativeLayout) itemView.findViewById(R.id.chatting);
            chat_al = (TextView) itemView.findViewById(R.id.chat_al);

        }
    }


}
