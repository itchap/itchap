package com.uandm.administrator.itsuandm;

/**
 * Created by young on 2016-11-21.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import java.util.ArrayList;

//import com.androprogrammer.tutorials.R;


public class Main_Event_Pager_Adapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<Main_Event_Pager_item> items;
    private int no;

    public Main_Event_Pager_Adapter(Context mContext, ArrayList<Main_Event_Pager_item> items) {
        this.mContext = mContext;
        this.items = items;
        this.no = items.size();
    }

    @Override
    public int getCount() {
        return no;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.l_main_event_pager_item, container, false);
        ImageView iv_1 = (ImageView) itemView.findViewById(R.id.iv_event_pager_item);

        Glide.with(mContext).load(items.get(position).getImg()).asBitmap().into(iv_1);

        iv_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (items.get(position).getType().equals("none")) {
                    Log.e("none_pager", "" + "갈곳이 없어용");
                } else if (items.get(position).getType().equals("webview")) {
                    Intent intent = new Intent(mContext, Main_Event_Webview.class);
                    Log.d("uri", items.get(position).getLink());
                    intent.putExtra("uri", items.get(position).getLink());
                    mContext.startActivity(intent);
                } else {
                    Log.e("none_pager", "" + "else??");
                }
            }
        });
//    imageView.setImageResource(mResources[position]);

        container.addView(itemView);

        return itemView;
    }
//

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}