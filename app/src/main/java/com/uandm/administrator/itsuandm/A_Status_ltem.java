package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by young on 2017-01-22.
 */

public class A_Status_ltem {
    String img[] = new String[4];
    String title, time, date;
    String time2;
    String time3;



    String date2;

    String btn_type;
    Context context;

    String owner;
    int type;

    public A_Status_ltem(Context context, String img[], String title, String date, String owner, int type, String btn_type) {
        this.context = context;
        this.title = title;
        this.owner = owner;
        this.type = type;
        this.btn_type = btn_type;
        this.date2=date;


        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String today = formatter.format(calendar.getTime());

        // this.date= date.substring(0, 8);
        time2 = date.substring(8, 10);
        time3 = date.substring(10, 12);
        this.time = time2 + ":" + time3;
        try {
            this.date = diffOfDate(date.substring(0, 8), today);
        } catch (Exception e) {

        }
        for (int i = 0; i < img.length; i++) {
            if (img[i] != null)
                this.img[i] = img[i];
            else
                this.img[i] = null;
        }
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String[] getImg() {
        return img;
    }

    public void setImg(String[] img) {
        this.img = img;
    }

    public String getBtn_type() {
        return btn_type;
    }

    public void setBtn_type(String btn_type) {
        this.btn_type = btn_type;
    }
    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    protected String diffOfDate(String begin, String end) throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        Date beginDate = formatter.parse(begin);
        Date endDate = formatter.parse(end);

        long diff = beginDate.getTime() - endDate.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        String date;
        if (diffDays > 0) {
            date = "D-" + diffDays;
        } else if(diffDays == 0) {
            date = "D-day";
        }else {
            //이미 지난 것들
            date = "D+" + (-diffDays);
        }
        return date;
    }
}

