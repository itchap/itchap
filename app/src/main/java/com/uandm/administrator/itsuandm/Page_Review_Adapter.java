package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by u on 2017-01-24.
 */
public class Page_Review_Adapter extends RecyclerView.Adapter<Page_Review_Adapter.ViewHolder> {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList//
    private ArrayList<Page_Review_Item> listViewItemList;
    private Context context;
    ////
    private Util_Dialog dialog;
    public static final int ITEM_TYPE_ONE = 1;
    public static final int ITEM_TYPE_TWO = 2;

    int type;

    public Page_Review_Adapter(ArrayList<Page_Review_Item> item, Context context) {
        this.context = context;
        this.listViewItemList = item;
    }

    @Override
    public int getItemViewType(int position) {
        return ITEM_TYPE_ONE;
    }

    @Override
    public Page_Review_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_review_more_item, viewGroup, false);
        return new Page_Review_Adapter.ViewHolder(view, 1);
    }


    /**
     * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(final Page_Review_Adapter.ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);
//        array_push($result,  array('no'=>$row[0],'writer'=>$row[1],'date'=>$row[2],'img'=>$row[3],'content'=>$row[4],'profile'=>$row2[0]));
        viewHolder.contents.setText(listViewItemList.get(position).getContents());
        viewHolder.nick.setText(listViewItemList.get(position).getNick());
        viewHolder.date.setText(listViewItemList.get(position).getDate());
        Glide.with(context).load(listViewItemList.get(position).getProfile()).asBitmap().into(viewHolder.profile);
        Glide.with(context).load(listViewItemList.get(position).getImg()).asBitmap().into(viewHolder.photo);
    }


    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }


    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        protected Util_ImageView profile;
        protected ImageView photo;
        protected TextView contents, nick,date;

        public ViewHolder(View itemView, int type) {
            super(itemView);
            profile = (Util_ImageView) itemView.findViewById(R.id.profile);
            profile.setType(11);
            photo = (ImageView) itemView.findViewById(R.id.photo);
            contents = (TextView) itemView.findViewById(R.id.contents);
            nick = (TextView) itemView.findViewById(R.id.nick);
            date =(TextView)itemView.findViewById(R.id.date);
        }
    }
}



