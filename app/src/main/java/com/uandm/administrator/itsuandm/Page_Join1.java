package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.content.*;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tsengvn.typekit.TypekitContextWrapper;

public class Page_Join1 extends AppCompatActivity {



    private TextView verification ;
    private RelativeLayout btn_back;
    private Struct_Join struct_join;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private Util_Dialog custiom_dial;

    private static final String ERROR_CODE_0 = "fail to loading module";
    private static final String ERROR_CODE_1 = "irregular access time termination";
    private static final String ERROR_CODE_2 = "irregular access ip mismatch";
    private String json, token;

    BroadcastReceiver receiver;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_join_form_2);

        Intent intent = getIntent();
        struct_join = (Struct_Join)intent.getSerializableExtra("tt");

        IntentFilter filter = new IntentFilter();
        filter.addAction("VERICAION");


        Findid();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String str = intent.getStringExtra("result");
                Log.e("broad result", str);

                if(str.equals("success")) {
                    struct_join = (Struct_Join) intent.getSerializableExtra("all_struct");

                    if(struct_join.getAge() < 20){
                        // 확인시 로그인 페이지로
                        custiom_dial = new Util_Dialog(Page_Join1.this, getResources().getDrawable(R.drawable.icon_lastpage), "20세 이상만 이용가능합니다.", "확인", "M_age");
                        custiom_dial.show();
                    }

                    struct_join.print();

                    try {
                        json = new Util_PHP(Page_Join1.this).execute(struct_join.getId(), struct_join.getPw(), struct_join.getNick(), struct_join.getWeght()
                                , struct_join.getHeight(), struct_join.getLike1(), struct_join.getLike2(), struct_join.getLike3(), Integer.toString(struct_join.getGender())
                                , struct_join.getImg1(), struct_join.getImg2(), struct_join.getImg3(), struct_join.getName(), struct_join.getPhone()
                                , Integer.toString(struct_join.getAge()), Integer.toString(struct_join.getNation()), struct_join.getJob()
                                , "Insert_member", "http://175.126.38.139/php/insert_member.php").get();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    if(json.equals("phone")){
                        // 중복된 휴대폰 번호
                        custiom_dial = new Util_Dialog(Page_Join1.this, getResources().getDrawable(R.drawable.icon_lastpage), "가입된 휴대폰번호 입니다.", "확인", "dismiss");
                        custiom_dial.show();
                    }else if(json.equals("image1") || json.equals("image2") || json.equals("image3")){
                        // 이미지 입력 실패
                    }else {
                        try{
                            token = new Util_PHP(Page_Join1.this).execute(struct_join.getNick(), FirebaseInstanceId.getInstance().getToken(), "Insert_token", "http://175.126.38.139/php/fcm/register.php").get();
                            Log.e("token insert", token);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        Log.e("1222 result", str);
                            // 회원가입성공
                        pref = getSharedPreferences("login", Activity.MODE_PRIVATE);
                        editor = pref.edit();

                        editor.putString("id", struct_join.getId());
                        editor.putString("nick", struct_join.getNick());
                        editor.putBoolean("autologin", true);
                        editor.commit();
                        Log.e("123123 result", str);
                        custiom_dial = new Util_Dialog(Page_Join1.this, getResources().getDrawable(R.drawable.icon_check), "회원가입이 완료되었습니다.", "확인", "join");
                        custiom_dial.show();

                        Intent main = new Intent(Page_Join1.this, Page_Main.class);
                        Log.e("232324 result", str);
                        main.putExtra("from","join");
                        main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        main.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(main);
                        finish();
                    }

                }else if(str.equals(ERROR_CODE_0)){
                    //에러 다이얼로그
                }else if(str.equals(ERROR_CODE_1)){

                }else if(str.equals(ERROR_CODE_2)){

                }
            }
        };

        registerReceiver(receiver, filter);

    }

    void Findid(){
        btn_back = (RelativeLayout)findViewById(R.id.join2_back_btn);
        btn_back.setOnClickListener(listener);
        verification =(TextView)findViewById(R.id.btn_verification);
        verification.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.join2_back_btn:
                    finish();
                    break;
                case R.id.btn_verification:
                    Intent v_intent = new Intent(Page_Join1.this, Page_Verification.class);
                    v_intent.putExtra("tt", struct_join);
                    v_intent.putExtra("type", 0);
                    startActivity(v_intent);
                    break;
            }
        }
    };


}


