package com.uandm.administrator.itsuandm;

import android.content.Context;

/**
 * Created by young on 2017-01-22.
 */

public class Page_Chatting_Item {
    private Context context;
    private String img;
    private String txt;
    private String type;
    private String id;
    private String dateTime;
    private String nick;
    private boolean isme = false;

    //

    public Page_Chatting_Item(Context context, String img, String txt, String type, String id, String dateTime, String nick, boolean isme) {
       this.context = context;
       this.img = img;
       this.txt = txt;
       this.type = type;
       this.id = id;
       this.dateTime = dateTime;
       this.isme = isme;
       this.nick = nick;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isIsme() {
        return isme;
    }

    public void setIsme(boolean isme) {
        this.isme = isme;
    }


    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

}

