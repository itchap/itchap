package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.*;
import com.tsengvn.typekit.Typekit;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class Page_Agreement extends AppCompatActivity {
    TextView btn1, btn2;
    WebView agree1, agree2;
    RelativeLayout btn_back;
    Button btn_agree;
    private  Util_Dialog dialog;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.l_agreement);
        btn1 = (TextView) findViewById(R.id.btn_agree1);
        btn2 = (TextView) findViewById(R.id.btn_agree2);
        agree1 = (WebView) findViewById(R.id.agree1);
        agree2 = (WebView) findViewById(R.id.agree2);
        btn_agree = (Button) findViewById(R.id.btn_agreement);
      //  agree1.setText(getData("http://175.126.38.139/txt/agree1.txt"));
        // agree2.setText(getData("http://175.126.38.139/txt/agree2.txt"));
        agree1.loadUrl("http://175.126.38.139/txt/agree1.txt");
        agree2.loadUrl("http://175.126.38.139/txt/agree2.txt");
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn1.getCurrentTextColor()==getResources().getColor(R.color.white)) {
                    btn1.setBackgroundResource(R.drawable.agree_white);
                    btn1.setTextColor(getResources().getColor(R.color.deep_grey));
                }else{
                    btn1.setBackgroundResource(R.drawable.agree_app_main);
                    btn1.setTextColor(getResources().getColor(R.color.white));
                }
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn2.getCurrentTextColor()==getResources().getColor(R.color.white)) {
                    btn2.setBackgroundResource(R.drawable.agree_white);
                    btn2.setTextColor(getResources().getColor(R.color.deep_grey));
                }else{
                    btn2.setBackgroundResource(R.drawable.agree_app_main);
                    btn2.setTextColor(getResources().getColor(R.color.white));
                }
            }
        });

        btn_back = (RelativeLayout) findViewById(R.id.agree__back_btn);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn1.getCurrentTextColor()==getResources().getColor(R.color.white)&&btn2.getCurrentTextColor()==getResources().getColor(R.color.white)){
                    Intent intent1 = new Intent(Page_Agreement.this, Page_Join.class);
                    startActivity(intent1);
                }else {
                    dialog = new Util_Dialog(Page_Agreement.this,getResources().getDrawable(R.drawable.icon_lastpage),"약관에 동의해주세요","확인", "dismiss");
                    dialog.show();
                }

            }
        });
    }

    public String getData(String url) {
        class GetData extends AsyncTask<String, Void, String> {
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";

            @Override
            protected String doInBackground(String... params) {
                DataOutputStream os;
                String uri = params[0];
                BufferedReader bufferedReader = null;
                StringBuilder sb = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestProperty("Connection", "Keep-Alive");
                    con.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    os = new DataOutputStream(con.getOutputStream());

                    os.flush();
                    os.close();

                    BufferedReader rd = null;
                    rd = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                    sb = new StringBuilder();
                    String json;
                    while ((json = rd.readLine()) != null) {
                        sb.append(json + "\n");
                    }
                    return sb.toString().trim();
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String result) {

            }
        }

        String result = null;
        GetData g = new GetData();
        try {
            result = g.execute(url).get();
        } catch (Exception e) {
            result = null;
        }
        return result;
    }
}