package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tsengvn.typekit.TypekitContextWrapper;


public class Page_ID_PW extends AppCompatActivity {
    private TextView btn_veri;
    private RelativeLayout btn_back;
    private BroadcastReceiver receiver;
    private static final String ERROR_CODE_0 = "fail to loading module";
    private static final String ERROR_CODE_1 = "irregular access time termination";
    private static final String ERROR_CODE_2 = "irregular access ip mismatch";

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_id_pw);

        Findid();

    }

    public void Findid(){

        btn_veri = (TextView)findViewById(R.id.btn_verification);
        btn_veri.setOnClickListener(listener);
        btn_back = (RelativeLayout)findViewById(R.id.id_pw_back_btn);
        btn_back.setOnClickListener(listener);


    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent ;
            switch (v.getId()){
                case R.id.btn_verification:
                    intent = new Intent(Page_ID_PW.this, Page_Verification.class);
                    intent.putExtra("type", 1);
                    startActivity(intent);
//                    intent = new Intent(Page_ID_PW.this, Page_ID_PW2.class);
//                    intent.putExtra("result","success");
//                    intent.putExtra("name","이경재");
//                    intent.putExtra("phone","01040336876");
                    startActivity(intent);
                    break;
                case R.id.id_pw_back_btn:
                    finish();
                    break;
            }
        }
    };
}


