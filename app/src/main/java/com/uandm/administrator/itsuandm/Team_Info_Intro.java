package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import com.tsengvn.typekit.TypekitContextWrapper;


public class Team_Info_Intro extends Activity {
    private RelativeLayout layout;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        editor.putBoolean("intro",true);
        editor.commit();
        finish();


    }
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_team_info_intro);

        pref = getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();

        layout = (RelativeLayout)findViewById(R.id.l_team_info_intro);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("intro",true);
                editor.commit();
                finish();
            }
        });

    }
}
