package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by u on 2016-12-23.
 */
public class Enroll_Store_RecyclerAdapter extends RecyclerView.Adapter<Enroll_Store_RecyclerAdapter.ViewHolder> {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList
    private ArrayList<Enroll_Store_Item> listViewItemList;
    private Context context;
    private Struct_Enroll struct_enroll;


    public Enroll_Store_RecyclerAdapter(ArrayList<Enroll_Store_Item> item, Context context, Struct_Enroll struct_enroll) {
        this.context = context;
        this.listViewItemList = item;
        this.struct_enroll = struct_enroll;
    }


    /**
     * 레이아웃을 만들어서 Holer에 저장
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_store, viewGroup, false);
        return new ViewHolder(view);

    }


    /**
     * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(final Enroll_Store_RecyclerAdapter.ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);

        viewHolder.name.setText(listViewItemList.get(position).getStore_name());
        viewHolder.loc.setText(listViewItemList.get(position).getStore_loc());
        Glide.with(context).load(listViewItemList.get(position).getImg()).into(viewHolder.img);

        viewHolder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Enroll_Store_Detail.class);
                intent.putExtra("id", listViewItemList.get(position).getId());
                struct_enroll.setLoc1(listViewItemList.get(position).getStore_loc());
                struct_enroll.setStore_id(listViewItemList.get(position).getId());
                struct_enroll.setName(listViewItemList.get(position).getStore_name());
                Log.e("dd", listViewItemList.get(position).getStore_name());
                intent.putExtra("Struct_Enroll", struct_enroll);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        });

        Log.e("stroe", "adapter");


    }


    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }

    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView name, loc;
        protected ImageView img;
        protected RelativeLayout main;

        public ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img);
            name = (TextView) itemView.findViewById(R.id.name);
            loc = (TextView) itemView.findViewById(R.id.loc);
            main = (RelativeLayout) itemView.findViewById(R.id.store_main);


        }
    }
}



