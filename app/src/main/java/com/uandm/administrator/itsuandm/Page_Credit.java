package com.uandm.administrator.itsuandm;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.vending.billing.IInAppBillingService;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.ArrayList;

public class Page_Credit extends AppCompatActivity {


    private ViewPager viewpager;
    private RelativeLayout charge_tab, list_tab;
    private ImageView under_1, under_2;
    private RelativeLayout back;
    TextView[] title = new TextView[2];
    ImageView[] under = new ImageView[2];

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_credit);

        Findid();

        viewpager = (ViewPager) findViewById(R.id.view_pager1);
        VIewPager_Adapter viewpagerAdapter = new VIewPager_Adapter(getSupportFragmentManager(), "credit", "");
        viewpager.setAdapter(viewpagerAdapter);  viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    title[0] = (TextView) findViewById(R.id.btn_1);
                    title[0].setTextColor(getResources().getColor(R.color.app_default));
                    under[0] = (ImageView) findViewById(R.id.underline_1);
                    under[0].setBackgroundColor(getResources().getColor(R.color.app_default));

                    title[1] = (TextView) findViewById(R.id.btn_2);
                    title[1].setTextColor(getResources().getColor(R.color.soft_grey));
                    under[1] = (ImageView) findViewById(R.id.underline_2);
                    under[1].setBackgroundColor(getResources().getColor(R.color.soft_grey));

                }else if(position == 1 ){
                    title[1] = (TextView) findViewById(R.id.btn_2);
                    title[1].setTextColor(getResources().getColor(R.color.app_default));
                    under[1] = (ImageView) findViewById(R.id.underline_2);
                    under[1].setBackgroundColor(getResources().getColor(R.color.app_default));

                    title[0] = (TextView) findViewById(R.id.btn_1);
                    title[0].setTextColor(getResources().getColor(R.color.soft_grey));
                    under[0] = (ImageView) findViewById(R.id.underline_1);
                    under[0].setBackgroundColor(getResources().getColor(R.color.soft_grey));
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    void Findid() {
        viewpager = (ViewPager) findViewById(R.id.view_pager1);

        charge_tab = (RelativeLayout) findViewById(R.id.credit_tab);
        charge_tab.setOnClickListener(btnListener);
        list_tab = (RelativeLayout) findViewById(R.id.list_tab);
        list_tab.setOnClickListener(btnListener);

        back = (RelativeLayout) findViewById(R.id.back_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Page_Credit.this, Page_Main.class);
                intent.putExtra("from","credit");
                startActivity(intent);
            }
        });

    }

    private void setCurrentInflateItem(int type) {
        if (type == 0) {
            viewpager.setCurrentItem(0);
        } else if (type == 1) {
            viewpager.setCurrentItem(1);
        }
    }


    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {


            switch (v.getId()) {
                case R.id.credit_tab:
                    title[0] = (TextView) findViewById(R.id.btn_1);
                    title[0].setTextColor(getResources().getColor(R.color.app_default));
                    under[0] = (ImageView) findViewById(R.id.underline_1);
                    under[0].setBackgroundColor(getResources().getColor(R.color.app_default));

                    title[1] = (TextView) findViewById(R.id.btn_2);
                    title[1].setTextColor(getResources().getColor(R.color.soft_grey));
                    under[1] = (ImageView) findViewById(R.id.underline_2);
                    under[1].setBackgroundColor(getResources().getColor(R.color.soft_grey));
                    setCurrentInflateItem(0);
                    break;

                case R.id.list_tab:
                    title[1] = (TextView) findViewById(R.id.btn_2);
                    title[1].setTextColor(getResources().getColor(R.color.app_default));
                    under[1] = (ImageView) findViewById(R.id.underline_2);
                    under[1].setBackgroundColor(getResources().getColor(R.color.app_default));

                    title[0] = (TextView) findViewById(R.id.btn_1);
                    title[0].setTextColor(getResources().getColor(R.color.soft_grey));
                    under[0] = (ImageView) findViewById(R.id.underline_1);
                    under[0].setBackgroundColor(getResources().getColor(R.color.soft_grey));
                    setCurrentInflateItem(1);
                    break;
            }
        }
    };



}





