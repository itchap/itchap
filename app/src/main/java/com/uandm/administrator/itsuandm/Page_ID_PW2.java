package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.opengl.ETC1;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.util.Util;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.regex.Pattern;

import static android.view.View.VISIBLE;

public class Page_ID_PW2 extends AppCompatActivity {
    private TextView btn_setting,btn_login,tv_id, confirm;
    private RelativeLayout btn_back;
    private LinearLayout content;
    private String result, name, phone, json;
    private EditText edit_pw, edit_repw;
    private boolean is_exist = false;
    private static final String ERROR_CODE_0 = "fail to loading module";
    private static final String ERROR_CODE_1 = "irregular access time termination";
    private static final String ERROR_CODE_2 = "irregular access ip mismatch";

    private String filterpw = "^(?=.*[a-zA-Z]+)(?=.*[!@#$%^*+=-]|.*[0-9]+).{6,20}$";
    private Pattern pspw = Pattern.compile(filterpw);

    private int dpi;
    private DisplayMetrics dm ;
    int width;
    int height ;
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_id_pw2);
        Intent intent = getIntent();
        result = intent.getStringExtra("result");
        name = intent.getStringExtra("name");
        phone = intent.getStringExtra("phone");

        Findid();

        if(result.equals("success")){
            try{
                json = new Util_PHP(Page_ID_PW2.this).execute(name,phone,"getdata_idpw","http://175.126.38.139/php/getdata_idpw.php").get();
            }catch (Exception e){
                e.printStackTrace();
            }

            if(json.equals("id not exist")){
                is_exist = false;
            }else{

                tv_id.setText(json+" 입니다");
                is_exist = true;
            }

        } else if (result.equals(ERROR_CODE_0)) {

        } else if(result.equals(ERROR_CODE_1)){

        } else if(result.equals(ERROR_CODE_2)){

        }



    }

    public void Findid(){
        btn_back = (RelativeLayout)findViewById(R.id.id_pw_back_btn);
        btn_back.setOnClickListener(listener);
        btn_setting = (TextView)findViewById(R.id.id_pw_setting);
        btn_setting.setOnClickListener(listener);
        btn_login = (TextView)findViewById(R.id.id_pw_login);
        btn_login.setOnClickListener(listener);

        tv_id = (TextView)findViewById(R.id.id_pw2_id);

       // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

    }
    void hide_keyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit_pw.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(edit_repw.getWindowToken(), 0);
    }

    private int pw_check(){
        if (!edit_pw.getText().toString().equals(edit_repw.getText().toString())) {
          return 0;
        } else if (!pspw.matcher(edit_pw.getText().toString()).matches()) {
            return 1;
        }else{
            return 2;
        }
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent ;
            switch (v.getId()){
                case R.id.id_pw_back_btn:
                    finish();
                    break;
                case R.id.id_pw_setting:
                    if(is_exist) {

                        dm = getApplicationContext().getResources().getDisplayMetrics();
                        width = dm.widthPixels;
                        height = dm.heightPixels;
                        dpi = getResources().getDisplayMetrics().densityDpi;



                        Log.e("asdfsadgsgsdaf",width+" "+ height+ " "+ dpi+" " +width/dpi);
                        final Dialog dialog = new Dialog(Page_ID_PW2.this);

                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.l_id_pw_dialog);

                        edit_pw = (EditText) dialog.findViewById(R.id.pw_edit);
                        edit_repw = (EditText) dialog.findViewById(R.id.repw_edit);
                        confirm = (TextView) dialog.findViewById(R.id.confirm);
                        content = (LinearLayout)dialog.findViewById(R.id.content);



                        content.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                hide_keyboard();
                            }
                        });

                        confirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String result = null;
                                int type = pw_check();
                                if(type == 0){
                                    new Util_Dialog(Page_ID_PW2.this,getResources().getDrawable(R.drawable.icon_warning),"비밀번호가 일치하지 않습니다.","확인","dismiss").show();
                                }else if(type == 1 ){
                                    new Util_Dialog(Page_ID_PW2.this,getResources().getDrawable(R.drawable.icon_warning),"비밀번호는 6자 이상, 영어,숫자 조합입니다.","확인","dismiss").show();
                                }else if(type == 2){

                                    try{
                                        result = new Util_PHP(Page_ID_PW2.this).execute(edit_pw.getText().toString(),name,phone,"update_pw","http://175.126.38.139/php/update_pw.php").get();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }


                                    if(result.equals("success")){
                                        dialog.dismiss();
                                        new Util_Dialog(Page_ID_PW2.this,getResources().getDrawable(R.drawable.icon_flower),"비밀번호가 변경되었습니다.","확인","dismiss").show();
                                    }else {
                                        dialog.dismiss();
                                        new Util_Dialog(Page_ID_PW2.this, getResources().getDrawable(R.drawable.icon_warning), "오류가 발생했습니다. 잠시후 시도해주세요.", "확인", "dismiss").show();
                                    }

                                }

                            }
                        });

                        dialog.show();
                    }else{
                        new Util_Dialog(Page_ID_PW2.this,getResources().getDrawable(R.drawable.icon_warning),"회원가입을 해주세요.","확인","dismiss").show();
                    }

                    break;
                case R.id.id_pw_login:
                    intent = new Intent(Page_ID_PW2.this, Page_Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    break;



            }
        }
    };
}


