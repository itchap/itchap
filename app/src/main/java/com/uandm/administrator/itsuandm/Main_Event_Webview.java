package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import com.tsengvn.typekit.TypekitContextWrapper;

public class Main_Event_Webview extends AppCompatActivity {
    private WebView webview;
    private String uri;
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_event_webview);
        Intent intent = getIntent(); // 값을 받아온다.
        String uri = intent.getStringExtra("uri");
        //  Intent intent = getIntent();
        //   uri = intent.getStringExtra("uri");
        Log.d("uri22", uri);
        webview = (WebView) findViewById(R.id.event_webview); //웹뷰 생성
        webview.getSettings().setJavaScriptEnabled(true); //자바 스크립트 enable
        webview.setWebViewClient(new WebViewClient() { //웹뷰 클라이언트(주소창 없애기 위해)

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        webview.loadUrl(uri); //주소입력
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig); //화면 전환시 처음페이지로 이동하지 않기 위해
    }

    public boolean onKeyDown(int KeyCode, KeyEvent event) { //버튼
        super.onKeyDown(KeyCode, event);
        if (event.getAction() == KeyEvent.ACTION_DOWN) { //눌렀을때
            if (KeyCode == KeyEvent.KEYCODE_BACK) { //뒤로가기버튼
                finish(); //현재 엑티비티 종료
            }
        }
        return false;
    }
}
