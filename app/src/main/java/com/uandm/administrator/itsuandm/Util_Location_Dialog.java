package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.tsengvn.typekit.TypekitContextWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by u on 2016-12-27.
 */

public class Util_Location_Dialog extends AppCompatActivity {
    private RecyclerView recyclerview;
    private String json;
    private JSONArray jsonArray;
    private String Do, Si;
    private TextView btn_confirm, btn_cancel;
//    private Struct_Filter struct_filter;
    private List<Filter_Loc_Adapter.PeopleListItem>  items;
    private Filter_Loc_Adapter adapter;
    private int status;



    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_dialog);

        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        btn_cancel = (TextView) findViewById(R.id.loc_cancel);
        btn_confirm = (TextView) findViewById(R.id.loc_confirm);
        items  = new ArrayList<>();

        try {
            json = new Util_PHP(Util_Location_Dialog.this).execute("0", "getdata_loction", "http://175.126.38.139/php/getdata_location.php").get();

        } catch (Exception e) {
            e.printStackTrace();
        }

        parse(json);


        btn_cancel.setOnClickListener(listener);
        btn_confirm.setOnClickListener(listener);

        adapter = new Filter_Loc_Adapter(Util_Location_Dialog.this,items);
        recyclerview.setAdapter(adapter);
    }

    private void parse(String json) {

        try {
            JSONObject jsonObj2 = new JSONObject(json);
            jsonArray = jsonObj2.getJSONArray("result");
             String temp_do = "초기";
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject c = jsonArray.getJSONObject(i);
                if(temp_do.equals(c.getString("do"))){
                    items.add(new Filter_Loc_Adapter.PeopleListItem("0",c.getString("si")));
                }else{
                    if(c.getString("status").equals("1")){
                        items.add(new Filter_Loc_Adapter.PeopleListItem(c.getString("do")));
                    }else {
                        items.add(new Filter_Loc_Adapter.PeopleListItem(c.getString("do")));
                        items.add(new Filter_Loc_Adapter.PeopleListItem("0",c.getString("si")));
                    }

                }
                temp_do=c.getString("do");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.loc_cancel:
                    intent = new Intent();
                    intent.putExtra("keey","/");
                    setResult(0,intent);
                    finish();
                    break;
                case R.id.loc_confirm:
                    if(adapter.loc[0] == null) {
                        intent = new Intent();
                        intent.putExtra("keey","/");
                        setResult(0,intent);
                        finish();
                    }else if(adapter.loc[1] == null) {
                        intent = new Intent();
                        intent.putExtra("keey",adapter.loc[0]);
                        setResult(0,intent);
                        finish();
                    }else if(adapter.loc[2] == null) {
                        intent = new Intent();
                        intent.putExtra("keey",adapter.loc[0]+"/"+adapter.loc[1]);
                        setResult(0,intent);
                        finish();
                    } else {
                        intent = new Intent();
                        intent.putExtra("keey",adapter.loc[0]+"/"+adapter.loc[1]+"/"+adapter.loc[2]);
                        setResult(0,intent);
                        finish();
                    }


                    break;
            }
        }
    };

}
