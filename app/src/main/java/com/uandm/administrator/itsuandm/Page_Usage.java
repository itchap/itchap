package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.ArrayList;

public class Page_Usage extends AppCompatActivity {

    private RelativeLayout back_btn;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private WebView web_intro;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    /////
    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_usage);

        pref = getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();

        back_btn = (RelativeLayout) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(listener);
        web_intro = (WebView)findViewById(R.id.web_intro);
        web_intro.getSettings().setJavaScriptEnabled(true); //자바 스크립트 enable
        web_intro.setWebViewClient(new WebViewClient() { //웹뷰 클라이언트(주소창 없애기 위해)

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        web_intro.loadUrl("http://175.126.38.139/how_to_use/intro.html"); //주소입력
    }




    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig); //화면 전환시 처음페이지로 이동하지 않기 위해
    }

    public boolean onKeyDown(int KeyCode, KeyEvent event) { //버튼
        super.onKeyDown(KeyCode, event);
        if (event.getAction() == KeyEvent.ACTION_DOWN) { //눌렀을때
            if (KeyCode == KeyEvent.KEYCODE_BACK) { //뒤로가기버튼
                editor.putBoolean("appFrist", false);
                editor.commit();
                finish(); //현재 엑티비티 종료
            }
        }
        return false;
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.back_btn:
                    editor.putBoolean("appFrist", false);
                    editor.commit();
                    finish();
                    break;

            }
        }
    };
}
