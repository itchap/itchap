package com.uandm.administrator.itsuandm;

import android.*;
import android.Manifest;
import android.app.AlertDialog;
import android.content.*;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.bumptech.glide.Glide;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

public class Page_Review extends AppCompatActivity {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private ImageView back, photo,photo_imgg;
    private TextView add_photo, btn_next, add_photo_title;
    private EditText contents;
    private RelativeLayout main, rel_img;
    private InputMethodManager imm;
    private String absolutePath;

    private Util_Dialog dialog;

    public static Uri mImageCaptureUri;
    public static Uri outputFileUri;
    private  Uri resultt ;
    private Uri photo_uri;
    private static final int PICK_FROM_CAMERA = 0;
    private static final int PICK_FROM_ALBUM = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE =  1;
    private static final int PERMISSIONS_REQUEST_CAMERA = 0;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_review);
        pref = this.getSharedPreferences("login", 0);
        editor = pref.edit();

        main = (RelativeLayout) findViewById(R.id.review_main);
        main.setOnClickListener(btnlistener);
        rel_img = (RelativeLayout) findViewById(R.id.rel_img);
        back = (ImageView) findViewById(R.id.back_btn_img);
        back.setOnClickListener(btnlistener);
        photo = (ImageView) findViewById(R.id.photo);
        photo_imgg = (ImageView) findViewById(R.id.img);
        add_photo = (TextView) findViewById(R.id.add_photo);
        add_photo.setOnClickListener(btnlistener);
        add_photo_title = (TextView) findViewById(R.id.photo_add_title);
        btn_next = (TextView) findViewById(R.id.btn_next);
        btn_next.setOnClickListener(btnlistener);
        contents = (EditText) findViewById(R.id.et_content);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        contents.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (contents.getText().toString().length() >= 100) {
                    dialog = new Util_Dialog(Page_Review.this, getResources().getDrawable(R.drawable.icon_lastpage), "최대 100자까지 입력 가능합니다!", "확인", "dismiss");
                    dialog.show();
                }
            }
        });
    }

    View.OnClickListener btnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.back_btn_img:
                    finish();
                    break;
                case R.id.add_photo:
                    addPhoto(v);
                    break;
                case R.id.btn_next:
                    if (add_photo.getText().equals("사진등록")) {
                        dialog = new Util_Dialog(Page_Review.this, getResources().getDrawable(R.drawable.icon_lastpage), "사진을 등록해주세요", "확인", "dismiss");
                        dialog.show();
                    } else if (contents.getText().toString().equals("") || contents.getText().toString().equals(null)) {
                        dialog = new Util_Dialog(Page_Review.this, getResources().getDrawable(R.drawable.icon_lastpage), "내용을 입력해주세요", "확인", "dismiss");
                        dialog.show();
                    } else if (contents.getText().toString().length() >= 100) {
                        dialog = new Util_Dialog(Page_Review.this, getResources().getDrawable(R.drawable.icon_lastpage), "100자 이내로 입력해주세요", "확인", "dismiss");
                        dialog.show();
                    } else {
                        try {
                            String result = new Util_PHP(Page_Review.this).execute(pref.getString("nick", ""), mImageCaptureUri.getPath().toString(), contents.getText().toString(),  getIntent().getStringExtra("alram_id"),"insert_review", "http://175.126.38.139/php/insert_review.php").get();
                            if (result.equals("okok")) {
                                dialog = new Util_Dialog(Page_Review.this, getResources().getDrawable(R.drawable.icon_lastpage), "등록이 완료되었습니다", "확인", "to_main");
                                dialog.show();
                            } else {
                                dialog = new Util_Dialog(Page_Review.this, getResources().getDrawable(R.drawable.icon_lastpage), "등록에 실패했습니다", "확인", "dismiss");
                                dialog.show();
                            }
                        } catch (Exception e) {
                        }
                    }
                    break;
                case R.id.review_main:
                    imm.hideSoftInputFromWindow(contents.getWindowToken(), 0);
                    break;
            }
        }
    };


    private void addPhoto(View v) {

        String path = Environment.getExternalStorageDirectory().getAbsolutePath();

        // 폴더명 및 파일명
        String folderPath = path + File.separator + "itchap";
        String filePath = File.separator + "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        File fileFolderPath = new File(folderPath);
        fileFolderPath.mkdir();

        // 파일 이름 지정
        File file = new File(filePath);
        outputFileUri = Uri.fromFile(file);

        final CharSequence[] items = new CharSequence[]{"앨범선택", "취소"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(v.getContext());
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (items[which].equals("사진촬영")) {
                    if(ContextCompat.checkSelfPermission(Page_Review.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                        if (ActivityCompat.shouldShowRequestPermissionRationale(Page_Review.this, android.Manifest.permission.CAMERA)) {

                            // ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                            // 사용자가 임의로 권한을 취소
                            // 다이어로그같은것을 띄워서 사용자에게 해당 권한이 필요한 이유에 대해 설명합니다
                            // 해당 설명이 끝난뒤 requestPermissions()함수를 호출하여 권한허가를 요청해야 합니다
                            ActivityCompat.requestPermissions(Page_Review.this,
                                    new String[]{android.Manifest.permission.CAMERA},
                                    PERMISSIONS_REQUEST_CAMERA);
                        } else {
                            // 최초 권한 요청
                            ActivityCompat.requestPermissions(Page_Review.this,
                                    new String[]{android.Manifest.permission.CAMERA},
                                    PERMISSIONS_REQUEST_CAMERA);
                            // 필요한 권한과 요청 코드를 넣어서 권한허가요청에 대한 결과를 받아야 합니다
                        }
                    }else{
                        // 권한이 존재하면
                        doTakePhotoAction();
                    }
                } else if (items[which].equals("앨범선택")) {
                    if(ContextCompat.checkSelfPermission(Page_Review.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        if (ActivityCompat.shouldShowRequestPermissionRationale(Page_Review.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            // ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                            // 사용자가 임의로 권한을 취소
                            // 다이어로그같은것을 띄워서 사용자에게 해당 권한이 필요한 이유에 대해 설명합니다
                            // 해당 설명이 끝난뒤 requestPermissions()함수를 호출하여 권한허가를 요청해야 합니다
                            ActivityCompat.requestPermissions(Page_Review.this,
                                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE);
                        } else {
                            // 최초 권한 요청
                            ActivityCompat.requestPermissions(Page_Review.this,
                                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE);
                            // 필요한 권한과 요청 코드를 넣어서 권한허가요청에 대한 결과를 받아야 합니다
                        }
                    }else{
                        // 권한이 존재하면
                        doTakeAlbumAction();
                    }

                } else
                    dialog.dismiss();
            }
        });
        dialog.show();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSIONS_REQUEST_CAMERA :
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // 권한 허가
                    // 해당 권한을 사용해서 작업을 진행할 수 있습니다
                    doTakePhotoAction();
                } else {
                    // 권한 거부
                    // 사용자가 해당권한을 거부했을때 해주어야 할 동작을 수행합니다
                    dialog = new Util_Dialog(Page_Review.this, getResources().getDrawable(R.drawable.icon_warning), "동의해주셔야 이용이 가능합니다. ", "확인","dismiss");
                    dialog.show();
                }
                return;
            case PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE:
                for(int i=0; i<permissions.length; i++){
                    String permi = permissions[i];
                    int result = grantResults[i];

                    if (permi.equals(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if(result == PackageManager.PERMISSION_GRANTED){
                            doTakeAlbumAction();
                        }else{

                        }
                    } else {
                        // 권한 거부
                        // 사용자가 해당권한을 거부했을때 해주어야 할 동작을 수행합니다
                        dialog = new Util_Dialog(Page_Review.this, getResources().getDrawable(R.drawable.icon_warning), "동의해주셔야 이용이 가능합니다. ", "확인","dismiss");
                        dialog.show();
                    }
                    return;
                }

        }


    }

    // 카메라에서 가져오기
    private void doTakePhotoAction() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    // 앨범에서 가져오기
    private void doTakeAlbumAction() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case PICK_FROM_ALBUM:
            case PICK_FROM_CAMERA: {
                try {
                    resultt = data.getData();
                    Log.e("result_camera",""+resultt);
                    Cursor c = getContentResolver().query(resultt, null,null,null,null);
                    c.moveToNext();
                    String path = c.getString(c.getColumnIndex(MediaStore.MediaColumns.DATA));
                    mImageCaptureUri  = Uri.fromFile(new File(path));
                    c.close();
                    if (resultt != null) {
                        Glide.with(Page_Review.this).load(resultt).asBitmap().into(photo);
                        add_photo_title.setText("");
                        add_photo.setText("사진 바꾸기");
                        rel_img.setBackground(getResources().getDrawable(R.drawable.white));
                        photo_imgg.setVisibility(View.GONE);
                    } else {
                        Log.e("result_camera",""+"elseee");
                    }

                } catch (Exception e) {
                }
                break;
            }
        }
    }


}
