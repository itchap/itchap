package com.uandm.administrator.itsuandm;


import android.*;
import android.Manifest;
import android.app.AlertDialog;
import android.content.*;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.bumptech.glide.Glide;
import com.tsengvn.typekit.TypekitContextWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import static android.view.View.VISIBLE;

public class Modify_Profile extends AppCompatActivity {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String id = null;
    private Util_ImageView img1, img2, img3;
    private TextView like_1, like_2, like_3, btn_confirm;
    private ImageView del_1, del_2, del_3;
    private Spinner spin_height, spin_weight;

    private String json, img1_uri, img2_uri, img3_uri, like1, like2, like3, height, weight, job;
    private JSONArray jsonArray;

    private EditText edit_tag_like, edit_job;
    private int input_no = 3, input_no_ = 3;

    private boolean on_keyboard = false;

    private RelativeLayout content, btn_back;

    private Util_Dialog custom_dial;
    private Util_SpinnerAdapter spin_height_adapter, spin_weight_adapter;

    public static Uri mImageCaptureUri;
    public static Uri outputFileUri;
    private Uri photouri[] = new Uri[3];
    private static final int PICK_FROM_CAMERA = 0;
    private static final int PICK_FROM_ALBUM = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 0;
    private static final int PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int PERMISSIONS_REQUEST_CAMERA = 2;
    private int sel_photho = 0;
    private boolean change_photo[] = new boolean[3];

    private int  spin_set_pos_height=0;
    private int  spin_set_pos_weight=0;


    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_modify_profile);

        pref = getSharedPreferences("login", 0);
        editor = pref.edit();
        id = pref.getString("id", null);

        try {
            json = new Util_PHP(Modify_Profile.this).execute(id, "Modify_getdata", "http://175.126.38.139/php/getdata_modify.php").get();
            //json = new getData().execute(id).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        parse(json);

        String[] spin_height_item = new Spinner_data_init().init_height();
        String[] spin_weight_item = new Spinner_data_init().init_weight();

        Findid();
        if (img1_uri != null && img2_uri != null && img3_uri != null) {
            Glide.with(this).load(Uri.parse(img1_uri)).asBitmap().into(img1);
            Glide.with(this).load(Uri.parse(img2_uri)).asBitmap().into(img2);
            Glide.with(this).load(Uri.parse(img3_uri)).asBitmap().into(img3);
        }

        spin_height_adapter = new Util_SpinnerAdapter(this,
                android.R.layout.simple_spinner_dropdown_item, spin_height_item);
        spin_weight_adapter = new Util_SpinnerAdapter(this,
                android.R.layout.simple_spinner_dropdown_item, spin_weight_item);

        for(int i =0 ;i < spin_height_item.length;i++){
            if(spin_height_item[i].equals(height)){
                spin_set_pos_height=i;
            }
        }
        for(int i =0 ;i < spin_weight_item.length;i++){
            if(spin_weight_item[i].equals(weight)){
                spin_set_pos_weight=i;
            }
        }

        spin_height.setAdapter(spin_height_adapter);
        spin_weight.setAdapter(spin_weight_adapter);
        spin_height.setSelection(spin_set_pos_height);
        spin_weight.setSelection(spin_set_pos_weight);

    }

    void Findid() {

        content = (RelativeLayout) findViewById(R.id.l_modify_profile);
        content.setOnClickListener(listener);
        img1 = (Util_ImageView) findViewById(R.id.photo1);
        img1.setType(11);
        img1.setOnClickListener(listener);
        img2 = (Util_ImageView) findViewById(R.id.photo2);
        img2.setType(11);
        img2.setOnClickListener(listener);
        img3 = (Util_ImageView) findViewById(R.id.photo3);
        img3.setType(11);
        img3.setOnClickListener(listener);

        edit_tag_like = (EditText) findViewById(R.id.edit_input_like);
        edit_tag_like.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm2.hideSoftInputFromWindow(edit_tag_like.getWindowToken(), 0);
                    if (edit_tag_like.getText().toString() == null || edit_tag_like.getText().toString().equals("")) {

                    } else {
                        if (input_no == 0) {
                            like_1.setText(edit_tag_like.getText());
                            like1 = edit_tag_like.getText().toString();
                            like_1.setVisibility(VISIBLE);
                            del_1.setVisibility(VISIBLE);
                            del_1.setOnClickListener(del_onclick);
                            edit_tag_like.setText("");
                            edit_tag_like.setHint("");
                            input_no = 1;
                            input_no_ = 1;
                        } else if (input_no == 1) {
                            like_2.setText(edit_tag_like.getText());
                            like_2.setVisibility(VISIBLE);
                            like2 = edit_tag_like.getText().toString();
                            del_2.setVisibility(VISIBLE);
                            del_2.setOnClickListener(del_onclick);
                            edit_tag_like.setText("");
                            edit_tag_like.setHint("");
                            input_no_ = 2;
                            input_no = 2;
                        } else if (input_no == 2) {
                            like_3.setText(edit_tag_like.getText());
                            like3 = edit_tag_like.getText().toString();
                            like_3.setVisibility(VISIBLE);
                            del_3.setVisibility(VISIBLE);
                            del_3.setOnClickListener(del_onclick);
                            edit_tag_like.setText("");
                            edit_tag_like.setHint("");
                            input_no = 3;
                            input_no_ = 3;
                        } else if (input_no == 3) {
                            edit_tag_like.setText("");
                            edit_tag_like.setHint("");
                            custom_dial = new Util_Dialog(Modify_Profile.this, getResources().getDrawable(R.drawable.icon_exit), "관심사는 3가지만 입력해주세요", "확인", "dismiss");
                            custom_dial.show();
                        }
                    }
                }
                return false;
            }
        });
        edit_tag_like.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {

                }
                return false;
            }
        });
        edit_tag_like.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                on_keyboard = true;
                return false;
            }
        });

        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter.LengthFilter(6);
        edit_tag_like.setFilters(filters);

        like_1 = (TextView) findViewById(R.id.like_1);
        like_1.setVisibility(View.VISIBLE);
        like_1.setText(like1);
        like_2 = (TextView) findViewById(R.id.like_2);
        like_2.setVisibility(View.VISIBLE);
        like_2.setText(like2);
        like_3 = (TextView) findViewById(R.id.like_3);
        like_3.setVisibility(View.VISIBLE);
        like_3.setText(like3);

        del_1 = (ImageView) findViewById(R.id.del_1);
        del_1.setVisibility(View.VISIBLE);
        del_1.setOnClickListener(del_onclick);
        del_2 = (ImageView) findViewById(R.id.del_2);
        del_2.setVisibility(View.VISIBLE);
        del_2.setOnClickListener(del_onclick);
        del_3 = (ImageView) findViewById(R.id.del_3);
        del_3.setVisibility(View.VISIBLE);
        del_3.setOnClickListener(del_onclick);

        edit_job = (EditText) findViewById(R.id.edit_input_job);
        edit_job.setText(job);

        btn_back = (RelativeLayout) findViewById(R.id.modify_back_btn);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_confirm = (TextView) findViewById(R.id.modify_confirm);
        btn_confirm.setOnClickListener(listener);

        spin_height = (Spinner) findViewById(R.id.m_spinner_height);
        spin_weight = (Spinner) findViewById(R.id.m_spinner_weight);

    }

    private void parse(String data) {
        try {
            JSONObject jsonObj = new JSONObject(data);
            jsonArray = jsonObj.getJSONArray("result");

            Log.e("Modify", "" + jsonArray + "  " + jsonArray.length());

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject c = jsonArray.getJSONObject(i);

                img1_uri = c.getString("photo_main");
                img2_uri = c.getString("photo_2");
                img3_uri = c.getString("photo_3");

                like1 = c.getString("like_1");
                like2 = c.getString("like_2");
                like3 = c.getString("like_3");

                weight = c.getString("weight");
                height = c.getString("height");

                job = c.getString("job");

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    void hide_keyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit_job.getWindowToken(), 0);
    }


    private void addPhoto() {

// 폴더명 및 파일명
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();

        String folderPath = path + File.separator + "itchap";
        String filePath = path + File.separator + "itchap" + File.separator + "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        File fileFolderPath = new File(folderPath);
        fileFolderPath.mkdir();

        // 파일 이름 지정
        File file = new File(filePath);
        outputFileUri = Uri.fromFile(file);

        Log.e("asdfadsf", "" + outputFileUri);

        final CharSequence[] items = new CharSequence[]{"앨범선택", "취소"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(Modify_Profile.this);
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (items[which].equals("사진촬영")) {
                    doTakePhotoAction();
                } else if (items[which].equals("앨범선택")) {
                    doTakeAlbumAction();
                } else
                    dialog.dismiss();
            }
        });
        dialog.show();

    }

    // 카메라에서 가져오기
    private void doTakePhotoAction() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // 임시로 사용할 파일의 경로를 생성
        // String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";

        //mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));


        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, outputFileUri);

        // 특정기기에서 사진을 저장못하는 문제가 있어 다음을 주석처리 합니다.
        //intent.putExtra("return-data", true);
        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    // 앨범에서 가져오기
    private void doTakeAlbumAction() {
        // 앨범 호출
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case CROP_FROM_CAMERA: {
                // 크롭이 된 이후의 이미지를 넘겨 받습니다.
                // 이미지뷰에 이미지를 보여준다거나 부가적인 작업 이후에
                // 임시 파일을 삭제합니다.
                Log.e("td", " " + outputFileUri.toString());
                final Bundle extras = data.getExtras();
                if (extras != null) {
                    Bitmap photo = extras.getParcelable("data");

                    if (sel_photho == 1) {
                        img1.setImageBitmap(photo, 11);
                        //img[0] = photo;
                        change_photo[0] = true;
                        photouri[0] = outputFileUri;
                    } else if (sel_photho == 2) {
                        img2.setImageBitmap(photo, 11);
                        //img[1] = photo;
                        change_photo[1] = true;
                        photouri[1] = outputFileUri;
                    } else if (sel_photho == 3) {
                        img3.setImageBitmap(photo, 11);
                        //img[2] = photo;
                        change_photo[2] = true;
                        photouri[2] = outputFileUri;
                    }
                }


                break;
            }

            case PICK_FROM_ALBUM: {
                // 이후의 처리가 카메라와 같으므로 일단  break없이 진행합니다.
                // 실제 코드에서는 좀더 합리적인 방법을 선택하시기 바랍니다.
                mImageCaptureUri = data.getData();

                Intent intent = new Intent("com.android.camera.action.CROP");
                intent.setDataAndType(mImageCaptureUri, "image/*");

                intent.putExtra("outputX", 400);
                intent.putExtra("outputY", 400);
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("scale", true);
                intent.putExtra("return-data", true);

                intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, outputFileUri);


                startActivityForResult(intent, CROP_FROM_CAMERA);

                break;

            }

            case PICK_FROM_CAMERA: {
                // 이미지를 가져온 이후의 리사이즈할 이미지 크기를 결정합니다.
                // 이후에 이미지 크롭 어플리케이션을 호출하게 됩니다.

                Intent intent = new Intent("com.android.camera.action.CROP");
                intent.setDataAndType(outputFileUri, "image/*");

                intent.putExtra("outputX", 400);
                intent.putExtra("outputY", 400);
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("scale", true);
                intent.putExtra("return-data", true);

                intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, outputFileUri);

                startActivityForResult(intent, CROP_FROM_CAMERA);

                break;
            }
        }
    }


    View.OnClickListener del_onclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.del_1:
                    if (like_3.getVisibility() == View.VISIBLE) {
                        like_3.setVisibility(View.GONE);
                        del_3.setVisibility(View.GONE);
                        like_1.setText(like_2.getText());
                        like_2.setText(like_3.getText());
                        input_no = 2;
                        input_no_ = 2;
                    } else if (like_2.getVisibility() == View.VISIBLE) {
                        like_2.setVisibility(View.GONE);
                        del_2.setVisibility(View.GONE);
                        like_1.setText(like_2.getText());
                        input_no = 1;
                        input_no_ = 1;
                    } else {
                        like_1.setVisibility(View.GONE);
                        del_1.setVisibility(View.GONE);
                        like_1.setText("");
                        input_no = 0;
                        input_no_ = 0;
                    }
                    break;
                case R.id.del_2:
                    if (like_3.getVisibility() == View.VISIBLE) {
                        like_3.setVisibility(View.GONE);
                        del_3.setVisibility(View.GONE);
                        like_2.setText(like_3.getText());
                        input_no = 2;
                        input_no_ = 2;
                    } else {
                        like_2.setVisibility(View.GONE);
                        del_2.setVisibility(View.GONE);
                        like_2.setText("");
                        input_no = 1;
                        input_no_ = 1;
                    }
                    break;
                case R.id.del_3:
                    like_3.setVisibility(View.GONE);
                    like_3.setText("");
                    del_3.setVisibility(View.GONE);
                    input_no = 2;
                    input_no_ = 2;
                    break;
            }
        }
    };

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.modify_confirm:
                    Log.e("키,체형",""+spin_weight.getSelectedItem().toString()+"/"+spin_height.getSelectedItem().toString());
                    String image[] = new String[3];

                    job = edit_job.getText().toString();

                    for (int i = 0; i < 3; i++) {
                        image[i] = "no_change";
                        if (change_photo[i]) {
                            image[i] = photouri[i].getPath().toString();
                        }
                    }
                    if (like_3.getVisibility() == View.GONE) {
                        custom_dial = new Util_Dialog(Modify_Profile.this, getResources().getDrawable(R.drawable.icon_lastpage), "관심사가 올바르지 않습니다.", "확인", "dismiss");
                        custom_dial.show();
                    }else if(spin_weight.getSelectedItem().toString().equals("체형")){
                        custom_dial = new Util_Dialog(Modify_Profile.this, getResources().getDrawable(R.drawable.icon_lastpage), "체형을 선택해주세요", "확인", "dismiss");
                        custom_dial.show();
                    }else if(spin_height.getSelectedItem().toString().equals("키")){
                        custom_dial = new Util_Dialog(Modify_Profile.this, getResources().getDrawable(R.drawable.icon_lastpage), "키를 선택해주세요", "확인", "dismiss");
                        custom_dial.show();
                    }  else {
                        try {
                            json = new Util_PHP(Modify_Profile.this).execute(id, image[0], image[1], image[2], spin_height.getSelectedItem().toString(), spin_weight.getSelectedItem().toString(),
                                    like1, like2, like3, job, "Modify_update", "http://175.126.38.139/php/update_modify.php").get();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        custom_dial = new Util_Dialog(Modify_Profile.this, getResources().getDrawable(R.drawable.icon_check), "프로필이 수정되었습니다", "확인", "modify");
                        custom_dial.show();
                    }
                    break;

                case R.id.photo1:
                    sel_photho = 1;
                    addPhoto();
                    break;
                case R.id.photo2:
                    sel_photho = 2;
                    addPhoto();
                    break;
                case R.id.photo3:
                    sel_photho = 3;
                    addPhoto();
                    break;
                case R.id.l_modify_profile:
                    hide_keyboard();
                    if (on_keyboard) {
                        on_keyboard = false;
                        if (edit_tag_like.getText().toString() == null || edit_tag_like.getText().toString().equals("")) {

                        } else {
                            if (input_no_ == 0) {
                                like_1.setText(edit_tag_like.getText());
                                like_1.setVisibility(VISIBLE);
                                del_1.setVisibility(VISIBLE);
                                del_1.setOnClickListener(del_onclick);
                                edit_tag_like.setText("");
                                edit_tag_like.setHint("");
                                input_no = 1;
                                input_no_ = 1;
                            } else if (input_no_ == 1) {
                                like_2.setText(edit_tag_like.getText());
                                like_2.setVisibility(VISIBLE);
                                del_2.setVisibility(VISIBLE);
                                del_2.setOnClickListener(del_onclick);
                                edit_tag_like.setText("");
                                edit_tag_like.setHint("");
                                input_no = 2;
                                input_no_ = 2;
                            } else if (input_no_ == 2) {
                                like_3.setText(edit_tag_like.getText());
                                like_3.setVisibility(VISIBLE);
                                del_3.setVisibility(VISIBLE);
                                del_3.setOnClickListener(del_onclick);
                                edit_tag_like.setText("");
                                edit_tag_like.setHint("");
                                input_no = 3;
                                input_no_ = 3;
                            } else if (input_no_ == 3) {
                                edit_tag_like.setText("");
                                edit_tag_like.setHint("");
                                custom_dial = new Util_Dialog(Modify_Profile.this, getResources().getDrawable(R.drawable.icon_lastpage), "관심사는 3가지만 입력해주세요", "확인", "dismiss");
                                custom_dial.show();
                            }
                        }
                    }

            }
        }
    };


}
