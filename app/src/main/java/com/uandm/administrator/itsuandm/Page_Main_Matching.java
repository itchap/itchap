package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.content.Intent;

import android.content.SharedPreferences;
import android.graphics.Bitmap;

import android.graphics.Color;
import android.os.Bundle;

import android.support.v4.app.Fragment;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import com.bumptech.glide.request.target.SimpleTarget;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.thoughtbot.expandablerecyclerview.R.id.icon;


public class Page_Main_Matching extends Fragment {
    private static final int REQUEST_CODE_FOR_MATCHING = 11;
    private Bitmap[] bitmap_ = new Bitmap[4];
    private Struct_Filter struct_filter;
    private String json;
    private String team_owner, up_date, title, loc, date_time, age, cnt_like, cnt_team;
    private String img[];
    private String nick;
    private JSONArray jsonArray;

    private ArrayList<Page_Main_Matching_ListViewItem> item;
    private Page_Main_Matching_RecylerAdapter adapter;
    protected View view, view1;
    private RecyclerViewPager viewpager;
    private RelativeLayout btn_filter, btn_enroll, empty;

    private RequestManager requestManager;
    private int pos;
    private int prvpos;
    private int lastpos;
    private int all_data_row = 0;


    private static int firstVisibleInListview;
    private Util_Dialog dialog;
    boolean init = true;
    boolean init_ = true;
    private int type = 0;
    private int current_ = 0;
    private int currentPos = 0;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;


    public void refresh(int type) {
        if (type == 0) {
            adapter.removeall();
            pos = 0;
            try {
                json = new Util_PHP(view.getContext()).execute("0", nick, "Matching_getdata", "http://175.126.38.139/php/mat.php").get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            parse(json);
        } else {
            adapter.removeall();
            pos = 0;
            try {
                json = new Util_PHP((view.getContext())).execute("0", nick
                        , struct_filter.getDate(), struct_filter.getLoc(), struct_filter.getTeam_count()
                        , Integer.toString(struct_filter.getStart_age()), Integer.toString(struct_filter.getEnd_age())
                        , "Matching_getdata_filter", "http://175.126.38.139/php/getdata_matching_filter.php").get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            parse(json);
        }
    }

    @Override
    public void onResume() {
        Log.e("matching_resume", "resume");
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();

        item = new ArrayList<>();
        if (type == 0) {
            // 맨처음 초기 데이터
            all_data_row = 0;
            pos = 0;

            try {
                json = new Util_PHP(view.getContext()).execute("0", nick, "Matching_getdata", "http://175.126.38.139/php/mat.php").get();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (type == 1) {
            all_data_row = 0;
            pos = 0;
//
            try {
                json = new Util_PHP((view.getContext())).execute("0", nick
                        , struct_filter.getDate(), struct_filter.getLoc(), struct_filter.getTeam_count()
                        , Integer.toString(struct_filter.getStart_age()), Integer.toString(struct_filter.getEnd_age())
                        , "Matching_getdata_filter", "http://175.126.38.139/php/getdata_matching_filter.php").get();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        parse(json);

        if (all_data_row == 0) {
            viewpager.setVisibility(View.INVISIBLE);
            empty.setVisibility(View.VISIBLE);
        } else {
            viewpager.setVisibility(View.VISIBLE);
            empty.setVisibility(View.INVISIBLE);
        }
        /* DB에 있는 모든 방개설 된 정보 호출 */

        /* 팀원 카운트 만큼 이미지(대표이미지) 가져오기 */

        adapter = new Page_Main_Matching_RecylerAdapter(item, getContext(), requestManager);

        final LinearLayoutManager layout = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        //RecyclerViewPager.LayoutParams layoutParams = new RecyclerView.LayoutParams(new RecyclerView.LayoutParams(RecyclerViewPager.LayoutParams.MATCH_PARENT,RecyclerViewPager.LayoutParams.MATCH_PARENT));
        //viewpager.setLayoutParams(layoutParams);
        firstVisibleInListview = layout.findFirstVisibleItemPosition();
        viewpager.setLayoutManager(layout);

        viewpager.setAdapter(adapter);
        viewpager.scrollToPosition(currentPos);

        viewpager.addOnPageChangedListener(new RecyclerViewPager.OnPageChangedListener() {

            @Override
            public void OnPageChanged(int i, int currentPos) {
                Page_Main_Matching.this.currentPos = currentPos;
            }


        });
        viewpager.setOnTouchListener(new View.OnTouchListener() {
            boolean dragFlag = false; // 터치이벤트인지 확인
            boolean fristdrag = true;
            float startPos = 0;
            float endPos = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                current_ = viewpager.getCurrentPosition();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        dragFlag = true;
                        if (fristdrag) {
                            startPos = event.getX();
                            fristdrag = false;
                        }
                        break;
                    //누르고 뗄때 ACTION_UP
                    case MotionEvent.ACTION_UP:
                        endPos = event.getX();
                        fristdrag = true;

                        if (dragFlag) {
                            if (startPos > endPos) {

                                if (current_ + 1 == pos) {
                                    if (all_data_row == pos) {
                                        new Util_Dialog(getContext(), getResources().getDrawable(R.drawable.icon_lastpage), "마지막 페이지 입니다.", "확인", "처음으로", "last_page", viewpager).show();
                                    } else if (all_data_row > pos) {
                                        if (type == 0) {
                                            addLoad(0);
                                        } else if (type == 1) {
                                            addLoad(1);
                                            ;
                                            // 필터링된 데이터 add
                                        }
                                    }
                                }
                            } else if (startPos < endPos) {
                                if (current_ == 0 && viewpager.getScrollState() == viewpager.SCROLL_STATE_DRAGGING) {
                                    //new Util_Dialog(getContext(), getResources().getDrawable(R.drawable.icon_exit), "왼쪽", "확인", "dismiss").show();

                                    if (type == 0) {
                                        refresh(0);
                                    } else if (type == 1) {
                                        // 필터링된 데이터 add
                                        refresh(1);
                                    }
                                } else {

                                }
                            }
                        }
                        startPos = 0.0f;
                        endPos = 0.0f;
                        dragFlag = false;
                        break;
                }
                return false;
            }
        });

        type = 0;

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.l_matching_recyclerviewpager, container, false);

        requestManager = Glide.with(this);
        struct_filter = new Struct_Filter();

        pref = getContext().getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();
        nick = pref.getString("nick", "null");

        btn_enroll = (RelativeLayout) view.findViewById(R.id.matching_layout_enroll);
        btn_filter = (RelativeLayout) view.findViewById(R.id.matching_layout_filter);
        viewpager = (RecyclerViewPager) view.findViewById(R.id.matching_viewpager);

        empty = (RelativeLayout) view.findViewById(R.id.empty);

        btn_enroll.setOnClickListener(btnlistener);
        btn_filter.setOnClickListener(btnlistener);

        return view;
    }

    private void addLoad(int type) {
        Log.e("addload호출됐씁니당", "");
        Log.e("addload호출됐씁니당_pos", "" + pos);
        if (type == 0) {
            try {
                json = new Util_PHP(view.getContext()).execute(Integer.toString(pos), nick, "Matching_getdata", "http://175.126.38.139/php/mat.php").get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            parse(json);

        } else {
            try {
                json = new Util_PHP((view.getContext())).execute("0", nick
                        , struct_filter.getDate(), struct_filter.getLoc(), struct_filter.getTeam_count()
                        , Integer.toString(struct_filter.getStart_age()), Integer.toString(struct_filter.getEnd_age())
                        , "Matching_getdata_filter", "http://175.126.38.139/php/getdata_matching_filter.php").get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            parse(json);

        }

    }

    private void parse(String data) {
        try {
            JSONObject jsonObj = new JSONObject(data);
            jsonArray = jsonObj.getJSONArray("result");
            Log.e("matching", "" + jsonArray + "  " + jsonArray.length());

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject c = jsonArray.getJSONObject(i);
                date_time = c.getString("date_time");
                if (check_date(date_time)) {
                    continue;
                }
                team_owner = c.getString("team_owner");
                up_date = c.getString("up_date");
                title = c.getString("title");
                loc = c.getString("loc");

                age = c.getString("age");
                cnt_like = c.getString("cnt_like");
                cnt_team = c.getString("cnt_team");
                all_data_row = Integer.parseInt(c.getString("data_length"));
                img = new String[Integer.parseInt(cnt_team)];

                switch (Integer.parseInt(cnt_team)) {
                    case 1:
                        img[0] = c.getString("img1");
                        break;
                    case 2:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        break;
                    case 3:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        img[2] = c.getString("img3");
                        break;
                    case 4:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        img[2] = c.getString("img3");
                        img[3] = c.getString("img4");
                        break;
                }

                item.add(new Page_Main_Matching_ListViewItem(getContext(), team_owner, img, title, loc, date_time, age, cnt_like, Integer.parseInt(cnt_team)));
                pos++;
                Log.e("pos", pos + "");
                if (pos > 9) {
                    adapter.notifyItemInserted(item.size() - 1);
                    adapter.notifyDataSetChanged();
                    // Log.e("size",item.size()+""+pos);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_FOR_MATCHING:
                if (resultCode == 0) {
                    this.type = 1;
                    struct_filter = (Struct_Filter) data.getSerializableExtra("struct_filter");
                } else if (resultCode == 1) {
                }
                break;
        }
    }

    View.OnClickListener btnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.matching_layout_enroll:
                    intent = new Intent(view.getContext(), Page_Main_Matching_Enroll.class);
                    startActivity(intent);
                    break;
                case R.id.matching_layout_filter:
                    intent = new Intent(view.getContext(), Page_Main_Matching_Filter.class);
                    startActivityForResult(intent, REQUEST_CODE_FOR_MATCHING);
                    break;
            }
        }
    };

    boolean check_date(String date) {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");
        String today_ = format.format(today);

        int compare = today_.compareTo(date);

        if (compare > 0) {
            return true;
        } else
            return false;
    }


}