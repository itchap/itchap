package com.uandm.administrator.itsuandm;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by its on 2017-01-10.
 */
public class Util_PHP extends AsyncTask<String, Void, String> {

    Util_Progress_Dialog loading;
    HttpURLConnection conn = null;
    DataOutputStream dos = null;
    String lineEnd = "\r\n";
    String twoHyphens = "--";
    String boundary = "*****";
    Context context;
    FileInputStream fileInputStream1, fileInputStream2, fileInputStream3;
    int maxBufferSize = 1 * 1024 * 1024;

    Util_PHP(Context context) {
        this.context = context;
        loading = new Util_Progress_Dialog(context);
    }


    @Override
    protected void onPreExecute() {
        Log.e("adatper", "pre");
        loading.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loading.show();
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        Log.e("adatper", "post");
        loading.dismiss();
        super.onPostExecute(s);

        //Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }

    private void writeFormField(String fieldName, String fieldValue) {
        try {
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"" + fieldName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);
            dos.write(fieldValue.getBytes());
            dos.writeBytes(lineEnd);
        } catch (Exception e) {
            //System.out.println("AndroidUploader.writeFormField: got: " + e.getMessage());
            Log.e("ASDf", "AndroidUploader.writeFormField: " + e.getMessage());
        }
    }

    private void writeFileField(
            String fieldName,
            String fieldValue,
            FileInputStream fis) {
        try {
            // opening boundary line
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\""
                    + fieldName + "\";filename=\""
                    + fieldValue + "\"" + lineEnd);
            //dos.writeBytes("Content-Type: " + type +  lineEnd);
            dos.writeBytes(lineEnd);


            // create a buffer of  maximum size
            int bytesAvailable = fis.available();

            int bufferSize = Math.min(bytesAvailable, maxBufferSize);
            byte[] buffer = new byte[bufferSize];

            // read file and write it into form...
            int bytesRead = fis.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {

                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fis.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fis.read(buffer, 0, bufferSize);

            }

            // send multipart form data necesssary after file data...
            dos.writeBytes(lineEnd);
        } catch (Exception e) {
            //System.out.println("GeoPictureUploader.writeFormField: got: " + e.getMessage());
            Log.e("file", "AndroidUploader.writeFormField: got: " + e.getMessage());
        }
    }

    @Override
    protected String doInBackground(String... params) {

        try {

            URL url = new URL(params[params.length - 1]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(conn.getOutputStream());


            // 메인 데이터
            //
            if (params[params.length - 2].equals("getdata_main")) {
                writeFormField("id", params[0]);

                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }

                return sb.toString().trim();
            }
            /* 드로어 리프레쉬 */
            //
            if (params[params.length - 2].equals("refresh_drawer")) {
                writeFormField("nick", params[0]);
                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("refresh_drawer", sb.toString().trim());
                return sb.toString().trim();
            }

            /* 매칭 데이터 가져오기 */
            //
            if (params[params.length - 2].equals("login")) {
                writeFormField("id", params[0]);
                writeFormField("pw", params[1]);
                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("login", sb.toString().trim());
                return sb.toString().trim();
            }

            if (params[params.length - 2].equals("Matching_getdata")) {
                writeFormField("offset", params[0]);
                writeFormField("nick", params[1]);

                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("offset", sb.toString().toString());

                return sb.toString().trim();

            }
            if (params[params.length - 2].equals("location")) {
                writeFormField("do", params[0]);


                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("offset", sb.toString().toString());

                return sb.toString().trim();

            }
            /* 필터 데이터가져오기 */
            if (params[params.length - 2].equals("getdata_loction")) {

                writeFormField("type", params[0]);

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("getdata_loction", sb.toString().toString());

                return sb.toString().trim();

            }

            if (params[params.length - 2].equals("Matching_getdata_filter")) {

                writeFormField("offset", params[0]);
                writeFormField("nick", params[1]);
                writeFormField("date", params[2]);
                writeFormField("loc", params[3]);
                writeFormField("team_count", params[4]);
                writeFormField("start_age", params[5]);
                writeFormField("end_age", params[6]);


                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("Matching_getdata_filter", sb.toString().toString());

                return sb.toString().trim();

            }
            // /*프로필 수정*
            // 데이터 가져오기
            if (params[params.length - 2].equals("Modify_getdata")) {
                writeFormField("id", params[0]);
                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }

                return sb.toString().trim();
            }

            /* 프로필 수정 */
            // 데이터 업데이트
            if (params[params.length - 2].equals("Modify_update")) {

                writeFormField("id", params[0]);
                writeFormField("height", params[4]);
                writeFormField("weight", params[5]);
                writeFormField("like1", params[6]);
                writeFormField("like2", params[7]);
                writeFormField("like3", params[8]);
                writeFormField("job", params[9]);


                //직업
                //writeFormField("id", params[9]);

                if (!params[1].equals("no_change")) {
                    fileInputStream1 = new FileInputStream(new File(params[1]));
                    writeFileField("image1", params[1], fileInputStream1);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    fileInputStream1.close();
                }

                if (!params[2].equals("no_change")) {
                    fileInputStream2 = new FileInputStream(new File(params[2]));
                    writeFileField("image2", params[2], fileInputStream2);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    fileInputStream2.close();
                }

                if (!params[3].equals("no_change")) {
                    fileInputStream3 = new FileInputStream(new File(params[3]));
                    writeFileField("image3", params[3], fileInputStream3);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    fileInputStream3.close();
                }
                File f[] = new File[3];
                for (int i = 0; i < 3; i++) {
                    f[i] = new File(params[i + 1]);
                    if (f[i].exists())
                        f[i].delete();
                }

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("modify_update", sb.toString().trim());
                return sb.toString().trim();

            }

            /* 회원가입 */
            // 데이터 입력
            if (params[params.length - 2].equals("Insert_member")) {

                fileInputStream1 = new FileInputStream(new File(params[9]));
                fileInputStream2 = new FileInputStream(new File(params[10]));
                fileInputStream3 = new FileInputStream(new File(params[11]));

                writeFormField("id", params[0]);
                writeFormField("pw", params[1]);
                writeFormField("nick", params[2]);
                writeFormField("weight", params[3]);
                writeFormField("height", params[4]);
                writeFormField("like1", params[5]);
                writeFormField("like2", params[6]);
                writeFormField("like3", params[7]);
                writeFormField("gender", params[8]);
                writeFormField("name", params[12]);
                writeFormField("phone", params[13]);
                writeFormField("age", params[14]);
                writeFormField("nation", params[15]);
                writeFormField("job", params[16]);


                writeFileField("image1", params[9], fileInputStream1);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                writeFileField("image2", params[10], fileInputStream2);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                writeFileField("image3", params[11], fileInputStream3);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);


                File f[] = new File[3];
                for (int i = 0; i < 3; i++) {
                    f[i] = new File(params[i + 9]);
                    long l = 0;
                    if (f[i].exists())
                        l = f[i].length();
                    f[i].delete();
                }


                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("insert_member", sb.toString().trim());
                return sb.toString().trim();
            }

            /* 회원 탈퇴 */
            //
            if (params[params.length - 2].equals("delete_member")) {
                writeFormField("nick", params[0]);
                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("delete_member", sb.toString().trim());
                return sb.toString().trim();
            }
            // 탈퇴시 맴버에게 푸시
            if (params[params.length - 2].equals("delete_member_fcm")) {

                writeFormField("nick", params[0]);

                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("delete_member_fcm", sb.toString().trim());
                return sb.toString().trim();

            }

            /* 스토어 디테일 */
            if (params[params.length - 2].equals("Store_detail")) {
                writeFormField("id", params[0]);
                writeFormField("date", params[1]);
                writeFormField("type", params[2]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }

                return sb.toString().trim();

            }
            /* 매칭등록*/

            if (params[params.length - 2].equals("Insert_Enroll")) {

                writeFormField("team_owner", params[0]);
                writeFormField("up_date", params[1]);
                writeFormField("title", params[2]);
                writeFormField("store_id", params[3]);
                writeFormField("date_time", params[4]);
                writeFormField("type", params[5]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("insert_enroll", sb.toString().trim());

                return sb.toString().trim();


            }
            if (params[params.length - 2].equals("store")) {

                writeFormField("post_loc_1", params[0]);
                writeFormField("post_loc_2", params[1]);
                writeFormField("index", params[2]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("store", sb.toString().trim());

                return sb.toString().trim();


            }
            if (params[params.length - 2].equals("ing_coupon")) {
                writeFormField("nick", params[0]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("ing_coupon", sb.toString().trim());

                return sb.toString().trim();


            }
            if (params[params.length - 2].equals("credit_list")) {
                writeFormField("my_nick", params[0]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("credit_list", sb.toString().trim());

                return sb.toString().trim();


            }
            if (params[params.length - 2].equals("status_us")) {

                writeFormField("type", params[0]);
                writeFormField("nick", params[1]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("status_us", sb.toString().trim());

                return sb.toString().trim();


            }
            if (params[params.length - 2].equals("org_team_ing")) {

                writeFormField("nick", params[0]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("org_team_ing", sb.toString().trim());

                return sb.toString().trim();


            }
            if (params[params.length - 2].equals("feedback")) {

                writeFormField("nick_from", params[0]);
                writeFormField("type", params[1]);
                writeFormField("contents", params[2]);
                writeFormField("nick_to", params[3]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("feedback", sb.toString().trim());

                return sb.toString().trim();


            }

            if (params[params.length - 2].equals("getdata_alram")) {
                writeFormField("nick", params[0]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("getdata_alram", sb.toString().trim());
                return sb.toString().trim();
            }
            if (params[params.length - 2].equals("accept_team")) {
                writeFormField("nick", params[0]);
                writeFormField("to_nick", params[1]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("accept_team", sb.toString().trim());
                return sb.toString().trim();
            }
            if (params[params.length - 2].equals("reject_team")) {
                writeFormField("nick", params[0]);
                writeFormField("to_nick", params[1]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;
//
                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("reject_team", sb.toString().trim());
                return sb.toString().trim();
            }
            if (params[params.length - 2].equals("add_team_4")) {
                writeFormField("team_owner", params[0]);
                writeFormField("team_2", params[1]);
                writeFormField("team_3", params[2]);
                writeFormField("team_4", params[3]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("add_team_4", sb.toString().trim());
                return sb.toString().trim();
            }
            if (params[params.length - 2].equals("add_team_3")) {
                writeFormField("team_owner", params[0]);
                writeFormField("team_2", params[1]);
                writeFormField("team_3", params[2]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("add_team_3", sb.toString().trim());
                return sb.toString().trim();
            }
            if (params[params.length - 2].equals("add_team_2")) {
                writeFormField("team_owner", params[0]);
                writeFormField("team_2", params[1]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("add_team_2", sb.toString().trim());
                return sb.toString().trim();
            }

            if (params[params.length - 2].equals("getdata_my_team")) {
                writeFormField("my_nick", params[0]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("getdata_my_team", sb.toString().trim());
                return sb.toString().trim();
            }

            if (params[params.length - 2].equals("withdraw_team")) {
                writeFormField("my_nick", params[0]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("withdraw_team", sb.toString().trim());
                return sb.toString().trim();
            }

            if (params[params.length - 2].equals("Insert_token")) {

                writeFormField("Id", params[0]);
                writeFormField("Token", params[1]);

                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("Insert_token", sb.toString().trim());
                return sb.toString().trim();

            }


            if (params[params.length - 2].equals("getdata_my_team")) {
                writeFormField("my_nick", params[0]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("getdata_my_team", sb.toString().trim());
                return sb.toString().trim();
            }

            if (params[params.length - 2].equals("withdraw_team")) {
                writeFormField("my_nick", params[0]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("withdraw_team", sb.toString().trim());
                return sb.toString().trim();
            }

            if (params[params.length - 2].equals("do_you_have_a_team")) {

                writeFormField("my_nick", params[0]);

                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("do_you_have_a_team", sb.toString().trim());
                return sb.toString().trim();

            }
            if (params[params.length - 2].equals("credit_value")) {

                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("credit_value", sb.toString().trim());
                return sb.toString().trim();

            }
            if (params[params.length - 2].equals("home_event")) {

                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("home_event", sb.toString().trim());
                return sb.toString().trim();

            }
            if (params[params.length - 2].equals("push_y_n")) {
                writeFormField("nick", params[0]);
                writeFormField("push_y_n", params[1]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("push_y_n", sb.toString().trim());
                return sb.toString().trim();

            }
            if (params[params.length - 2].equals("get_push_y_n")) {
                writeFormField("nick", params[0]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("get_push_y_n", sb.toString().trim());
                return sb.toString().trim();

            }
            if (params[params.length - 2].equals("insert_review")) {
                fileInputStream1 = new FileInputStream(new File(params[1]));
                writeFormField("my_nick", params[0]);
                writeFormField("contents", params[2]);
                writeFormField("alram_id", params[3]);
                writeFileField("image", params[1], fileInputStream1);

                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                dos.flush();
                dos.close();
                fileInputStream1.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("insert_review", sb.toString().trim());
                return sb.toString().trim();
            }
            if (params[params.length - 2].equals("getdata_review")) {
                writeFormField("no", params[0]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;
                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("getdata_review", sb.toString().trim());
                return sb.toString().trim();
            }
            if (params[params.length - 2].equals("review_best")) {
                //  writeFormField("no", params[0]);
                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("review_best", sb.toString().trim());
                return sb.toString().trim();
            }

            if (params[params.length - 2].equals("like_check")) {

                writeFormField("my_nick", params[0]);
                writeFormField("nick", params[1]);
                writeFormField("team_cnt", params[2]);
                writeFormField("date", params[3]);

                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("like_check", sb.toString().trim());
                return sb.toString().trim();

            }
            if (params[params.length - 2].equals("like_check1")) {

                writeFormField("my_nick", params[0]);
                writeFormField("nick", params[1]);
                writeFormField("team_cnt", params[2]);
                writeFormField("date", params[3]);
                writeFormField("ment", params[4]);

                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("like_check1", sb.toString().trim());
                return sb.toString().trim();

            }
            if (params[params.length - 2].equals("team_cnt")) {

                writeFormField("nick", params[0]);
                writeFormField("type", params[1]);
                writeFormField("enroll_date", params[2]);

                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("team_cnt", sb.toString().trim());
                return sb.toString().trim();

            }


            /*  팀 세부 정보 */
            /////////////////

            if (params[params.length - 2].equals("getdata_team_info")) {
                writeFormField("nick", params[0]);
                writeFormField("team_owner", params[1]);
                writeFormField("team_count", params[2]);
                writeFormField("date_time", params[3]);
                writeFormField("type", params[4]);

                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("team_info", sb.toString().trim());
                return sb.toString().trim();

            }

            // 매칭등록 가능여부 //

            if (params[params.length - 2].equals("Check_enroll")) {
                writeFormField("team_owner", params[0]);
                writeFormField("date", params[1]);
                writeFormField("type", params[2]);


                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("check_enroll", sb.toString());
                return sb.toString().trim();
            }


            // 매칭등록 가능여부 한번더

            if (params[params.length - 2].equals("Check_enroll2")) {
                writeFormField("nick", params[0]);
                writeFormField("date", params[1]);


                dos.flush();
                dos.close();

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");

                }
                Log.e("check_enroll2", sb.toString());
                return sb.toString().trim();
            }

            /* 팀 존재 유무 */
            ////

            if (params[params.length - 2].equals("Check_team")) {
                writeFormField("nick", params[0]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("Check_team", sb.toString().trim());

                return sb.toString().trim();

            }
            if (params[params.length - 2].equals("sent_by_me_personal")) {
                writeFormField("type", params[0]);
                writeFormField("my_nick", params[1]);
                writeFormField("index", params[2]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("sent_by_me_personal", sb.toString().trim());

                return sb.toString().trim();

            }


            if (params[params.length - 2].equals("delete_token")) {

                writeFormField("nick", params[0]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("delete_token", sb.toString().trim());

                return sb.toString().trim();
            }

            if (params[params.length - 2].equals("status_received")) {
                writeFormField("type", params[0]);
                writeFormField("my_nick", params[1]);
                writeFormField("index", params[2]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("status_received", sb.toString().trim());

                return sb.toString().trim();

            }

            if (params[params.length - 2].equals("status_cancel")) {
                writeFormField("my_nick", params[0]);
                writeFormField("sender", params[1]);
                writeFormField("date_time", params[2]);
                writeFormField("team_cnt", params[3]);


                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("status_cancel", sb.toString().trim());

                return sb.toString().trim();

            }

            if (params[params.length - 2].equals("status_complete")) {
                writeFormField("my_nick", params[0]);
                writeFormField("sender", params[1]);
                writeFormField("date_time", params[2]);
                writeFormField("team_cnt", params[3]);


                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("status_complete", sb.toString().trim());

                return sb.toString().trim();

            }
            if (params[params.length - 2].equals("team_ing_cancel")) {
                writeFormField("my_nick", params[0]);


                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("team_ing_cancel", sb.toString().trim());

                return sb.toString().trim();

            }


            // 채팅 //
            if (params[params.length - 2].equals("chatting_room")) {

                writeFormField("no", params[0]);
                writeFormField("team_count", params[1]);
                writeFormField("nick", params[2]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("Chatting_room", sb.toString().trim());

                return sb.toString().trim();

            }

            if (params[params.length - 2].equals("ing_rendering")) {

                writeFormField("nick", params[0]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("ing_rendering", sb.toString().trim());

                return sb.toString().trim();

            }

            if (params[params.length - 2].equals("chatting_history")) {
                writeFormField("no", params[0]);
                writeFormField("team_count", params[1]);
                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("Chatting_his", sb.toString().trim());

                return sb.toString().trim();

            }


            if (params[params.length - 2].equals("Insert_msg")) {
                writeFormField("nick", params[0]);
                writeFormField("team_count", params[1]);
                writeFormField("to_send", params[2]);
                writeFormField("msg", params[3]);
                writeFormField("id", params[4]);
                writeFormField("time", params[5]);


                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("Insert_msg", sb.toString().trim());

                return sb.toString().trim();

            }

            if (params[params.length - 2].equals("Insert_msg2")) {
                writeFormField("nick", params[0]);
                writeFormField("team_count", params[1]);
                writeFormField("to_send", params[2]);
                writeFormField("to_send2", params[3]);
                writeFormField("to_send3", params[4]);
                writeFormField("msg", params[5]);
                writeFormField("id", params[6]);
                writeFormField("time", params[7]);


                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("Insert_msg2", sb.toString().trim());

                return sb.toString().trim();

            }

            if (params[params.length - 2].equals("Insert_msg3")) {
                writeFormField("nick", params[0]);
                writeFormField("team_count", params[1]);
                writeFormField("to_send", params[2]);
                writeFormField("to_send2", params[3]);
                writeFormField("to_send3", params[4]);
                writeFormField("to_send4", params[5]);
                writeFormField("to_send5", params[6]);
                writeFormField("msg", params[7]);
                writeFormField("id", params[8]);
                writeFormField("time", params[9]);


                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("Insert_msg3", sb.toString().trim());

                return sb.toString().trim();

            }

            if (params[params.length - 2].equals("Insert_msg4")) {
                writeFormField("nick", params[0]);
                writeFormField("team_count", params[1]);
                writeFormField("to_send", params[2]);
                writeFormField("to_send2", params[3]);
                writeFormField("to_send3", params[4]);
                writeFormField("to_send4", params[5]);
                writeFormField("to_send5", params[6]);
                writeFormField("to_send6", params[7]);
                writeFormField("to_send7", params[8]);
                writeFormField("msg", params[9]);
                writeFormField("id", params[10]);
                writeFormField("time", params[11]);


                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("Insert_msg4", sb.toString().trim());

                return sb.toString().trim();

            }


            /* ID/PW 정보 가져오기 */


            if (params[params.length - 2].equals("getdata_idpw")) {

                writeFormField("name", params[0]);
                writeFormField("phone", params[1]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e("getdata_idpw", sb.toString().trim());

                return sb.toString().trim();

            }


            /* id/pw 패스우드 업데이트 */

            if (params[params.length - 2].equals("update_pw")) {
                writeFormField("pw", params[0]);
                writeFormField("name", params[1]);
                writeFormField("phone", params[2]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e(" update_pw", sb.toString().trim());

                return sb.toString().trim();

            }
            if (params[params.length - 2].equals("com_cancel")) {
                writeFormField("my_nick", params[0]);
                writeFormField("nick", params[1]);
                writeFormField("date_time", params[2]);
                writeFormField("team_cnt", params[3]);
                writeFormField("msg", params[4]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e(" com_cancel", sb.toString().trim());

                return sb.toString().trim();
            }
            if (params[params.length - 2].equals("update_chat_al_zero")) {
                writeFormField("nick", params[0]);
                writeFormField("id", params[1]);
                writeFormField("team_cnt", params[2]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e(" update_chat_al_zero", sb.toString().trim());

                return sb.toString().trim();
            }

            if (params[params.length - 2].equals("session_check")) {
                writeFormField("id", params[0]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e(" session_check", sb.toString().trim());

                return sb.toString().trim();
            }


            if (params[params.length - 2].equals("check_chatting")) {
                writeFormField("nick", params[0]);
                writeFormField("team_count", params[1]);
                writeFormField("cmp_no", params[2]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e(" check_chatting", sb.toString().trim());

                return sb.toString().trim();
            }


            if (params[params.length - 2].equals("update_chatting_open")) {
                writeFormField("nick", params[0]);
                writeFormField("team_count", params[1]);
                writeFormField("cmp_no", params[2]);

                dos.flush();
                dos.close();
                dos = null;

                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }
                Log.e(" update_chatting_open", sb.toString().trim());

                return sb.toString().trim();
            }


            return "fail";

        } catch (Exception e) {
            Log.e("php", new String("Exception: " + e.getMessage()));
            return new String("Exception: " + e.getMessage());
        }

    }


}

