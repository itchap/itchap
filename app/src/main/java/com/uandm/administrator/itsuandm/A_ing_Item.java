package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by u on 2017-02-01.
 */

public class A_ing_Item {
    private Context context;
    private String ad;
    private Double ad_loc1;
    private Double ad_loc2;
    private String tel;
    private String name;
    private String date2, date, time, full_date;
    private String img[] = new String[4];
    private String title;
    private String team_owner;

    private String al_cnt;


    private String store_id;

    private int cmp_id;
    private int type;


    public A_ing_Item(Context context, String img[], String title, int type,int cmp_id, String ad, Double ad_loc1, Double ad_loc2, String tel, String name, String date, String time, String full_date, String team_owner, String store_id,String al_cnt) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String today = formatter.format(calendar.getTime());

        this.context = context;
        this.ad = ad;
        this.ad_loc1 = ad_loc1;
        this.ad_loc2 = ad_loc2;
        this.tel = tel;
        this.name = name;
        this.date2 = date;
        this.time = time;
        this.full_date = full_date;
        this.type = type;
        this.cmp_id = cmp_id;
        this.title = title;
        this.team_owner = team_owner;
        this.store_id = store_id;
        this.al_cnt = al_cnt;

        for (int i = 0; i < img.length; i++) {
            if (img[i] != null)
                this.img[i] = img[i];
            else
                this.img[i] = null;
        }
        try {
            this.date = diffOfDate(full_date.substring(0, 8), today);
        } catch (Exception e) {

        }
    }


    public int getCmp_id() {
        return cmp_id;
    }

    public void setCmp_id(int cmp_id) {
        this.cmp_id = cmp_id;
    }
    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }

    public String getAl_cnt() {
        return al_cnt;
    }

    public void setAl_cnt(String al_cnt) {
        this.al_cnt = al_cnt;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public Double getAd_loc1() {
        return ad_loc1;
    }

    public void setAd_loc1(Double ad_loc1) {
        this.ad_loc1 = ad_loc1;
    }

    public Double getAd_loc2() {
        return ad_loc2;
    }

    public void setAd_loc2(Double ad_loc2) {
        this.ad_loc2 = ad_loc2;
    }


    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getFull_date() {
        return full_date;
    }

    public void setFull_date(String full_date) {
        this.full_date = full_date;
    }

    public String[] getImg() {
        return img;
    }

    public void setImg(String[] img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public String getTeam_owner() {
        return team_owner;
    }

    public void setTeam_owner(String team_owner) {
        this.team_owner = team_owner;
    }

    public void setType(int type) {
        this.type = type;
    }

    protected String diffOfDate(String begin, String end) throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        Date beginDate = formatter.parse(begin);
        Date endDate = formatter.parse(end);

        long diff = beginDate.getTime() - endDate.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        String date;
        if (diffDays > 0) {
            date = "D-" + diffDays;
        } else if (diffDays == 0) {
            date = "D-day";
        } else {
            //이미 지난 것들
            date = "D+" + (-diffDays);
        }
        return date;
    }
}