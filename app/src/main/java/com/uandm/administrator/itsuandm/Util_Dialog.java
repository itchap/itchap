package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.*;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;

import me.leolin.shortcutbadger.ShortcutBadger;


/**
 * Created by Administrator on 2016-11-22.
 */
public class Util_Dialog extends Dialog {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;


    private ImageView mIcon;
    private TextView mMsg;
    private TextView mBtn1, mBtn2;
    private ImageView mBtnMargin;

    private Drawable mIcon_bg = null;
    private String mType;
    private String msg1, btn1, btn2;

    private BroadcastReceiver receiver_am;


    public String put_data;

    private String result;
    private RecyclerViewPager viewpager;

    Context mContext;

    //only for matching_adapter
    public Util_Dialog(Context context, Drawable icon, String msg, String btn_1, String btn_2, String type, RecyclerViewPager viewpager) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mContext = context;
        this.mType = type;
        this.msg1 = msg;
        this.btn1 = btn_1;
        this.btn2 = btn_2;
        this.mIcon_bg = icon;
        this.viewpager = viewpager;
    }


    //버튼두개일때
    public Util_Dialog(Context context, Drawable icon, String msg, String btn_1, String btn_2, String type) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mContext = context;
        this.mType = type;
        this.msg1 = msg;
        this.btn1 = btn_1;
        this.btn2 = btn_2;
        this.mIcon_bg = icon;
    }


    //버튼하나일때
    public Util_Dialog(Context context, Drawable icon, String msg, String btn_1, String type) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mContext = context;
        this.mType = type;
        this.msg1 = msg;
        this.btn1 = btn_1;
        this.btn2 = null;
        this.mIcon_bg = icon;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 다이얼로그 외부 화면 흐리게 표현
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.l_custom_dialog);

        mIcon = (ImageView) findViewById(R.id.icon);
        mMsg = (TextView) findViewById(R.id.msg);
        mBtn1 = (TextView) findViewById(R.id.btn_1);

        mBtnMargin = (ImageView) findViewById(R.id.two_btn_margin);
        mBtn2 = (TextView) findViewById(R.id.btn_2);

        pref = mContext.getSharedPreferences("login", 0);
        editor = pref.edit();

//        버튼 하나일때
        if (btn2 == null) {
            mMsg.setText(msg1);
            mBtn1.setText(btn1);
            mBtn1.setOnClickListener(btnlistener);
            mIcon.setBackground(mIcon_bg);
        }
        //버튼두개일때
        else {
            mMsg.setText(msg1);
            mBtn1.setText(btn1);
            mBtnMargin.setVisibility(View.VISIBLE);
            mBtn2.setVisibility(View.VISIBLE);
            mBtn2.setText(btn2);
            mBtn1.setOnClickListener(btnlistener2);
            mBtn2.setOnClickListener(btnlistener3);
            mIcon.setBackground(mIcon_bg);
        }


    }

    View.OnClickListener btnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (mType) {
                //버튼하나일때 아무 작업도 없이 다이얼로그 죽이는 경우
                case "dismiss":
                case "error_submit_no_content":
                    Util_Dialog.this.dismiss();
                    break;
                //제안오류신고 제출하기!!
                case "error_submit":
                    //@@@@
                    Util_Dialog.this.dismiss();
                    break;
                case "login_fail":
                    Util_Dialog.this.dismiss();
                    Page_Login.btn_login.setBackgroundResource(R.drawable.login_btn);
                    Page_Login.btn_login.setTextColor(mContext.getResources().getColor(R.color.app_default));
///////////
                    break;
                case "modify":
                    Util_Dialog.this.dismiss();
                    intent = new Intent(mContext, Page_Main.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("from", "modify");
                    getContext().startActivity(intent);
                    break;
                case "M_age":
                    Util_Dialog.this.dismiss();
                    intent = new Intent(mContext, Page_Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                    break;
                ///////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                case "join":
                case "to_main":
                    Util_Dialog.this.dismiss();
                    Intent main = new Intent(mContext, Page_Main.class);
                    main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    main.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    main.putExtra("from", "dialog");
                    mContext.startActivity(main);
                    break;
                case "withdraw":
                    Util_Dialog.this.dismiss();
                    intent = new Intent(mContext, Page_Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("logout", true);
                    getContext().startActivity(intent);
                    break;
                case "to_ing":
                    Util_Dialog.this.dismiss();
                    intent = new Intent(mContext, A_ing.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);
                    break;
                case "matching_enroll":
                case "to_back":
                    Util_Dialog.this.dismiss();
                    Activity activity = (Activity) mContext;
                    activity.finish();
                    break;

            }
        }
    };

    // 왼쪽 버튼
    View.OnClickListener btnlistener2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (mType) {
                case "withdraw":
                case "to_organize_team":
                case "logout":
                case "withdraw_team":
                case "to_credit":
                case "last_page":
                case "team_ing_cancel":
                    Util_Dialog.this.dismiss();
                    break;
                //팀수락 거절
                case "team_waiting":
                    try {
                        result = new Util_PHP(mContext).execute(pref.getString("nick", ""), put_data, "reject_team", "http://175.126.38.139/php/fcm/reject_team.php").get();
                        Log.e("거절 : ", "" + result);
                        Intent intent1 = new Intent(getContext(), Page_Main.class);
                        intent1.putExtra("from", "reject");
                        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getContext().startActivity(intent1);
                        return;
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                    Util_Dialog.this.dismiss();
                    break;
                case "network":
                    Util_Dialog.this.dismiss();
                    intent = new Intent(mContext, Page_Splash.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("finish", true);
                    getContext().startActivity(intent);
                    break;
                case "update":
                    Util_Dialog.this.dismiss();
                    intent = new Intent(mContext, Page_Splash.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("finish", true);
                    getContext().startActivity(intent);
                    break;


            }
        }
    };
    // 오른쪽 버튼
    View.OnClickListener btnlistener3 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (mType) {
                case "to_organize_team":
                    Util_Dialog.this.dismiss();
                    intent = new Intent(getContext(), Drawer_Organize_Team.class);
                    //액티비티 스택 제거하면 되는건가??? 아님 메인으로 가야하나...?
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);
                    break;
                case "logout":
                    Util_Dialog.this.dismiss();
                    intent = new Intent(mContext, Page_Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    FirebaseMessagingService.bagde_count_total = 0;
                    FirebaseMessagingService.bagde_count = 0;
                    FirebaseMessagingService.bagde_count_chat = 0;
                    ShortcutBadger.removeCount(mContext);
                    intent.putExtra("logout", true);
                    try {
                        result = new Util_PHP(mContext).execute(pref.getString("nick", " "), "delete_token", "http://175.126.38.139/php/del_token.php").get();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    getContext().startActivity(intent);
                    break;
                //팀수락 수락
                case "team_waiting":
                    try {
                        result = new Util_PHP(mContext).execute(pref.getString("nick", ""), put_data, "accept_team", "http://175.126.38.139/php/fcm/accept_team.php").get();
                        Intent intent1 = new Intent(getContext(), Page_Main.class);
                        intent1.putExtra("from", "matchong_enroll");
                        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getContext().startActivity(intent1);
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                    Util_Dialog.this.dismiss();
                    break;
                //팀 폭파
                case "withdraw_team":
                    try {
                        result = new Util_PHP(mContext).execute(pref.getString("nick", ""), "withdraw_team", "http://175.126.38.139/php/fcm/withdraw_team.php").get();
                        // 팀테이블 지우고, enroll table에 있는것도 지우고, 팀원들한테 푸시알림
                        Log.e("팀 폭파: ", "" + result);
                        if (result.contains("success")) {
                            Intent intent1 = new Intent(getContext(), Page_Main.class);
                            intent1.putExtra("from", "matchong_enroll");
                            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getContext().startActivity(intent1);
                        } else {
                            Log.e("ERROR", "");
                        }
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                    Util_Dialog.this.dismiss();
                    break;
                case "last_page":
                    Util_Dialog.this.dismiss();
                    viewpager.scrollToPosition(0);
                    //
                    break;
                case "to_credit":
                    Util_Dialog.this.dismiss();
                    intent = new Intent(mContext, Page_Credit.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);
                    break;
                case "network":
                    ConnectivityManager manager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                    NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                    Util_Dialog.this.dismiss();
                    if (mobile.isConnected() || wifi.isConnected()) {

                        Util_Dialog.this.dismiss();
                        intent = new Intent(mContext, Page_Splash.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getContext().startActivity(intent);

                    } else {
                        new Util_Dialog(mContext, mContext.getResources().getDrawable(R.drawable.icon_warning), "인터넷 연결을 확인해주세요.", "나가기", "확인", "network").show();
                    }
                    break;
                case "team_ing_cancel":
                    try {
                        result = new Util_PHP(mContext).execute(pref.getString("nick", ""), "team_ing_cancel", "http://175.126.38.139/php/fcm/sss.php").get();
                        Log.e("team_ing_cancel", "" + result);
                        if (result.contains("success")) {
                            Intent intent1 = new Intent(getContext(), Page_Main.class);
                            intent1.putExtra("from", "matchong_enroll");
                            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getContext().startActivity(intent1);
                        } else {
                            Log.e("ERROR", "");
                        }
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                    Util_Dialog.this.dismiss();
                    break;
                case "update":
                    //업데이트 확인
                    //업데이트할게여
                    Util_Dialog.this.dismiss();
                    intent = new Intent(mContext, Page_Splash.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("finish", true);
                    getContext().startActivity(intent);
                    break;
            }
        }
    };


}

