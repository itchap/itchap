package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.bumptech.glide.Glide;

/**
 * Created by u on 2016-12-26.
 */
public class Pager_Enroll_Store_Adapter extends PagerAdapter {

    private Context mContext;
    private Uri [] mResources;

    public Pager_Enroll_Store_Adapter(Context mContext, Uri[] mResources) {
        this.mContext = mContext;
        this.mResources = mResources;
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_enroll_store, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.pager_store_item);
        Glide.with(mContext).load(mResources[position]).into(imageView);
        container.addView(itemView);

        return itemView;
    }
    //
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}