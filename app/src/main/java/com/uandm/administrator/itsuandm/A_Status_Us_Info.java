package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.media.AudioRecord;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.tsengvn.typekit.TypekitContextWrapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by u on 2017-02-06.
 */
public class A_Status_Us_Info extends AppCompatActivity {

    private Intent intent;
    private String type, enroll_date, team_cnt, nick;
    private RelativeLayout rel_action;
    private JSONArray jsonArray = null;
    private String json;
    private TextView title, like, loc, date, time, age;
    private Util_ImageView img1, img2, img3, img4;
    private ImageView left_list, right_list;
    private  RelativeLayout back;


    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        intent = getIntent();
        type = intent.getStringExtra("type");
        enroll_date = intent.getStringExtra("full_date");
        nick = intent.getStringExtra("nick");
        try {
            json = new Util_PHP(A_Status_Us_Info.this).execute(nick, type, enroll_date, "team_cnt", "http://175.126.38.139/php/team_cnt.php").get();
            if(json.equals("error")){
                Log.e("json",""+json);
            }else{
                rendering(json);
            }
        } catch (Exception e) {

        }

    }

    public void find_id() {
        rel_action = (RelativeLayout) findViewById(R.id.mat_action);
        rel_action.setVisibility(View.VISIBLE);
        back  = (RelativeLayout)findViewById(R.id.back_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        left_list = (ImageView) findViewById(R.id.left_list);
        left_list.setVisibility(View.INVISIBLE);
        right_list = (ImageView) findViewById(R.id.right_list);
        right_list.setVisibility(View.INVISIBLE);

        title = (TextView) findViewById(R.id.matching_title);
        date = (TextView) findViewById(R.id.matching_date);
        time = (TextView) findViewById(R.id.matching_time);
        age = (TextView) findViewById(R.id.matching_age);
        like = (TextView) findViewById(R.id.matching_count_like);
        loc = (TextView) findViewById(R.id.matching_loc);

        if (type.equals("1")) {
            img1 = (Util_ImageView) findViewById(R.id.matching_profile1);
            img2 = null;
            img3 = null;
            img4 = null;
        } else if (type.equals("2")) {
            img1 = (Util_ImageView) findViewById(R.id.matching_profile1);
            img2 = (Util_ImageView) findViewById(R.id.matching_profile2);
            img3 = null;
            img4 = null;

        } else if (type.equals("3")) {
            img1 = (Util_ImageView) findViewById(R.id.matching_profile1);
            img2 = (Util_ImageView) findViewById(R.id.matching_profile2);
            img3 = (Util_ImageView) findViewById(R.id.matching_profile3);
            img4 = null;

        } else if (type.equals("4")) {
            img1 = (Util_ImageView) findViewById(R.id.matching_profile1);
            img2 = (Util_ImageView) findViewById(R.id.matching_profile2);
            img3 = (Util_ImageView) findViewById(R.id.matching_profile3);
            img4 = (Util_ImageView) findViewById(R.id.matching_profile4);
        } else {

        }
    }


    protected void rendering(String json){
        try{
            JSONObject jsonObj = new JSONObject(json);
            jsonArray = jsonObj.getJSONArray("result");
            JSONObject c = jsonArray.getJSONObject(0);
            type = c.getString("team_cnt");
                if (type.equals("1")) {
                    setContentView(R.layout.l_matching_item_1);
                    find_id();
                    img1.setType(1);
                    Glide.with(A_Status_Us_Info.this).load(c.getString("img1")).asBitmap().into(img1);
                } else if (type.equals("2")) {
                    setContentView(R.layout.l_matching_item_2);
                    find_id();
                    img1.setType(2);
                    img2.setType(3);
                    Glide.with(A_Status_Us_Info.this).load(c.getString("img1")).asBitmap().into(img1);
                    Glide.with(A_Status_Us_Info.this).load(c.getString("img2")).asBitmap().into(img2);
                } else if (type.equals("3")) {
                    setContentView(R.layout.l_matching_item_3);
                    find_id();
                    img1.setType(4);
                    img2.setType(5);
                    img3.setType(6);
                    Glide.with(A_Status_Us_Info.this).load(c.getString("img1")).asBitmap().into(img1);
                    Glide.with(A_Status_Us_Info.this).load(c.getString("img2")).asBitmap().into(img2);
                    Glide.with(A_Status_Us_Info.this).load(c.getString("img3")).asBitmap().into(img3);
                } else if (type.equals("4")) {
                    setContentView(R.layout.l_matching_item_4);
                    find_id();
                    img1.setType(7);
                    img2.setType(8);
                    img3.setType(9);
                    img4.setType(10);
                    Glide.with(A_Status_Us_Info.this).load(c.getString("img1")).asBitmap().into(img1);
                    Glide.with(A_Status_Us_Info.this).load(c.getString("img2")).asBitmap().into(img2);
                    Glide.with(A_Status_Us_Info.this).load(c.getString("img3")).asBitmap().into(img3);
                    Glide.with(A_Status_Us_Info.this).load(c.getString("img4")).asBitmap().into(img4);
                } else {
                    Log.e("json3",""+json);
                }
            title.setText(c.getString("title"));
            date.setText(c.getString("date"));
            loc.setText(c.getString("loc"));
            age.setText(c.getString("age"));
            time.setText(c.getString("time"));
            like.setText(c.getString("cnt_like"));


            }catch (Exception e){

        }

    }
}
