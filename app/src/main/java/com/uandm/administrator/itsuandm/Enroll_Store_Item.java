package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;

import java.util.ArrayList;

/**
 * Created by u on 2016-12-23.
 */
public class Enroll_Store_Item {


    String img;
    String id;
    String store_name;
    String store_loc;
    String store_type;
    String index;
    Context context;

    public Enroll_Store_Item(Context context, String img, String id, String store_name, String store_loc, String store_type, String index) {
        this.context = context;
        this.img = img;
        this.id = id;
        this.store_name = store_name;
        this.store_loc = store_loc;
        this.store_type = store_type;
        this.index = index;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }


    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getStore_loc() {
        return store_loc;
    }

    public void setStore_loc(String store_loc) {
        this.store_loc = store_loc;
    }

    public String getStore_type() {
        return store_type;
    }

    public void setStore_type(String store_type) {
        this.store_type = store_type;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }


}