package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.net.Uri;

/**
 * Created by u on 2017-02-10.
 */
public class Location_Item {

    Context context;
    String no;
    String loc_1;
    String loc_2;


    public Location_Item(Context context, String no, String loc_1,String loc_2) {
        this.context = context;
        this.no = no;
        this.loc_1 = loc_1;
        this.loc_2 = loc_2;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getLoc_1() {
        return loc_1;
    }

    public void setLoc_1(String loc_1) {
        this.loc_1 = loc_1;
    }

    public String getLoc_2() {
        return loc_2;
    }

    public void setLoc_2(String loc_2) {
        this.loc_2 = loc_2;
    }

}

