package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.bumptech.glide.RequestManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Created by young on 2017-01-22.
 *////

public class A_Status_Adapter extends RecyclerView.Adapter<A_Status_Adapter.ViewHolder> {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList
    private ArrayList<A_Status_ltem> listViewItemList;
    private Context context;
    int type = -1;
    private Util_Dialog dialog;
    public static final int ITEM_TYPE_ONE = 1;
    public static final int ITEM_TYPE_TWO = 2;
    public static final int ITEM_TYPE_THREE = 3;
    public static final int ITEM_TYPE_FOUR = 4;
    private RequestManager requestManager;


    public A_Status_Adapter(ArrayList<A_Status_ltem> item, Context context, RequestManager requestManager) {
        this.context = context;
        this.listViewItemList = item;
        this.requestManager = requestManager;

    }


    @Override
    public int getItemViewType(int position) {

        if (listViewItemList.get(position).getType() == 1) {
            this.type = 1;
            return ITEM_TYPE_ONE;
        } else if (listViewItemList.get(position).getType() == 2) {
            this.type = 2;
            return ITEM_TYPE_TWO;
        } else if (listViewItemList.get(position).getType() == 3) {
            this.type = 3;
            return ITEM_TYPE_THREE;
        } else {
            this.type = 4;
            return ITEM_TYPE_FOUR;
        }


    }

    /**
     * 레이아웃을 만들어서 Holer에 저장
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public A_Status_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view;
        if (viewType == 1) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_status_item, viewGroup, false);
            return new A_Status_Adapter.ViewHolder(view, 1);
        } else if (viewType == 2) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_status_item_2, viewGroup, false);
            return new A_Status_Adapter.ViewHolder(view, 2);
        } else if (viewType == 3) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_status_item_3, viewGroup, false);
            return new A_Status_Adapter.ViewHolder(view, 3);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_status_item_4, viewGroup, false);
            return new A_Status_Adapter.ViewHolder(view, 4);
        }


    }


    @Override
    public void onBindViewHolder(final A_Status_Adapter.ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);
        if (itemtype == ITEM_TYPE_ONE) {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
        } else if (itemtype == ITEM_TYPE_TWO) {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
        } else if (itemtype == ITEM_TYPE_THREE) {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
            requestManager.load(listViewItemList.get(position).getImg()[2]).asBitmap().into(viewHolder.iv_3);

        } else {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
            requestManager.load(listViewItemList.get(position).getImg()[2]).asBitmap().into(viewHolder.iv_3);
            requestManager.load(listViewItemList.get(position).getImg()[3]).asBitmap().into(viewHolder.iv_4);

        }
        viewHolder.title.setText(listViewItemList.get(position).getTitle());
        viewHolder.date.setText(listViewItemList.get(position).getDate());
        viewHolder.time.setText(listViewItemList.get(position).getTime());
        viewHolder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,Team_Info.class);
                intent.putExtra("teammate_count",itemtype);
                intent.putExtra("get_intent_type",listViewItemList.get(position).getBtn_type());
                intent.putExtra("date_time",listViewItemList.get(position).getDate2());
                intent.putExtra("owner_nick",listViewItemList.get(position).getOwner());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }

    /**
     * 뷰 재활용을 위한 viewHolder`
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        protected Util_ImageView iv_1, iv_2, iv_3, iv_4;
        protected TextView title, date, time;
        protected  RelativeLayout main ;


        public ViewHolder(View itemView, int type) {
            super(itemView);
            if (type == 1) {
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                iv_1.setType(1);
                iv_2 = null;
                iv_3 = null;
                iv_4 = null;
            } else if (type == 2) {
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                iv_1.setType(2);
                iv_2 = (Util_ImageView) itemView.findViewById(R.id.img2);
                iv_2.setType(3);
                iv_3 = null;
                iv_4 = null;
            } else if (type == 3) {
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                iv_1.setType(4);
                iv_2 = (Util_ImageView) itemView.findViewById(R.id.img2);
                iv_2.setType(5);
                iv_3 = (Util_ImageView) itemView.findViewById(R.id.img3);
                iv_3.setType(6);
                iv_4 = null;
            } else {
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                iv_1.setType(7);
                iv_2 = (Util_ImageView) itemView.findViewById(R.id.img2);
                iv_2.setType(9);
                iv_3 = (Util_ImageView) itemView.findViewById(R.id.img3);
                iv_3.setType(8);
                iv_4 = (Util_ImageView) itemView.findViewById(R.id.img4);
                iv_4.setType(10);
            }
            title = (TextView) itemView.findViewById(R.id.title);
            date = (TextView) itemView.findViewById(R.id.date);
            time = (TextView) itemView.findViewById(R.id.time);
            main = (RelativeLayout)itemView.findViewById(R.id.main);

        }

    }


}

