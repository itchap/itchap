package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by young on 2016-11-22.
 */

public class Page_Main_Matching_RecylerAdapter extends RecyclerView.Adapter<Page_Main_Matching_RecylerAdapter.ViewHolder>{
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList
    private ArrayList<Page_Main_Matching_ListViewItem> listViewItemList;
    private Context context;
    private int type = -1;
    public static final int ITEM_TYPE_ONE = 1;
    public static final int ITEM_TYPE_TWO = 2;
    public static final int ITEM_TYPE_THREE = 3;
    public static final int ITEM_TYPE_FOUR = 4;
    private RequestManager requestManager;
    private   DisplayMetrics dm;
    private static int width,height,dpi;
    private static int l_width, l_height;
    public ViewHolder vh;



    public Page_Main_Matching_RecylerAdapter(ArrayList<Page_Main_Matching_ListViewItem> item, Context context, RequestManager requestManager) {
        this.context = context;
        this.listViewItemList = item;
        this.requestManager = requestManager;

        dm = context.getApplicationContext().getResources().getDisplayMetrics();
        dpi = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45,context.getResources().getDisplayMetrics());
        width = dm.widthPixels;
        height = dm.heightPixels;
    }
    @Override
    public int getItemViewType(int position) {

        if (listViewItemList.get(position).getType() == 1) {
            this.type = 1;
            return ITEM_TYPE_ONE;
        } else if (listViewItemList.get(position).getType() == 2) {
            this.type = 2;
            return ITEM_TYPE_TWO;
        } else if (listViewItemList.get(position).getType() == 3) {
            this.type = 3;
            return ITEM_TYPE_THREE;
        } else {
            this.type = 4;
            return ITEM_TYPE_FOUR;
        }


    }

    /**
     * 레이아웃을 만들어서 Holer에 저장
     *
     * @param viewGroup
     * @param viewType
     * @return
     */


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;
        if (viewType == ITEM_TYPE_ONE) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_matching_item_1, viewGroup, false);
            vh = new ViewHolder(view,1);
            return vh ;
        } else if (viewType == ITEM_TYPE_TWO) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_matching_item_2, viewGroup, false);
            return new ViewHolder(view, 2);
        } else if (viewType == ITEM_TYPE_THREE) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_matching_item_3, viewGroup, false);
            return new ViewHolder(view, 3);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_matching_item_4, viewGroup, false);
            vh = new ViewHolder(view,4);
            return vh ;
        }

    }



    /**
     * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);

        if (itemtype == ITEM_TYPE_ONE) {
           requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);

            viewHolder.iv_1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()){
                        case R.id.matching_profile1:
                        case R.id.matching_profile_layout:
                            Intent intent = new Intent(context, Team_Info.class);
                            intent.putExtra("teammate_count", itemtype);
                            intent.putExtra("get_intent_type","main");
                            intent.putExtra("owner_nick",listViewItemList.get(position).getOwner_name());
                            intent.putExtra("date_time",listViewItemList.get(position).getDate_time());
                            context.startActivity(intent);
                            break;
                    }
//
                }
            });
            //viewHolder.iv_1.setImageBitmap(listViewItemList.get(position).getImg()[0],1);
        } else if (itemtype == ITEM_TYPE_TWO) {
            viewHolder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()){
                        case R.id.matching_profile1:
                        case R.id.matching_profile_layout:
                            Intent intent = new Intent(context, Team_Info.class);
                            intent.putExtra("teammate_count", itemtype);
                            intent.putExtra("get_intent_type","main");
                            intent.putExtra("owner_nick",listViewItemList.get(position).getOwner_name());
                            intent.putExtra("date_time",listViewItemList.get(position).getDate_time());
                            context.startActivity(intent);
                            break;
                    }

                }
            });
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
            //viewHolder.iv_1.setImageBitmap(listViewItemList.get(position).getImg()[0],2);
            //viewHolder.iv_2.setImageBitmap(listViewItemList.get(position).getImg()[1],3);
        } else if (itemtype == ITEM_TYPE_THREE) {
            viewHolder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()){
                        case R.id.matching_profile1:
                        case R.id.matching_profile_layout:
                            Intent intent = new Intent(context, Team_Info.class);
                            intent.putExtra("teammate_count", itemtype);
                            intent.putExtra("get_intent_type","main");
                            intent.putExtra("owner_nick",listViewItemList.get(position).getOwner_name());
                            intent.putExtra("date_time",listViewItemList.get(position).getDate_time());
                            context.startActivity(intent);
                            break;
                    }

                }
            });
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
            requestManager.load(listViewItemList.get(position).getImg()[2]).asBitmap().into(viewHolder.iv_3);

            //viewHolder.iv_1.setImageBitmap(listViewItemList.get(position).getImg()[0],4);
            //viewHolder.iv_2.setImageBitmap(listViewItemList.get(position).getImg()[1],5);
            //viewHolder.iv_3.setImageBitmap(listViewItemList.get(position).getImg()[2],6);
        } else {
            viewHolder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()){
                        case R.id.matching_profile1:
                        case R.id.matching_profile_layout:
                            Intent intent = new Intent(context, Team_Info.class);
                            intent.putExtra("teammate_count", itemtype);
                            intent.putExtra("get_intent_type","main");
                            intent.putExtra("owner_nick",listViewItemList.get(position).getOwner_name());
                            intent.putExtra("date_time",listViewItemList.get(position).getDate_time());
                            //intent.putExtra
                            context.startActivity(intent);

                            break;
                    }

                }
            });
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
            requestManager.load(listViewItemList.get(position).getImg()[2]).asBitmap().into(viewHolder.iv_3);
            requestManager.load(listViewItemList.get(position).getImg()[3]).asBitmap().into(viewHolder.iv_4);

        }

        viewHolder.tv_loc.setText(listViewItemList.get(position).getLoc());
        viewHolder.tv_date.setText(listViewItemList.get(position).getDate());
        viewHolder.tv_time.setText(listViewItemList.get(position).getTime());
        viewHolder.tv_age.setText(listViewItemList.get(position).getAge());
        viewHolder.tv_title.setText(listViewItemList.get(position).getTitle());

        viewHolder.tv_like.setText(listViewItemList.get(position).getCnt_like());
        Log.e("adatper", "adapter");

    }
    public void removeall() {
        int size = listViewItemList.size();
        listViewItemList.clear();
        notifyItemRangeRemoved(0, size);

    }






    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }
//


    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public final static class ViewHolder extends RecyclerView.ViewHolder{
        protected Util_ImageView iv_1, iv_2, iv_3, iv_4;
        protected TextView tv_title, tv_loc, tv_date, tv_time, tv_age, tv_like;
        protected RelativeLayout layout,layout_;

        public ViewHolder(View itemView, int type) {
            super(itemView);
            if (type == 1) {
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.matching_profile1);
                iv_1.setType(1);
                iv_2 = null;
                iv_3 = null;
                iv_4 = null;
            } else if (type == 2) {
                layout = (RelativeLayout)itemView.findViewById(R.id.matching_profile_layout);
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.matching_profile1);
                iv_1.setType(2);
                iv_2 = (Util_ImageView) itemView.findViewById(R.id.matching_profile2);
                iv_2.setType(3);
                iv_3 = null;
                iv_4 = null;
            } else if (type == 3) {
                layout = (RelativeLayout)itemView.findViewById(R.id.matching_profile_layout);
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.matching_profile1);
                iv_1.setType(4);
                iv_2 = (Util_ImageView) itemView.findViewById(R.id.matching_profile2);
                iv_2.setType(5);
                iv_3 = (Util_ImageView) itemView.findViewById(R.id.matching_profile3);
                iv_3.setType(6);
                iv_4 = null;
            } else {
                layout = (RelativeLayout)itemView.findViewById(R.id.matching_profile_layout);
                iv_1 = (Util_ImageView) itemView.findViewById(R.id.matching_profile1);
                iv_1.setType(7);
                iv_2 = (Util_ImageView) itemView.findViewById(R.id.matching_profile2);
                iv_2.setType(8);
                iv_3 = (Util_ImageView) itemView.findViewById(R.id.matching_profile3);
                iv_3.setType(9);
                iv_4 = (Util_ImageView) itemView.findViewById(R.id.matching_profile4);
                iv_4.setType(10);
            }
            tv_title = (TextView)itemView.findViewById(R.id.matching_title);
            tv_like = (TextView)itemView.findViewById(R.id.matching_count_like);
            tv_loc = (TextView)itemView.findViewById(R.id.matching_loc);
            tv_date = (TextView)itemView.findViewById(R.id.matching_date);
            tv_time = (TextView)itemView.findViewById(R.id.matching_time);
            tv_age = (TextView)itemView.findViewById(R.id.matching_age);
            layout_ = (RelativeLayout)itemView.findViewById(R.id.matching_layout_profile1);

            layout_.post(new Runnable() {
                @Override
                public void run() {
                  l_width = layout_.getWidth();
                  l_height= layout_.getHeight();



                }
            });
            //Log.e("ddd", l_height+" "+ l_width);


            //layout_.getWidth();
            //layout_.getHeight();
            //Log.e("eeee",  j +" "+i );

//            LinearLayout.LayoutParams params_ = (LinearLayout.LayoutParams)layout_.getLayoutParams();
//            params_.width = width;
//            params_.height = width;
//
//            layout_.setLayoutParams(params_);



        }
    }


}

