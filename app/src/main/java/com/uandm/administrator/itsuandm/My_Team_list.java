package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.tsengvn.typekit.TypekitContextWrapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class My_Team_list extends AppCompatActivity {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private Util_ImageView photo_1, photo_2, photo_3, photo_4;
    private TextView nick_1, nick_2, nick_3, nick_4;
    private ImageView icon_1, icon_2, icon_3, icon_4, back;
    private RelativeLayout team_1, team_2, team_3, team_4, btn_next,btn_back;
///
    private  Util_Dialog dialog;


    private static final String TAG_RESULTS = "result";
    private String json;
    JSONArray team_list;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_my_team_list);

        team_1 = (RelativeLayout) findViewById(R.id.team_1);
        team_2 = (RelativeLayout) findViewById(R.id.team_2);
        team_3 = (RelativeLayout) findViewById(R.id.team_3);
        team_4 = (RelativeLayout) findViewById(R.id.team_4);

        photo_1 = (Util_ImageView)findViewById(R.id.photo_1);
        photo_2 = (Util_ImageView)findViewById(R.id.photo_2);
        photo_3 = (Util_ImageView)findViewById(R.id.photo_3);
        photo_4 = (Util_ImageView)findViewById(R.id.photo_4);

        photo_1.setType(11);
        photo_2.setType(11);
        photo_3.setType(11);
        photo_4.setType(11);


        nick_1=(TextView)findViewById(R.id.nick_1);
        nick_2=(TextView)findViewById(R.id.nick_2);
        nick_3=(TextView)findViewById(R.id.nick_3);
        nick_4=(TextView)findViewById(R.id.nick_4);

        icon_1 =(ImageView)findViewById(R.id.icon_1);
        icon_2 =(ImageView)findViewById(R.id.icon_2);
        icon_3 =(ImageView)findViewById(R.id.icon_3);
        icon_4 =(ImageView)findViewById(R.id.icon_4);

        btn_next =(RelativeLayout)findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog= new Util_Dialog(My_Team_list.this, getResources().getDrawable(R.drawable.icon_exit), "정말 팀을 나가시겠습니까?\n탈퇴 시 진행 중인 모든 미팅이 취소됩니다", "취소","탈퇴하기", "withdraw_team");
                dialog.show();
            }
        });

        btn_back = (RelativeLayout)findViewById(R.id.team_list_back_btn);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        pref = getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();

        try {
            json = new Util_PHP(My_Team_list.this).execute(pref.getString("nick", ""), "getdata_my_team", "http://175.126.38.139/php/getdata_team_list.php").get();
        }catch (Exception e){
            e.getStackTrace();
        }
        Log.e("++++",""+json);
      rendering(json);
    }
    protected void rendering(String json) {
        try {
            JSONObject jsonObj2 = new JSONObject(json);
            team_list = jsonObj2.getJSONArray(TAG_RESULTS);
            if (team_list.length() == 1) {
                JSONObject c = team_list.getJSONObject(0);
                if(c.getString("team_no").equals("2")){
                    team_1.setVisibility(View.VISIBLE);
                    team_2.setVisibility(View.VISIBLE);
                    Glide.with(this).load(c.getString("photo_uri_1")).asBitmap().into(photo_1);
                    Glide.with(this).load(c.getString("photo_uri_2")).asBitmap().into(photo_2);
                    nick_1.setText(c.getString("team_owner"));
                    nick_2.setText(c.getString("team_member_2"));
                    icon_1.setBackground(getResources().getDrawable(R.drawable.crown));
                }else if(c.getString("team_no").equals("3")){
                    team_1.setVisibility(View.VISIBLE);
                    team_2.setVisibility(View.VISIBLE);
                    team_3.setVisibility(View.VISIBLE);
                    Glide.with(this).load(c.getString("photo_uri_1")).asBitmap().into(photo_1);
                    Glide.with(this).load(c.getString("photo_uri_2")).asBitmap().into(photo_2);
                    Glide.with(this).load(c.getString("photo_uri_3")).asBitmap().into(photo_3);
                    nick_1.setText(c.getString("team_owner"));
                    nick_2.setText(c.getString("team_member_2"));
                    nick_3.setText(c.getString("team_member_3"));
                    icon_1.setBackground(getResources().getDrawable(R.drawable.crown));
                }else if(c.getString("team_no").equals("4")){
                    team_1.setVisibility(View.VISIBLE);
                    team_2.setVisibility(View.VISIBLE);
                    team_3.setVisibility(View.VISIBLE);
                    team_4.setVisibility(View.VISIBLE);
                        Glide.with(this).load(c.getString("photo_uri_1")).asBitmap().into(photo_1);
                        Glide.with(this).load(c.getString("photo_uri_2")).asBitmap().into(photo_2);
                        Glide.with(this).load(c.getString("photo_uri_3")).asBitmap().into(photo_3);
                        Glide.with(this).load(c.getString("photo_uri_4")).asBitmap().into(photo_4);
                        nick_1.setText(c.getString("team_owner"));
                        nick_2.setText(c.getString("team_member_2"));
                        nick_3.setText(c.getString("team_member_3"));
                        nick_4.setText(c.getString("team_member_4"));
                        icon_1.setBackground(getResources().getDrawable(R.drawable.crown));
                }else {
                    Log.e("ERROR", "2/3/4아님.");
                }
                //String uri = c.getString(TAG_PHOTO_MAIN);
                //Uri str_to_uri = Uri.parse(uri);
               // Glide.with(Drawer_Organize_Team.this).load(str_to_uri).asBitmap().into(my_photo);
            } else {
                Log.e("ERROR", "동일 닉네임 한명 이상");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
