package com.uandm.administrator.itsuandm;

import android.app.PendingIntent;
import android.content.*;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.android.vending.billing.IInAppBillingService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.TELECOM_SERVICE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class Page_Credit_Charge extends Fragment {

    private Util_Dialog dialog;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    protected View view;
    private ArrayList<Page_Credit_Item> credit_items;
    private Page_Credit_Adapter adapter;
    public RecyclerView recyclerview;
    String item_name;
    private String json;
    private JSONArray credit_value = null;
    IInAppBillingService mService;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.l_credit_charge, container, false);
        recyclerview = (RecyclerView) view.findViewById(R.id.recycler_credit);
        recyclerview.setOverScrollMode(View.OVER_SCROLL_NEVER);
        recyclerview.setNestedScrollingEnabled(false);
        LinearLayoutManager layout = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(layout);
        credit_items = new ArrayList<>();

        recyclerview.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {


            }
        });


        try {
            json = new Util_PHP(getContext()).execute("credit_value", "http://175.126.38.139/php/getdata_credit_value.php").get();
            rendering(json);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    protected void rendering(String json) {
        try {
            JSONObject jsonObj = new JSONObject(json);
            credit_value = jsonObj.getJSONArray("result");
            for (int i = 0; i < credit_value.length(); i++) {
                JSONObject c = credit_value.getJSONObject(i);
                credit_items.add(new Page_Credit_Item(getContext(),"x"+c.getString("credit_no"),c.getString("credit_no_plus"),c.getString("price")+"원"));
                adapter = new Page_Credit_Adapter(credit_items, getContext());
                recyclerview.setAdapter(adapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



}