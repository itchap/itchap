package com.uandm.administrator.itsuandm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.*;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.google.firebase.messaging.FirebaseMessaging;
import com.tsengvn.typekit.TypekitContextWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.security.auth.login.LoginException;

import java.io.DataInput;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import static android.view.View.VISIBLE;

public class Page_Main_Matching_Enroll extends AppCompatActivity {

    private TextView mTv_choice_date, mTv_choice_location;
    private RelativeLayout enroll_back, content;


    //개인 팀
    private ImageView personal, group;
    private int max_no;

    //다이얼로그
    private TextView dialog_cancel, dialog_confirm;
    private boolean confirm;


    //시간
    private TimePicker timePicker;
    private int TIME_PICKER_INTERVAL = 10;
    NumberPicker minutePicker;
    List<String> displayedValues;

    private NumberPicker numberPicker_ampm, numberPicker_hour, numberPicker_minute;

    //날짜
    private Calendar mCalender = Calendar.getInstance();
    private Calendar currentCalender = Calendar.getInstance();
    private int[] day = new int[7];
    private int[] month = new int[7];
    private String intweek[] = new String[7];
    private RecyclerView recyclerView;
    private Util_Date_Adapter adapter;
    private ArrayList<Util_Date_Item> item;
    boolean date_set = false;
    boolean location_set = false;
    private static int current_type;
    private JSONArray jsonArray;
    private String date_time_;


    //한마디
    private EditText edit_ment;

    //등록완료
    private TextView enroll_next;


    Util_Dialog dialog2;
    private Struct_Enroll struct_enroll;

    private String json, json1;
    private String nick;
    private String have_team;
    private String is_owner;
    private String status;

    BroadcastReceiver receiver;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_main_matching_enroll);
        current_type = 0;

        pref = getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();
        nick = pref.getString("nick", "null");

        try {
            json1 = new Util_PHP(Page_Main_Matching_Enroll.this).execute(nick, "Check_team", "http://175.126.38.139/php/check_team.php").get();
            parse_(json1);
        } catch (Exception e) {
            e.printStackTrace();
        }


        findid();
        get_day_of_month();
        get_day_of_week();

        for (int i = 0; i < day.length; i++) {
            Log.e("hhhh", day[i] + " " + intweek[i]);
        }

        mTv_choice_date.setOnClickListener(btnlistener);
        mTv_choice_location.setOnClickListener(btnlistener);
        personal.setOnClickListener(btnlistener);
        group.setOnClickListener(btnlistener);
        enroll_next.setOnClickListener(btnlistener);

        struct_enroll = new Struct_Enroll();
        struct_enroll.max_no = 2;

        IntentFilter filter = new IntentFilter();
        filter.addAction("DETAIL");

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String str = intent.getStringExtra("result");
                Log.e("enroll result", str);

                if (str.equals("success")) {
                    struct_enroll = (Struct_Enroll) intent.getSerializableExtra("all_struct");
                    mTv_choice_location.setBackgroundResource(R.drawable.box_round_maincolor);
                    mTv_choice_location.setTextColor(Color.WHITE);
                    mTv_choice_location.setText(struct_enroll.getLoc1() + " " + struct_enroll.getName());
                    location_set = true;
//


                }
            }
        };
        registerReceiver(receiver, filter);
    }

    public void findid() {
        mTv_choice_date = (TextView) findViewById(R.id.tv_choice_date);
        mTv_choice_location = (TextView) findViewById(R.id.tv_choice_location);
        personal = (ImageView) findViewById(R.id.personal_icon);
        group = (ImageView) findViewById(R.id.group_icon);
        edit_ment = (EditText) findViewById(R.id.et_ment);
        enroll_next = (TextView) findViewById(R.id.enroll_next);
        enroll_back = (RelativeLayout) findViewById(R.id.enroll_back_btn);
        enroll_back.setOnClickListener(btnlistener);

        content = (RelativeLayout) findViewById(R.id.l_page_main_matching_enroll);
        content.setOnClickListener(btnlistener);

    }

    void hide_keyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit_ment.getWindowToken(), 0);
    }

    View.OnClickListener btnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.l_page_main_matching_enroll:
                    hide_keyboard();
                    break;

                case R.id.tv_choice_date:
                    final Dialog dialog = new Dialog(Page_Main_Matching_Enroll.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.l_main_matching_enroll_dialog);
                    timePicker = (TimePicker) dialog.findViewById(R.id.time_picker);

                    item = new ArrayList<>();
                    for (int i = 0; i < day.length; i++) {
                        item.add(new Util_Date_Item(intweek[i], day[i], month[i], 1));
                        // 요일, 날짜
                    }

                    adapter = new Util_Date_Adapter(item, getApplicationContext(), 1);
                    recyclerView = (RecyclerView) dialog.findViewById(R.id.matching_dialog_recycler);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);
                    final LinearLayoutManager horizontalLayoutManagaer
                            = new LinearLayoutManager(Page_Main_Matching_Enroll.this, LinearLayoutManager.HORIZONTAL, false);
                    recyclerView.setLayoutManager(horizontalLayoutManagaer);
                    recyclerView.setAdapter(adapter);

                    setTimePickerInterval(timePicker);
                    dialog_cancel = (TextView) dialog.findViewById(R.id.dialog_cancel);
                    dialog_confirm = (TextView) dialog.findViewById(R.id.dialog_confirm);

                    dialog_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    //확인!!!@@@
                    dialog_confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String month_, day_, hour_, min_;
                            String selected_date;
                            int hour;
                            int minute;
                            //월 일 요일 저장
                            adapter.getCurrentItem(struct_enroll);

                            hour = timePicker.getCurrentHour();
                            minute = timePicker.getCurrentMinute() * 10;

                            struct_enroll.setMinute(minute);
                            struct_enroll.setHour(hour);

                            // Log.e("time11", struct_enroll.getMinute()+" "+struct_enroll.getHour());

                            //시 분 저장

                            Log.e("tag", struct_enroll.getDay() + struct_enroll.getWeek());

                            if (struct_enroll.getMonth() < 10) {
                                month_ = "0" + struct_enroll.getMonth();
                            } else {
                                month_ = Integer.toString(struct_enroll.getMonth());
                            }

                            if (struct_enroll.getDay() < 10) {
                                day_ = "0" + struct_enroll.getDay();
                            } else
                                day_ = Integer.toString(struct_enroll.getDay());

                            if (struct_enroll.getHour() < 10) {

                                if (struct_enroll.getHour() == 0)
                                    hour_ = "00";
                                else
                                    hour_ = "0" + struct_enroll.getHour();

                            } else
                                hour_ = Integer.toString(struct_enroll.getHour());


                            if (struct_enroll.getMinute() == 0) {
                                min_ = "0" + Integer.toString(struct_enroll.getMinute());
                            } else {
                                min_ = Integer.toString(struct_enroll.getMinute());
                            }

                            selected_date = Integer.toString(Calendar.getInstance().get(Calendar.YEAR)) + month_ + day_ + hour_ + min_;
                            Log.e("date", selected_date);

                            int selected_time, current_time;

                            try {
                                json = new Util_PHP(Page_Main_Matching_Enroll.this).execute(nick, selected_date, Integer.toString(current_type), "Check_enroll", "http://175.126.38.139/php/check_enroll.php").get();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (json.equals("able")) {
                                if (struct_enroll.getDay() == Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) {
                                    Calendar calendar = Calendar.getInstance();
                                    selected_time = (struct_enroll.getHour() * 60) + struct_enroll.getMinute();
                                    current_time = ((calendar.get(Calendar.HOUR_OF_DAY) * 60) + calendar.get(Calendar.MINUTE));

                                    Log.e("time", selected_time + " " + current_time + " " + (selected_time - current_time));

                                    if (selected_time - current_time <= 60) {
                                        dialog2 = new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_exit), "한 시간 이후의 미팅만 등록이 가능합니다.", "확인", "dismiss");
                                        dialog2.show();
                                    } else {
                                        if (date_set) {
                                            reset(1);
                                            mTv_choice_date.setBackgroundResource(R.drawable.box_round_maincolor);
                                            mTv_choice_date.setTextColor(Color.WHITE);
                                            mTv_choice_date.setText(month_ + "월 " + day_ + "일 " + struct_enroll.getWeek() + "요일 " + hour_ + ":" + min_);
                                            date_set = true;
                                        } else {
                                            mTv_choice_date.setBackgroundResource(R.drawable.box_round_maincolor);
                                            mTv_choice_date.setTextColor(Color.WHITE);
                                            mTv_choice_date.setText(month_ + "월 " + day_ + "일 " + struct_enroll.getWeek() + "요일 " + hour_ + ":" + min_);
                                            date_set = true;
                                        }
                                        dialog.dismiss();
                                    }
                                } else if (struct_enroll.getDay() == 0 || struct_enroll.getWeek() == null) {
                                    dialog2 = new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_exit), "날짜를 선택해주세요.", "확인", "dismiss");
                                    dialog2.show();

                                } else {
                                    if (date_set) {
                                        reset(1);
                                        mTv_choice_date.setBackgroundResource(R.drawable.box_round_maincolor);
                                        mTv_choice_date.setTextColor(Color.WHITE);
                                        mTv_choice_date.setText(month_ + "월 " + day_ + "일 " + struct_enroll.getWeek() + "요일 " + hour_ + ":" + min_);
                                        date_set = true;
                                    } else {
                                        mTv_choice_date.setBackgroundResource(R.drawable.box_round_maincolor);
                                        mTv_choice_date.setTextColor(Color.WHITE);
                                        mTv_choice_date.setText(month_ + "월 " + day_ + "일 " + struct_enroll.getWeek() + "요일 " + hour_ + ":" + min_);
                                        date_set = true;
                                    }
                                    dialog.dismiss();
                                }
                            } else if (json.equals("already")) {
                                dialog2 = new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_exit), "해당 날짜에 이미 등록되어있습니다", "확인", "dismiss");
                                dialog2.show();
                            }


                        }
                    });
                    dialog.show();
                    break;
                case R.id.tv_choice_location:
                    if (date_set == false) {
                        dialog2 = new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_exit), "날짜를 먼저 설정해주세요", "확인", "dismiss");
                        dialog2.show();
                    } else if (date_set == true) {
                        Intent intent = new Intent(Page_Main_Matching_Enroll.this, Page_Main_Matching_Enroll_Store.class);
                        intent.putExtra("Struct_Enroll", struct_enroll);
                        startActivity(intent);
                    }
                    break;
                case R.id.personal_icon:
                    if (current_type == 1) {
                        reset(0);
                    }

                    personal.setBackgroundResource(R.drawable.personal_ch);
                    group.setBackgroundResource(R.drawable.group_unch);
                    current_type = 0;
                    Log.e("tag type", current_type + "");
                    struct_enroll.setType(0);
                    struct_enroll.max_no = 2;

                    break;


                case R.id.group_icon:
                    if (status.equals("2")) {
                        if (have_team.equals("2") || have_team.equals("3") || have_team.equals("4")) {
                            if (is_owner.equals("true")) {
                                if (current_type == 0) {
                                    reset(0);
                                }
                                personal.setBackgroundResource(R.drawable.personal_unch);
                                group.setBackgroundResource(R.drawable.group_ch);
                                struct_enroll.max_no = 8;
                                current_type = 1;
                                Log.e("tag type", current_type + "");
                                struct_enroll.setType(1);
                            } else {
                                dialog2 = new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_warning), "팀장만 등록 가능합니다.", "확인", "dismiss");
                                dialog2.show();
                            }

                        }
                    } else if (status.equals("0")) {
                        dialog2 = new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_exit), "팀을 먼저 만들어 주세요", "취소", "팀 만들기", "to_organize_team");
                        dialog2.show();
                    } else {
                        dialog2 = new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_warning), "팀 생성 중입니다.", "확인", "dismiss");
                        dialog2.show();
                    }


                    break;
                case R.id.enroll_next:
                    String date_time, month, hour, day, min;
                    Log.e("tag type", current_type + "");
                    struct_enroll.setType(current_type);
                    Log.e("tag type", struct_enroll.getType() + "");
                    if (date_set == false) {
                        dialog2 = new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_exit), "날짜와 시간을 선택해 주세요", "확인", "dismiss");
                        dialog2.show();
                    } else if (location_set == false) {
                        dialog2 = new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_exit), "장소를 선택해 주세요", "확인", "dismiss");
                        dialog2.show();
                    } else if (edit_ment.getText().equals("") || edit_ment.getText().toString().length() < 1 || edit_ment.getText().toString().length() > 16) {
                        dialog2 = new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_exit), "한마디를 1-15자로 작성해주세요", "확인", "dismiss");
                        dialog2.show();
                    } else {
                        // 현재시간을 msec 으로 구한다.
                        long now = System.currentTimeMillis();
                        // 현재시간을 date 변수에 저장한다.
                        Date date = new Date(now);
                        // 시간을 나타냇 포맷을 정한다 ( yyyy/MM/dd 같은 형태로 변형 가능 )
                        SimpleDateFormat sdfNow = new SimpleDateFormat("yyyyMMddHHmmss");
                        // nowDate 변수에 값을 저장한다.
                        String formatDate = sdfNow.format(date);

                        if (struct_enroll.getMonth() < 10) {
                            month = "0" + struct_enroll.getMonth();
                        } else {
                            month = Integer.toString(struct_enroll.getMonth());
                        }

                        if (struct_enroll.getHour() < 10) {
                            if (struct_enroll.getHour() == 00)
                                hour = "00";
                            else
                                hour = "0" + struct_enroll.getHour();
                        } else
                            hour = Integer.toString(struct_enroll.getHour());

                        if (struct_enroll.getDay() < 10) {
                            day = "0" + struct_enroll.getDay();
                        } else
                            day = Integer.toString(struct_enroll.getDay());

                        if (struct_enroll.getMinute() < 10) {
                            //Log.e("min1", ""+struct_enroll.getMinute());
                            min = "0" + struct_enroll.getMinute();
                        } else {
                            min = Integer.toString(struct_enroll.getMinute());
                            //Log.e("min2",""+struct_enroll.getMinute());
                        }

                        //date_time = Calendar.getInstance().get(Calendar.YEAR) + month+day+hour+min;
                        date_time = Integer.toString(Calendar.getInstance().get(Calendar.YEAR)) + month + day + hour + min;
                        Log.e("date", date_time);

                        try {
                            json = new Util_PHP(Page_Main_Matching_Enroll.this).execute(nick, date_time, Integer.toString(current_type), "Check_enroll", "http://175.126.38.139/php/check_enroll.php").get();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (json.equals("able")) {
                            // 데이터 세팅
                            try {
                                Log.e("ddd", nick + formatDate + " "
                                        + edit_ment.getText().toString() + struct_enroll.getStore_id()
                                        + date_time);
                                json = new Util_PHP(Page_Main_Matching_Enroll.this).execute(nick, formatDate
                                        , edit_ment.getText().toString(), struct_enroll.getStore_id()
                                        , date_time
                                        , Integer.toString(struct_enroll.getType())
                                        , "Insert_Enroll", "http://175.126.38.139/php/insert_enroll.php").get();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (json.contains("success")) {
                                new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_exit), "매칭이 등록되었습니다.", "확인", "matching_enroll").show();
                            } else if (json.contains("fail")) {
                                new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_warning), "error \n재 등록 바랍니다", "확인", "dismiss").show();
                            }
                        } else {
                            new Util_Dialog(Page_Main_Matching_Enroll.this, getResources().getDrawable(R.drawable.icon_exit), "error \n잠시후 다시 시도 해주세요", "확인", "to_main").show();
                        }
                    }
                    break;
                case R.id.enroll_back_btn:
                    finish();
                    break;
            }
        }
    };

    void reset(int type) {

        if (type == 0) {
            struct_enroll.setDay(0);
            struct_enroll.setWeek(null);
            struct_enroll.setMonth(0);

            struct_enroll.setLoc1(null);
            struct_enroll.setName(null);
            struct_enroll.setStore_id(null);


            mTv_choice_date.setBackgroundResource(R.drawable.matching_round);
            mTv_choice_date.setText("선택하기");
            mTv_choice_date.setTextColor(getResources().getColor(R.color.deep_grey));


            mTv_choice_location.setBackgroundResource(R.drawable.matching_round);
            mTv_choice_location.setText("선택하기");
            mTv_choice_location.setTextColor(getResources().getColor(R.color.deep_grey));

            date_set = false;
            location_set = false;
        } else if (type == 1) {
            struct_enroll.setLoc1(null);
            struct_enroll.setName(null);
            struct_enroll.setStore_id(null);

            mTv_choice_location.setBackgroundResource(R.drawable.matching_round);
            mTv_choice_location.setText("선택하기");
            mTv_choice_location.setTextColor(getResources().getColor(R.color.deep_grey));

            location_set = false;
        }
    }


    //날짜 구하기
    public void get_day_of_month() {
        mCalender.get(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < 7; i++) {
            if (i == 0) {
                int j = mCalender.get(Calendar.DAY_OF_MONTH);
                int jj = (mCalender.get(Calendar.MONTH)) + 1;
                day[i] = j;
                month[i] = jj;
            } else {
                mCalender.add(Calendar.DAY_OF_MONTH, 1);
                int j = mCalender.get(Calendar.DAY_OF_MONTH);
                int jj = (mCalender.get(Calendar.MONTH)) + 1;
                day[i] = j;
                month[i] = jj;
            }
        }

    }

    //요일 구하기
    public void get_day_of_week() {
        mCalender.get(Calendar.DAY_OF_WEEK);
        for (int i = 0; i < 7; i++) {
            if (i == 0) {
                int j = mCalender.get(Calendar.DAY_OF_WEEK);
                intweek[i] = Integer.toString(j);
            } else {
                mCalender.add(Calendar.DAY_OF_WEEK, 1);
                int j = mCalender.get(Calendar.DAY_OF_WEEK);
                intweek[i] = Integer.toString(j);
            }
        }
        conver_day_of_week(intweek);
    }


    //시간 차이 구하기....ㅎㅎㅎ

    public void conver_day_of_week(String[] intweek) {

//        intweek[0] = "오늘";
//        intweek[1] = "내일";
        for (int i = 0; i < 7; i++) {
            if (intweek[i].equals("1")) {
                intweek[i] = "월";
            } else if (intweek[i].equals("2")) {
                intweek[i] = "화";
            } else if (intweek[i].equals("3")) {
                intweek[i] = "수";
            } else if (intweek[i].equals("4")) {
                intweek[i] = "목";
            } else if (intweek[i].equals("5")) {
                intweek[i] = "금";
            } else if (intweek[i].equals("6")) {
                intweek[i] = "토";
            } else {
                intweek[i] = "일";
            }
        }

    }

    private void parse_(String data) {
        try {
            JSONObject jsonObj = new JSONObject(data);
            jsonArray = jsonObj.getJSONArray("result");
            Log.e("enroll_", "" + jsonArray + "  " + jsonArray.length());

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject c = jsonArray.getJSONObject(i);
                status = c.getString("status");
                have_team = c.getString("have_team");
                is_owner = c.getString("is_owner");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    //타임픽커 10분 단위
    @SuppressLint("NewApi")
    private void setTimePickerInterval(TimePicker timePicker) {
        try {
            Class<?> classForid = Class.forName("com.android.internal.R$id");
            // Field timePickerField = classForid.getField("timePicker");

            Field field = classForid.getField("minute");
            minutePicker = (NumberPicker) timePicker
                    .findViewById(field.getInt(null));

            minutePicker.setMinValue(0);
            minutePicker.setMaxValue(5);
            displayedValues = new ArrayList<String>();
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
            }
            minutePicker.setDisplayedValues(displayedValues
                    .toArray(new String[0]));
            minutePicker.setWrapSelectorWheel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


