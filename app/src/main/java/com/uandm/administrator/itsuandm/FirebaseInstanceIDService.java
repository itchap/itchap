package com.uandm.administrator.itsuandm;

/**
 * Created by u on 2017-01-17.
 */

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;


import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String nick;
//
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {

        // Get updated InstanceID token.
        String token = FirebaseInstanceId.getInstance().getToken();
//        String nick = pref.getString("nick", "");
        Log.d(TAG, "Refreshed token: " + token);


        // 생성등록된 토큰을 개인 앱서버에 보내 저장해 두었다가 추가 뭔가를 하고 싶으면 할 수 있도록 한다.
        sendRegistrationToServer(token);
    }

    public void aa(String aa) {
        nick=aa;
        onTokenRefresh();
    }

    private void sendRegistrationToServer(String token) {
        Log.e("FirebaService", "" + "dsf");
        // Add custom implementation, as needed.
        Log.d(TAG, "Refreshed token: " + token);


        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("Token", token)
                .build();
        //request
        Request request = new Request.Builder()
                .url("http://175.126.38.139/php/fcm/register.php")
                .post(body)
                .build();

        try {
            client.newCall(request).execute();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
