package com.uandm.administrator.itsuandm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by u on 2017-02-13.
 */
public class BroadcastD extends BroadcastReceiver {
    String INTENT_ACTION = Intent.ACTION_BOOT_COMPLETED;
    private static final String TAG = BroadcastD.class.getSimpleName();
    private String nick;

    @Override
    public void onReceive(Context context, Intent intent) {//알람 시간이 되었을때 onReceive를 호출함
        //NotificationManager 안드로이드 상태바에 메세지를 던지기위한 서비스 불러오고
        Log.d(TAG, "BroadcastReceiver :: com.dwfox.myapplication.SEND_BROAD_CAST :: ");

//        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, Util_PHP.class), PendingIntent.FLAG_UPDATE_CURRENT);
//        Notification.Builder builder = new Notification.Builder(context);
//        builder.setSmallIcon(R.drawable.icon_flower).setTicker("HETT").setWhen(System.currentTimeMillis())
//                .setNumber(1).setContentTitle("푸쉬 제목").setContentText("푸쉬내용")
//                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE).setContentIntent(pendingIntent).setAutoCancel(true);
//        notificationmanager.notify(1, builder.build());
        nick = intent.getStringExtra("nick");
        Log.e("6066 nick", nick);
        Log.e("606", "onReceive7");
        getData("http://175.126.38.139/php/fcm/sss.php");
        Log.e("606", "onReceive8");

//        try {
//            Log.e("606", "onReceive7");
//            new Util_PHP(context).execute("sss", "175.126.38.139/php/fcm/sss.php?team_owner="+"설현").get();
//            Log.e("606", "onReceive8");
//        } catch (Exception e) {
//            e.getStackTrace();
//        }
    }

    public String getData(String url) {
        class GetDataJSON extends AsyncTask<String, Void, String> {
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";

            @Override
            protected String doInBackground(String... params) {
                Log.e("606", "onReceive9");
                DataOutputStream os;
                String uri = params[0];
                BufferedReader bufferedReader = null;
                StringBuilder sb = null;
                try {
                    Log.e("606", "onReceive8");
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Connection", "Keep-Alive");
                    con.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    os = new DataOutputStream(con.getOutputStream());


                    String my_nick = nick;
                    os.writeBytes(twoHyphens + boundary + lineEnd);
                    os.writeBytes("Content-Disposition: form-data; name=\"my_nick\"" + lineEnd);
                    os.writeBytes(lineEnd);
                    os.write(my_nick.getBytes());
                    os.writeBytes(lineEnd);

                    os.flush();
                    os.close();

                    BufferedReader rd = null;
                    rd = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                    sb = new StringBuilder();
                    String json;
                    while ((json = rd.readLine()) != null) {
                        sb.append(json + "\n");
                    }
                    Log.e("exper", sb.toString().trim());
                    return sb.toString().trim();
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Log.e("606", "onPreExecute");
            }


            @Override
            protected void onPostExecute(String result) {
                Log.e("606", "onPostExecute");
            }
        }


        String result2 = null;
        GetDataJSON g = new GetDataJSON();
        try {
            result2 = g.execute(url).get();
            Log.e("606", "result2 : " + result2);

        } catch (Exception e) {

        }
        return result2;
    }

}


//    public void onReceive(Context context, Intent intent) {//알람 시간이 되었을때 onReceive를 호출함
//        Log.e("606", "onReceive1");
//        //NotificationManager 안드로이드 상태바에 메세지를 던지기위한 서비스 불러오고
//        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, Page_Splash.class), PendingIntent.FLAG_UPDATE_CURRENT);
//        Notification.Builder builder = new Notification.Builder(context);
//        Log.e("606", "onReceive2");
//        builder.setSmallIcon(R.drawable.heart).setTicker("HETT").setWhen(System.currentTimeMillis())
//                .setNumber(1).setContentTitle("푸쉬 제목").setContentText("푸쉬내용")
//                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE).setContentIntent(pendingIntent).setAutoCancel(true);
//        Log.e("606", "onReceive3");
//        notificationmanager.notify(1, builder.build());
//
//        Bundle bundle = intent.getExtras();
//        Log.e("606", "onReceive4");
//        Log.e("606", "onReceive5");
//        String s = bundle.getString("nick");
//        Log.e("606", "onReceive5"+s);
//        //NotificationManager 안드로이드 상태바에 메세지를 던지기위한 서비스 불러오고
//
//        try {
//            Log.e("606", "onReceive7");
//            json = new Util_PHP(context).execute("sss", "175.126.38.139/php/fcm/sss.php?team_owner="+s).get();
//            Log.e("606", "onReceive8");
//        } catch (Exception e) {
//            e.getStackTrace();
//        }
////        Notification.Builder builder = new Notification.Builder(context);
////        builder.setSmallIcon(R.drawable.on).setTicker("HETT").setWhen(System.currentTimeMillis())
////                .setNumber(1).setContentTitle("푸쉬 제목").setContentText("푸쉬내용")
////                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE).setContentIntent(pendingIntent).setAutoCancel(true);
////
////        notificationmanager.notify(1, builder.build());
//    }
//}