package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.Dialog;
import android.content.*;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.tsengvn.typekit.TypekitContextWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Page_Chatting extends AppCompatActivity {

    private RecyclerView recyclerView;
    private EditText edit_msg;
    private RelativeLayout content, btn_back;
    private LinearLayout btn_enter;

    private ArrayList<Page_Chatting_Item> item;
    private Page_Chatting_Adapter adapter;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private String json, nick, nick_, msg, send_time, no, team_count, to_send[], other;

    private JSONArray jsonArray;

    private String id, open_time, close_time, my_img;
    private String img_;

    private FirebaseRemoteConfig firebaseRemoteConfig;
    BroadcastReceiver receiver;
    private RequestManager requestManager;

    private Dialog dialogg;
    private Util_Dialog dialog;
    private ImageView report;
    private int index = 0;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onStart() {
        Log.e("onstar", "onstar");
        super.onStart();
        item.clear();
        try {
            json = new Util_PHP(Page_Chatting.this).execute(no, team_count, "chatting_history", "http://175.126.38.139/php/getdata_chatting_history.php").get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        parse_(json);

        recyclerView.scrollToPosition(item.size() - 1);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_chatting_main);
        Log.e("oncreate", "oncre");
        requestManager = Glide.with(Page_Chatting.this);

        //team count 받고.
        //컴플릿 넘버 받고.
        //단체
//        no = "1"; //컴플릿 넘버
//        team_count ="2";// 팀 카운트
        //개인
        Intent ing_intent = getIntent();
        team_count = Integer.toString(ing_intent.getIntExtra("team_count", -1));
        no = Integer.toString(ing_intent.getIntExtra("cmp_no", -1));
        other = ing_intent.getStringExtra("other");

        Log.e("getinent", team_count + " " + no);


        pref = getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();
        nick = pref.getString("nick", "null");

        Log.e("create", nick);

//        if (getIntent().getExtras() != null) {
//            for (String key : getIntent().getExtras().keySet()) {
//                Object value = getIntent().getExtras().get(key);
//                Log.e("chatting", "Key: " + key + " Value: " + value);
//            }
//        }
        try {
            json = new Util_PHP(Page_Chatting.this).execute(no, team_count, nick, "chatting_room", "http://175.126.38.139/php/getdata_chatting.php").get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        parse(json);


        IntentFilter filter = new IntentFilter();
        filter.addAction("CHATTING");

        item = new ArrayList<>();

        LinearLayoutManager layout = new LinearLayoutManager(Page_Chatting.this, LinearLayoutManager.VERTICAL, false);
        //RecyclerViewPager.LayoutParams layoutParams = new RecyclerView.LayoutParams(new RecyclerView.LayoutParams(RecyclerViewPager.LayoutParams.MATCH_PARENT,RecyclerViewPager.LayoutParams.MATCH_PARENT));
        //viewpager.setLayoutParams(layoutParams);
        adapter = new Page_Chatting_Adapter(item, Page_Chatting.this, requestManager);
        report = (ImageView) findViewById(R.id.report);
        report.setOnClickListener(new View.OnClickListener() {
            private LinearLayout report_content;
            private TextView report_cancel, report_submit;
            private EditText report_edit;
            private String report_json;
            private RadioGroup report_radio;

            @Override
            public void onClick(View v) {
                dialogg = new Dialog(Page_Chatting.this);
                dialogg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogg.setContentView(R.layout.l_ing_dialog_report);
                dialogg.setCancelable(false);
                dialogg.show();

                report_content = (LinearLayout) dialogg.findViewById(R.id.report_content);
                report_edit = (EditText) dialogg.findViewById(R.id.report_edit);
                report_cancel = (TextView) dialogg.findViewById(R.id.report_cancel);
                report_submit = (TextView) dialogg.findViewById(R.id.report_submit);
                report_radio = (RadioGroup) dialogg.findViewById(R.id.report_ra);
                report_radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checked_id) {
                        if (checked_id != -1) {
                            RadioButton rb = (RadioButton) dialogg.findViewById(checked_id);
                            if (rb != null) {
                                index = group.indexOfChild(rb);
                                Log.e("radio", "" + index);

                            }
                        }
                    }
                });

                report_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogg.dismiss();
                    }
                });

                report_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("radio", "" + report_radio.getCheckedRadioButtonId());
                        if (report_edit.getText().toString().equals("") || report_edit.getText().toString() == null) {
                            dialog = new Util_Dialog(Page_Chatting.this, Page_Chatting.this.getResources().getDrawable(R.drawable.icon_lastpage), "내용을 입력해주세요", "확인", "dismiss");
                            dialog.show();
                        } else {
                            try {
                                report_json = new Util_PHP(Page_Chatting.this).execute(pref.getString("nick", ""), "2" + index, report_edit.getText().toString(), other, "feedback", "http://175.126.38.139/php/feedback.php").get();
                                if (report_json.equals("success")) {
                                    dialog = new Util_Dialog(Page_Chatting.this, Page_Chatting.this.getResources().getDrawable(R.drawable.icon_check), "신고가 접수 되었습니다", "확인", "dismiss");
                                    dialog.show();
                                    dialogg.dismiss();
                                } else if (report_json.equals("error")) {
                                    dialog = new Util_Dialog(Page_Chatting.this, Page_Chatting.this.getResources().getDrawable(R.drawable.icon_check), "실패했습니다. 다시 신고해주세요", "확인", "dismiss");
                                    dialog.show();
                                    dialogg.dismiss();
                                }
                            } catch (Exception e) {
                                dialogg.dismiss();
                            }
                        }
                    }
                });


            }
        });
        Findid();
        recyclerView.setLayoutManager(layout);
        recyclerView.setAdapter(adapter);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String str = intent.getStringExtra("result");
                String msg = intent.getStringExtra("msg");
                String _img = intent.getStringExtra("img");
                String from_nick = intent.getStringExtra("from_nick");
                String sendT = intent.getStringExtra("sendtime");
                Log.e("chat result", str + " " + sendT);

                Log.e("asdfasdfs", item.size() + "" + msg + _img + from_nick + sendT);

                // 오너 전지현 // 센더 공유
                if (str.equals("success")) {
                    if (item.size() == 0) {
                        item.add(new Page_Chatting_Item(Page_Chatting.this, _img, msg, "1", id, sendT, from_nick, false));
                        Log.e("asdfsdfsdfsa", "add");
                    } else if (item.get(item.size() - 1).getNick().equals(from_nick) && item.get(item.size() - 1).getDateTime().equals(sendT)) {
                        item.get(item.size() - 1).setDateTime(null);
                        item.add(new Page_Chatting_Item(Page_Chatting.this, null, msg, "1", id, sendT, from_nick, false));
                    } else if (item.get(item.size() - 1).getNick().equals(from_nick) && !item.get(item.size() - 1).getDateTime().equals(sendT)) {
                        //상대방 보낸사람 이미지
                        item.add(new Page_Chatting_Item(Page_Chatting.this, null, msg, "1", id, sendT, from_nick, false));
                    } else
                        item.add(new Page_Chatting_Item(Page_Chatting.this, _img, msg, "1", id, sendT, from_nick, false));
                    adapter.notifyDataSetChanged();
                    recyclerView.smoothScrollToPosition(item.size() - 1);
                }
            }
        };

        registerReceiver(receiver, filter);


    }

    void Findid() {

        recyclerView = (RecyclerView) findViewById(R.id.chat_recycle);
        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    recyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (recyclerView.getAdapter().getItemCount() == 0) {
                                return;
                            } else {
                                recyclerView.smoothScrollToPosition(recyclerView.getAdapter().getItemCount() - 1);
                            }
                        }
                    }, 100);
                }
            }
        });
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        hide_keyboard();
                        break;
                }
                return false;
            }
        });

        edit_msg = (EditText) findViewById(R.id.messageEdit);

        btn_enter = (LinearLayout) findViewById(R.id.chat_send_layout);
        btn_enter.setOnClickListener(listener);
        btn_back = (RelativeLayout) findViewById(R.id.chat_back_btn);
        btn_back.setOnClickListener(listener);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


    }

    void hide_keyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit_msg.getWindowToken(), 0);
    }

    //초기 데이터 가져오기
    private void parse(String data) {
        try {
            JSONObject jsonObj = new JSONObject(data);
            jsonArray = jsonObj.getJSONArray("result");
            Log.e("chatting", "" + jsonArray + "  " + jsonArray.length());

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject c = jsonArray.getJSONObject(i);

                id = c.getString("id");
                open_time = c.getString("open_time");
                close_time = c.getString("close_time");
                to_send = new String[Integer.parseInt(team_count) * 2 - 1];
                my_img = c.getString("img");

                if (team_count.equals("1")) {
                    to_send[0] = c.getString("nick1");
                } else if (team_count.equals("2")) {
                    to_send[0] = c.getString("nick1");
                    to_send[1] = c.getString("nick2");
                    to_send[2] = c.getString("nick3");
                } else if (team_count.equals("3")) {
                    to_send[0] = c.getString("nick1");
                    to_send[1] = c.getString("nick2");
                    to_send[2] = c.getString("nick3");
                    to_send[3] = c.getString("nick4");
                    to_send[4] = c.getString("nick5");
                } else if (team_count.equals("4")) {
                    to_send[0] = c.getString("nick1");
                    to_send[1] = c.getString("nick2");
                    to_send[2] = c.getString("nick3");
                    to_send[3] = c.getString("nick4");
                    to_send[4] = c.getString("nick5");
                    to_send[5] = c.getString("nick6");
                    to_send[6] = c.getString("nick7");
                }


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    //히스토리 가져오기
    private void parse_(String data) {
        try {
            JSONObject jsonObj = new JSONObject(data);
            jsonArray = jsonObj.getJSONArray("result");
            Log.e("chatting_his", "" + jsonArray + "  " + jsonArray.length());

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject c = jsonArray.getJSONObject(i);

                id = c.getString("id");
                Log.e("fcm_chat", "" + id);
                nick_ = c.getString("nick");
                msg = c.getString("msg");
                // Log.e("sdafsadfsadf", c.getString("sendT"));
                send_time = c.getString("sendT");
                img_ = c.getString("img");


                if (nick.equals(nick_)) {
                    if (item.size() == 0) {
                        item.add(new Page_Chatting_Item(Page_Chatting.this, img_, msg, "1", id, send_time, nick_, true));
                    } else if (item.get(i - 1).getNick().equals(nick_) && item.get(i - 1).getDateTime().equals(send_time)) {
                        item.get(i - 1).setDateTime(null);
                        item.add(new Page_Chatting_Item(Page_Chatting.this, null, msg, "1", id, send_time, nick_, true));
                    } else if (item.get(i - 1).getNick().equals(nick_) && !item.get(i - 1).getDateTime().equals(send_time)) {
                        item.add(new Page_Chatting_Item(Page_Chatting.this, null, msg, "1", id, send_time, nick_, true));
                    } else {
                        item.add(new Page_Chatting_Item(Page_Chatting.this, img_, msg, "1", id, send_time, nick_, true));
                    }
                    //Log.e("nick>", nick_);

                    //   오른쪽
                } else {
                    if (item.size() == 0) {
                        item.add(new Page_Chatting_Item(Page_Chatting.this, img_, msg, "1", id, send_time, nick_, false));
                    } else if (item.get(i - 1).getNick().equals(nick_) && item.get(i - 1).getDateTime().equals(send_time)) {
                        item.get(i - 1).setDateTime(null);
                        item.add(new Page_Chatting_Item(Page_Chatting.this, null, msg, "1", id, send_time, nick_, false));
                    } else if (item.get(i - 1).getNick().equals(nick_) && !item.get(i - 1).getDateTime().equals(send_time)) {
                        item.add(new Page_Chatting_Item(Page_Chatting.this, null, msg, "1", id, send_time, nick_, false));
                    } else
                        item.add(new Page_Chatting_Item(Page_Chatting.this, img_, msg, "1", id, send_time, nick_, false));

                    //Log.e("nick<", nick_);

                    // 왼쪽
                }
                // Log.e("ddd", item.get(i).getNick());

                //  Log.e("size", item.size()+""+item.get(i).getImg());
            }
            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.chat_send_layout:

                    Log.e("1212", "sdas");
                    String msg = edit_msg.getText().toString();

                    if (msg.equals("") || msg == null) {
                        break;
                    }

                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");
                    String today_ = format.format(today);
                    //today_ = convert(today_);

                    try {

                        if (team_count.equals("1")) {
                            json = new Util_PHP(Page_Chatting.this).execute(nick, team_count, to_send[0], msg, id, today_, "Insert_msg", "http://175.126.38.139/php/fcm_chatting.php").get();
                        } else if (team_count.equals("2")) {
                            json = new Util_PHP(Page_Chatting.this).execute(nick, team_count, to_send[0], to_send[1], to_send[2], msg, id, today_, "Insert_msg2", "http://175.126.38.139/php/fcm_chatting.php").get();
                        } else if (team_count.equals("3")) {
                            json = new Util_PHP(Page_Chatting.this).execute(nick, team_count, to_send[0], to_send[1], to_send[2], to_send[3], to_send[4], msg, id, today_, "Insert_msg3", "http://175.126.38.139/php/fcm_chatting.php").get();
                        } else if (team_count.equals("4")) {
                            json = new Util_PHP(Page_Chatting.this).execute(nick, team_count, to_send[0], to_send[1], to_send[2], to_send[3], to_send[4], to_send[5], to_send[6], msg, id, today_, "Insert_msg4", "http://175.126.38.139/php/fcm_chatting.php").get();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (item.size() == 0) {
                        item.add(new Page_Chatting_Item(Page_Chatting.this, null, msg, "1", id, today_, nick, true));
                    } else if (item.get(item.size() - 1).getNick().equals(nick) && item.get(item.size() - 1).getDateTime().equals(today_)) {
                        item.get(item.size() - 1).setDateTime(null);
                        item.add(new Page_Chatting_Item(Page_Chatting.this, null, msg, "1", id, today_, nick, true));
                    } else {
                        item.add(new Page_Chatting_Item(Page_Chatting.this, null, msg, "1", id, today_, nick, true));
                    }

                    adapter.notifyDataSetChanged();
                    edit_msg.setText("");
                    recyclerView.scrollToPosition(item.size() - 1);

                    edit_msg.requestFocus();
                    InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.showSoftInput(edit_msg, InputMethodManager.SHOW_FORCED);

                    break;

                case R.id.chat_back_btn:
                    finish();
                    break;


            }

        }
    };


}
