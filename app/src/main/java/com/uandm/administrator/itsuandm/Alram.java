package com.uandm.administrator.itsuandm;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;
import com.tsengvn.typekit.TypekitContextWrapper;

import me.leolin.shortcutbadger.ShortcutBadger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class Alram extends AppCompatActivity {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private RecyclerView recyclerViewPager;
    private Alram_Recycler_Adapter adapter;
    private ArrayList<Alram_ListItem> item;

    private RelativeLayout on_off, btn_back;
    private ImageView on_off_img;
    private String img[];
    private boolean is_off = false;
    private String json;
    private RelativeLayout no_item;
    RequestManager requestManager;
    private String push_y_n = "0";
    JSONArray alram_list = null;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private static final String TAG_RESULTS = "result";

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_alram);

        item = new ArrayList<>();
        findid();


        requestManager = Glide.with(Alram.this);
        pref = this.getSharedPreferences("login", 0);
        editor = pref.edit();
        try {
            json = new Util_PHP(Alram.this).execute(pref.getString("nick", ""), "getdata_alram", "http://175.126.38.139/php/getdata_alram.php").get();
        } catch (Exception e) {
            e.getStackTrace();
        }
        rendering(json);


        adapter = new Alram_Recycler_Adapter(item, this, requestManager);
        LinearLayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewPager.setLayoutManager(layout);
        recyclerViewPager.setAdapter(adapter);

        if (push_y_n.equals("0")) {
            on_off_img.setBackgroundResource(R.drawable.icon_alram_on);
        } else if (push_y_n.equals("1")) {
            on_off_img.setBackgroundResource(R.drawable.icon_alram_off);
        } else {
        }

    }

    void findid() {

        recyclerViewPager = (RecyclerView) findViewById(R.id.alram_recycler);
        on_off_img = (ImageView) findViewById(R.id.alram_on_off_img);
        on_off = (RelativeLayout) findViewById(R.id.alram_on_off);
        on_off.setOnClickListener(btnlistener);
        btn_back = (RelativeLayout) findViewById(R.id.alram_back_btn);
        btn_back.setOnClickListener(btnlistener);
        no_item = (RelativeLayout) findViewById(R.id.no_item);

    }

    View.OnClickListener btnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.alram_on_off:
                    if (push_y_n.equals("1")) {
                        on_off_img.setBackgroundResource(R.drawable.icon_alram_on);
                        push_y_n = "0";
                    } else {
                        on_off_img.setBackgroundResource(R.drawable.icon_alram_off);
                        push_y_n = "1";
                    }
                    try {
                        new Util_PHP(Alram.this).execute(pref.getString("nick", ""), push_y_n, "push_y_n", "http://175.126.38.139/php/push_y_n.php").get();
                        //editor.putString("push_y_n", push_y_n);
                    } catch (Exception e) {

                    }
                    break;
                case R.id.alram_back_btn:
                    finish();
//                    Intent intent = NavUtils.getParentActivityIntent(Alram.this);
//                    if (NavUtils.shouldUpRecreateTask(Alram.this, intent)) {
//                        TaskStackBuilder.create(Alram.this)
//                                .addNextIntentWithParentStack(intent)
//                                .startActivities();
//                    } else {
//                        NavUtils.navigateUpTo(Alram.this, intent);
//                    }
                    break;
            }
        }
    };

    protected void rendering(String json) {
        try {
            if (json.contains("no_alram_list")) {
                if (json.contains("0")) {
                    push_y_n = "0";
                } else if (json.contains("1")) {
                    push_y_n = "1";
                }
            } else {
                no_item.setVisibility(GONE);
                recyclerViewPager.setVisibility(VISIBLE);
                JSONObject jsonObj = new JSONObject(json);
                alram_list = jsonObj.getJSONArray(TAG_RESULTS);
                for (int i = 0; i < alram_list.length(); i++) {
                    JSONObject c = alram_list.getJSONObject(i);
                    push_y_n = c.getString("push_y_n");
                    img = new String[Integer.parseInt(c.getString("img_cnt"))];
                    switch (Integer.parseInt(c.getString("img_cnt"))) {
                        case 1:
                            img[0] = c.getString("img1");
                            break;
                        case 2:
                            img[0] = c.getString("img1");
                            img[1] = c.getString("img2");
                            break;
                        case 3:
                            img[0] = c.getString("img1");
                            img[1] = c.getString("img2");
                            img[2] = c.getString("img3");
                            break;
                        case 4:
                            img[0] = c.getString("img1");
                            img[1] = c.getString("img2");
                            img[2] = c.getString("img3");
                            img[3] = c.getString("img4");
                            break;
                    }
                    item.add(new Alram_ListItem(this, img, getResources().getDrawable(R.drawable.icon_heart), c.getString("time"), c.getString("title"), c.getString("sender_nick"), c.getString("alram_status"), c.getString("alram_type"), c.getInt("img_cnt"), c.getString("alram_id")));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
