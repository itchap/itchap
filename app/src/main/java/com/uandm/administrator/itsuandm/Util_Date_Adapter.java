package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import java.util.ArrayList;

/**
 * Created by young on 2016-11-22.
 */

public class Util_Date_Adapter extends RecyclerView.Adapter<Util_Date_Adapter.ViewHolder> {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList//
    private ArrayList<Util_Date_Item> listViewItemList;
    private Context context;
    public static final int ITEM_TYPE_ONE = 1;
    public static final int ITEM_TYPE_TWO = 2;
    private boolean[] selected = new boolean[7];
    private boolean init = true;
    private int type = -1;
    private int type_filter_enroll = 0;
    private String json;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;


    public Util_Date_Adapter(ArrayList<Util_Date_Item> item, Context context) {
        this.context = context;
        this.listViewItemList = item;
    }

    public Util_Date_Adapter(ArrayList<Util_Date_Item> item, Context context, int type) {
        this.context = context;
        this.listViewItemList = item;
        this.type_filter_enroll = type;
    }

    @Override
    public int getItemViewType(int position) {

        if (listViewItemList.get(position).getType() == 1) {
            this.type = 1;
            return ITEM_TYPE_ONE;
        } else {
            this.type = 2;
            return ITEM_TYPE_TWO;
        }


    }

    /**
     * 레이아웃을 만들어서 Holer에 저장
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;
        pref = context.getSharedPreferences("login", 0);
        editor = pref.edit();

        if (viewType == ITEM_TYPE_ONE) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_date_item, viewGroup, false);
            return new ViewHolder(view, ITEM_TYPE_ONE);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_status_7_item, viewGroup, false);
            return new ViewHolder(view, ITEM_TYPE_TWO);
        }

    }


    /**
     * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);

        if (itemtype == ITEM_TYPE_ONE) {

            switch (position) {
                case 0:
                    if (init) {
                        viewHolder.intweek.setTextColor(context.getResources().getColor(R.color.app_default));
                        viewHolder.day.setBackgroundResource(R.drawable.round_date_day);
                        init = false;
                        selected[position] = true;
                    } else if (selected[position]) {
                        viewHolder.intweek.setTextColor(context.getResources().getColor(R.color.app_default));
                        viewHolder.day.setBackgroundResource(R.drawable.round_date_day);
                    } else {
                        viewHolder.intweek.setTextColor(context.getResources().getColor(R.color.deep_grey));
                        viewHolder.day.setBackgroundColor(context.getResources().getColor(R.color.white));
                        viewHolder.day.setTextColor(context.getResources().getColor(R.color.deep_grey));
                    }
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    if (selected[position]) {
                        viewHolder.intweek.setTextColor(context.getResources().getColor(R.color.app_default));
                        viewHolder.day.setBackgroundResource(R.drawable.round_date_day);
                    } else {
                        viewHolder.intweek.setTextColor(context.getResources().getColor(R.color.deep_grey));
                        viewHolder.day.setBackgroundColor(context.getResources().getColor(R.color.white));
                        viewHolder.day.setTextColor(context.getResources().getColor(R.color.deep_grey));
                    }
                    break;

            }

            if (position == 0) {
                viewHolder.intweek.setText("오늘");
            } else if (position == 1) {
                viewHolder.intweek.setText("내일");
            } else {
                viewHolder.intweek.setText(listViewItemList.get(position).getIntweek());
            }
            viewHolder.day.setText(listViewItemList.get(position).getDay());
            viewHolder.content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selected[position]) {
                        viewHolder.intweek.setTextColor(context.getResources().getColor(R.color.deep_grey));
                        viewHolder.day.setBackgroundColor(context.getResources().getColor(R.color.white));
                        viewHolder.day.setTextColor(context.getResources().getColor(R.color.deep_grey));
                        selected[position] = false;
                    } else {
                        viewHolder.intweek.setTextColor(context.getResources().getColor(R.color.app_default));
                        viewHolder.day.setBackgroundResource(R.drawable.round_date_day);
                        selected[position] = true;

                    }
                    if (type_filter_enroll == 1) {
                        for (int i = 0; i < selected.length; i++) {
                            if (i == position)
                                continue;
                            selected[i] = false;
                        }
                        notifyDataSetChanged();
                    }

                }

            });

        } else {
            if (position == 0) {
                viewHolder.day.setText("오늘");
                viewHolder.day.setTextColor(context.getResources().getColor(R.color.app_default));
            } else if (position == 1) {
                viewHolder.day.setText("내일");
            } else {
                viewHolder.day.setText(listViewItemList.get(position).getDay());
            }

            if (position == 0 && !listViewItemList.get(position).getFull_date().equals("")) {
                viewHolder.day.setTextColor(context.getResources().getColor(R.color.white));
                viewHolder.content.setBackgroundResource(R.drawable.round_bg_red);
            } else if (position != 0 && !listViewItemList.get(position).getFull_date().equals("")) {
                viewHolder.day.setTextColor(context.getResources().getColor(R.color.white));
                viewHolder.content.setBackgroundResource(R.drawable.round_bg_main_color);
            }

            viewHolder.content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!listViewItemList.get(position).getFull_date().equals("")) {
                        Intent intent1 = new Intent(context, A_Status_Us_Info.class);
                        intent1.putExtra("nick", pref.getString("nick", ""));
                        intent1.putExtra("full_date", listViewItemList.get(position).getFull_date());
                        intent1.putExtra("type", listViewItemList.get(position).getType_p_g());
//                        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent1);
                    } else {
                        Log.e("un_selected", "" + 12);
                    }

                }
            });
        }


    }


    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }

    public void getCurrentItem(Struct_Enroll struct_enroll) {
        for (int i = 0; i < selected.length; i++) {
            if (selected[i]) {
                struct_enroll.setDay(Integer.parseInt(listViewItemList.get(i).getDay()));
                struct_enroll.setWeek(listViewItemList.get(i).getIntweek());
                struct_enroll.setMonth(listViewItemList.get(i).getMonth());
            }
        }
    }

    public String getCurrentItem_() {
        String str= "";
        String month = "";
        String day = "";
        for (int i = 0; i < selected.length; i++) {
            if (selected[i]) {
                if(listViewItemList.get(i).getMonth() < 10){
                    month = "0"+listViewItemList.get(i).getMonth();
                }else
                    month = ""+listViewItemList.get(i).getMonth();
                if(Integer.parseInt(listViewItemList.get(i).getDay()) < 10){
                    day = "0"+listViewItemList.get(i).getDay();
                }else
                    day = listViewItemList.get(i).getDay();
                str += month+day+" ";
            }
        }

        return str;
    }


    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView intweek, day;
        protected RelativeLayout content;

        public ViewHolder(View itemView, int type) {
            super(itemView);
            if (type == 1) {
                intweek = (TextView) itemView.findViewById(R.id.intweek);
            } else if (type == 2) {
                intweek = null;
            }
            day = (TextView) itemView.findViewById(R.id.day);
            content = (RelativeLayout) itemView.findViewById(R.id.content);
        }
    }

}


