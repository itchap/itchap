package com.uandm.administrator.itsuandm;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.ArrayList;
import java.util.Calendar;

public class Page_Main_Matching_Filter extends AppCompatActivity {

    private TextView no_1, no_2, no_3, no_4, ageMin, ageMax;
    private CrystalRangeSeekbar age;

    private Calendar mCalender = Calendar.getInstance();
    private int[] day = new int[7];
    private int[] month = new int[7];
    private String intweek[] = new String[7];

    private TextView btn_choice_location, btn_confirm;
    private RecyclerView recyclerView;
    private RelativeLayout btn_back;

    private Util_Date_Adapter adapter;
    private ArrayList<Util_Date_Item> item;

    private Struct_Filter struct_filter;
    private boolean selected_team[] = new boolean[4];
    Intent intent;
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    //
    @Override
    public void onBackPressed() {
        intent = new Intent();
        setResult(1, intent);
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_main_matching_filter);

        struct_filter = new Struct_Filter();

        //숫자 1234 (선택인원)
        no_1 = (TextView) findViewById(R.id.no_1);
        no_1.setOnClickListener(btnlistener);
        no_2 = (TextView) findViewById(R.id.no_2);
        no_2.setOnClickListener(btnlistener);
        no_3 = (TextView) findViewById(R.id.no_3);
        no_3.setOnClickListener(btnlistener);
        no_4 = (TextView) findViewById(R.id.no_4);
        no_4.setOnClickListener(btnlistener);

        age = (CrystalRangeSeekbar) findViewById(R.id.age);
        ageMin = (TextView) findViewById(R.id.age_min);
        ageMax = (TextView) findViewById(R.id.age_max);

        btn_choice_location = (TextView) findViewById(R.id.tv_choice_location);
        btn_choice_location.setOnClickListener(btnlistener);

        btn_back = (RelativeLayout)findViewById(R.id.filter_back_btn);
        btn_back.setOnClickListener(btnlistener);


        btn_confirm = (TextView)findViewById(R.id.btn_filter_next);
        btn_confirm.setOnClickListener(btnlistener);
        // set listener
        age.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                ageMin.setText(String.valueOf(minValue));
                ageMax.setText(String.valueOf(maxValue));
            }
        });

        age.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
              @Override
        public void finalValue(Number minValue, Number maxValue) {
        }
    });

    get_day_of_month();
    get_day_of_week();

        item = new ArrayList<>();

        for (int i = 0; i < day.length; i++) {
            item.add(new Util_Date_Item(intweek[i], day[i], month[i],1));
            // 요일, 날짜
        }

        adapter = new Util_Date_Adapter(item, getApplicationContext(), 2);


        recyclerView = (RecyclerView) findViewById(R.id.filter_recycle);
        recyclerView.setAdapter(adapter);
        recyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(Page_Main_Matching_Filter.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);

        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case 0:
               String team_loc =  data.getStringExtra("keey");
                if(resultCode == 0){
                    if(team_loc.equals("/")){
                    }else{
                        btn_choice_location.setText(team_loc);
                        btn_choice_location.setTextColor(getResources().getColor(R.color.white));
                        btn_choice_location.setBackgroundResource(R.color.app_default);
                    }
                }else{
                }

                struct_filter.setLoc(team_loc);
                break;
        }
    }

    //날짜 구하기
    public void get_day_of_month() {
        mCalender.get(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < 7; i++) {
            if (i == 0) {
                int j = mCalender.get(Calendar.DAY_OF_MONTH);
                int jj = (mCalender.get(Calendar.MONTH)) + 1;
                day[i] = j;
                month[i] = jj;
            } else {
                mCalender.add(Calendar.DAY_OF_MONTH, 1);
                int j = mCalender.get(Calendar.DAY_OF_MONTH);
                int jj = (mCalender.get(Calendar.MONTH)) + 1;
                day[i] = j;
                month[i] = jj;
            }
            Log.i("#month : #day", "" + month[i] + " 월 " + day[i] + "일");
        }

    }

    //요일 구하기
    public void get_day_of_week() {
        mCalender.get(Calendar.DAY_OF_WEEK);
        for (int i = 0; i < 7; i++) {
            if (i == 0) {
                int j = mCalender.get(Calendar.DAY_OF_WEEK);
                intweek[i] = Integer.toString(j);
            } else {
                mCalender.add(Calendar.DAY_OF_WEEK, 1);
                int j = mCalender.get(Calendar.DAY_OF_WEEK);
                intweek[i] = Integer.toString(j);
            }
        }
        conver_day_of_week(intweek);
        for (int i = 0; i < 7; i++) {
            Log.i("########", intweek[i]);
        }
    }

    public void conver_day_of_week(String[] intweek) {
        intweek[0] = "오늘";
        intweek[1] = "내일";
        for (int i = 2; i < 7; i++) {
            if (intweek[i].equals("1")) {
                intweek[i] = "월";
            } else if (intweek[i].equals("2")) {
                intweek[i] = "화";
            } else if (intweek[i].equals("3")) {
                intweek[i] = "수";
            } else if (intweek[i].equals("4")) {
                intweek[i] = "목";
            } else if (intweek[i].equals("5")) {
                intweek[i] = "금";
            } else if (intweek[i].equals("6")) {
                intweek[i] = "토";
            } else {
                intweek[i] = "일";
            }
        }

    }


    View.OnClickListener btnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          //  Intent intent;
            switch (v.getId()) {
                case R.id.no_1:
                    if (no_1.getCurrentTextColor() == getResources().getColor(R.color.deep_grey)) {
                        no_1.setTextColor(getResources().getColor(R.color.app_default));
                        selected_team[0] = true;
                    } else {
                        no_1.setTextColor(getResources().getColor(R.color.deep_grey));
                        selected_team[0] = false;
                    }
                    break;
                case R.id.no_2:
                    if (no_2.getCurrentTextColor() == getResources().getColor(R.color.deep_grey)) {
                        no_2.setTextColor(getResources().getColor(R.color.app_default));
                        selected_team[1] = true;
                    } else {
                        no_2.setTextColor(getResources().getColor(R.color.deep_grey));
                        selected_team[1] = false;
                    }
                    break;
                case R.id.no_3:
                    if (no_3.getCurrentTextColor() == getResources().getColor(R.color.deep_grey)) {
                        no_3.setTextColor(getResources().getColor(R.color.app_default));
                        selected_team[2] = true;
                    } else {
                        no_3.setTextColor(getResources().getColor(R.color.deep_grey));
                        selected_team[2] = false;
                    }
                    break;
                case R.id.no_4:
                    if (no_4.getCurrentTextColor() == getResources().getColor(R.color.deep_grey)) {
                        no_4.setTextColor(getResources().getColor(R.color.app_default));
                        selected_team[3] = true;
                    } else {
                        no_4.setTextColor(getResources().getColor(R.color.deep_grey));
                        selected_team[3] = false;
                    }
                    break;
                case R.id.tv_choice_location:
                    intent = new Intent(Page_Main_Matching_Filter.this,Util_Location_Dialog.class);
                    startActivityForResult(intent, 0);
                    break;
                case R.id.btn_filter_next:
                    struct_filter.setStart_age(Integer.parseInt(ageMin.getText().toString()));
                    struct_filter.setEnd_age(Integer.parseInt(ageMax.getText().toString()));
                    struct_filter.setTeam_count(selected_team);
                    struct_filter.setDate(adapter.getCurrentItem_());
                    //Log.e("asfsag",struct_filter.getDate()+" "+struct_filter.getTeam_count()+struct_filter.getLoc());
                    intent = new Intent();
                    intent.putExtra("struct_filter", struct_filter);
                    setResult(0,intent);
                    finish();

                    break;
                case R.id.filter_back_btn:
                    intent = new Intent();
                    setResult(1, intent);
                    finish();
                    break;


            }
        }
    };
}