package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.zip.Inflater;

public class _BaseActivity extends AppCompatActivity {
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    private NetworkInfo mobile;
    private  NetworkInfo wifi;
    private  ConnectivityManager manager;
    private RelativeLayout layout;
    private LayoutInflater inflater;
    private ImageView img;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("ondestory","dd");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("onstop","dd");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("onstart","dd");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.e("onpostresum","dd");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onresum","dd");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_base_activity);

        layout = (RelativeLayout)findViewById(R.id.a_base_activity);
        inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        img = (ImageView)findViewById(R.id.base_img);
    }

    public boolean is_connet_internet(){
        manager = (ConnectivityManager) _BaseActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifi.isConnected() || mobile.isConnected()) {
            return true;
        }else{
            return false;
        }
    }

    public RelativeLayout setUI_not_connect(){
           return (RelativeLayout)inflater.inflate(R.layout.a_base_activity, null);
        //return  layout;
    }
}
