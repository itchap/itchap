package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.annotation.IntRange;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.view.View.GONE;
import static android.view.View.TEXT_ALIGNMENT_CENTER;
import static android.view.View.VISIBLE;

public class A_Status_Receive extends Fragment {

    private Util_Dialog dialog;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private LinearLayout no_item;
    private TextView no_item_title, no_item_sub_title;
    private RecyclerView recyclerview;

    private ArrayList<A_Status_ltem> item;
    private String type;
    private A_Status_Adapter adapter;
    protected View view;
    private String img[];
    String json, json2;
    JSONArray jsonArray_personal = null;
    JSONArray jsonArray_date = null;
    private TextView btn_choice_location;
    private RecyclerView recyclerview_date;

    private Util_Date_Adapter adapter_date;
    private ArrayList<Util_Date_Item> item_date;
    private RequestManager requestManager;
    private static final String TAG_RESULTS = "result";

    private Calendar mCalender = Calendar.getInstance();
    private int[] day = new int[7];
    private int width;
    private LinearLayout more;
    private int post_index = 0;

    public static A_Status_Receive newInstance(int page , String type){
        A_Status_Receive re = new A_Status_Receive();
        Bundle arg = new Bundle();
        arg.putInt("page", page);
        arg.putString("type", type);
        re.setArguments(arg);
        return re;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getString("type");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.l_status_receive, container, false);


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        width = displayMetrics.widthPixels;// 가로

        no_item = (LinearLayout) view.findViewById(R.id.no_list);
        no_item_title = (TextView)view.findViewById(R.id.title);
        no_item_sub_title= (TextView)view.findViewById(R.id.sub_title);
        recyclerview = (RecyclerView) view.findViewById(R.id.status_receive_recycler);
        recyclerview_date = (RecyclerView) view.findViewById(R.id.status_receive_recycler_us);


        get_day_of_month();
        item_date = new ArrayList<>();

        recyclerview.setOverScrollMode(View.OVER_SCROLL_NEVER);
        recyclerview.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.addItemDecoration(new A_Status_Receive.GridSpacingItemDecoration(3, dpToPx(10), true));
        recyclerview.setItemAnimator(new DefaultItemAnimator());

        item = new ArrayList<>();

        requestManager = Glide.with(getContext());
        adapter = new A_Status_Adapter(item, getContext(), requestManager);
        pref = getContext().getSharedPreferences("login", 0);
        editor = pref.edit();
        more = (LinearLayout) view.findViewById(R.id.more);

        Log.e("시작", "" + post_index);

        try {
            json = new Util_PHP(getContext()).execute(type, pref.getString("nick", ""), Integer.toString(post_index), "status_received", "http://175.126.38.139/php/like_received.php").get();
            json2 = new Util_PHP(getContext()).execute(type, pref.getString("nick", ""), "status_us", "http://175.126.38.139/php/like_us.php").get();
            if (json.contains("no_list") && json2.contains("no_list")) {
            } else if (json.contains("no_list") && !json2.contains("no_list")) {
                rendering_date(json2);
                recyclerview_date.setVisibility(VISIBLE);
                no_item_title.setText("받은 미팅신청이 없습니다.");
                no_item_sub_title.setText("이성이 미팅을 신청하게 되면 목록에 보여집니다. \n         한번 다른 날도 미팅을 등록해보세요~!");
                no_item_sub_title.setGravity(TEXT_ALIGNMENT_CENTER);
            } else {
                no_item.setVisibility(GONE);
                recyclerview_date.setVisibility(VISIBLE);
                recyclerview.setVisibility(VISIBLE);
                rendering_date(json2);
                rendering(json);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        recyclerview_date.setOverScrollMode(View.OVER_SCROLL_NEVER);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_date.setLayoutManager(horizontalLayoutManagaer);
        recyclerview_date.addItemDecoration(new A_Status_Receive.GridSpacingItemDecoration(7, (int)((width-740.7)/11), true));

        recyclerview_date.setOverScrollMode(View.OVER_SCROLL_NEVER);
        adapter_date = new Util_Date_Adapter(item_date, getContext());
        recyclerview_date.setAdapter(adapter_date);

        recyclerview.setAdapter(adapter);

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.e("모어", "" + post_index);
                    json = new Util_PHP(getContext()).execute(type, pref.getString("nick", ""), Integer.toString(post_index), "status_received", "http://175.126.38.139/php/like_received.php").get();

                    if(json.contains("no_list")){
                        Log.e("끝", "");
                    }else {
                        rendering(json);
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        });
        return view;
    }

    protected void rendering_date(String json2) {

        for (int i = 0; i < day.length; i++) {
            item_date.add(new Util_Date_Item(type, day[i], "", "", 2));
            // 요일, 날짜
        }
        try {
            JSONObject jsonObj3 = new JSONObject(json2);
            jsonArray_date = jsonObj3.getJSONArray(TAG_RESULTS);
            for (int i = 0; i < jsonArray_date.length(); i++) {
                JSONObject c = jsonArray_date.getJSONObject(i);
                for (int j = 0; j < 7; j++) {
                    if (c.getString("date").equals(item_date.get(j).getDay())) {
                        item_date.get(j).setFull_date(c.getString("date_time"));
                        item_date.get(j).setStatus(c.getString("status"));
                    } else {

                    }
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    protected void rendering(String json_personal) {
        String time, time2, time3, date, date2 = null;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String today = formatter.format(calendar.getTime());

        try {
            JSONObject jsonObj2 = new JSONObject(json_personal);
            jsonArray_personal = jsonObj2.getJSONArray(TAG_RESULTS);
            post_index = post_index + jsonArray_personal.length();
            if (jsonArray_personal.length() == 9) {
                more.setVisibility(VISIBLE);
            } else {
                more.setVisibility(GONE);
            }
            for (int i = 0; i < jsonArray_personal.length(); i++) {

                JSONObject c = jsonArray_personal.getJSONObject(i);

                img = new String[Integer.parseInt(c.getString("img_cnt"))];

                switch (Integer.parseInt(c.getString("img_cnt"))) {
                    case 1:
                        img[0] = c.getString("img1");
                        break;
                    case 2:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        break;
                    case 3:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        img[2] = c.getString("img3");
                        break;
                    case 4:
                        img[0] = c.getString("img1");
                        img[1] = c.getString("img2");
                        img[2] = c.getString("img3");
                        img[3] = c.getString("img4");
                        break;
                }
                item.add(new A_Status_ltem(getContext(), img, c.getString("title"), c.getString("date"), c.getString("owner"), Integer.parseInt(c.getString("img_cnt")), "status_complete"));
                Log.e("cnt", c.getString("img_cnt"));
            }
            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public class CustomLayoutManager extends LinearLayoutManager {
        private int mParentWidth;
        private int mItemWidth;

        public CustomLayoutManager(Context context, int parentWidth, int itemWidth) {
            super(context);
            setOrientation(LinearLayoutManager.HORIZONTAL);
            mItemWidth = itemWidth;
            mParentWidth = parentWidth;

        }

        @Override
        public int getPaddingLeft() {
            return Math.round(mParentWidth / 2f - mItemWidth / 2f);
        }

        @Override
        public int getPaddingRight() {
            return getPaddingLeft();
        }
    }

    //날짜 구하기
    public void get_day_of_month() {
        mCalender.get(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < 7; i++) {
            if (i == 0) {
                int j = mCalender.get(Calendar.DAY_OF_MONTH);
                day[i] = j;
            } else {
                mCalender.add(Calendar.DAY_OF_MONTH, 1);
                int j = mCalender.get(Calendar.DAY_OF_MONTH);
                day[i] = j;
            }
        }

    }

}