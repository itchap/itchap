package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.net.Uri;

/**
 * Created by u on 2017-01-23.
 */
public class Main_Event_Pager_item {

    String type, img, link;
    Context context;


    public Main_Event_Pager_item(Context context, String type, String img, String link) {
        this.context = context;
        this.type = type;
        this.img = img;
        this.link = link;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}