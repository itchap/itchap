package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.net.Uri;

/**
 * Created by young on 2017-01-22.
 */

public class Page_Credit_Item {

//
    String credit_no, credit_no_plus, price;
    Context context;
    int type;


    public Page_Credit_Item(Context context, String credit_no, String credit_no_plus, String price) {
        this.context = context;
        this.credit_no = credit_no;
        this.credit_no_plus = credit_no_plus;
        this.price = price;
        this.type = 1;
    }

    public String getCredit_no() {
        return credit_no;
    }

    public void setCredit_no(String credit_no) {
        this.credit_no = credit_no;
    }

    public String getCredit_no_plus() {
        return credit_no_plus;
    }

    public void setCredit_no_plus(String credit_no_plus) {
        this.credit_no_plus = credit_no_plus;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

}

