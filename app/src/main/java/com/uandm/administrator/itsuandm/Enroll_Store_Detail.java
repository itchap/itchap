package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.bumptech.glide.Glide;
import com.nhn.android.maps.*;
import com.nhn.android.maps.maplib.NGeoPoint;
import com.nhn.android.maps.overlay.NMapPOIdata;
import com.nhn.android.maps.overlay.NMapPOIitem;
import com.nhn.android.mapviewer.overlay.NMapCalloutOverlay;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager;
import com.nhn.android.mapviewer.overlay.NMapPOIdataOverlay;
import com.nhn.android.mapviewer.overlay.NMapResourceProvider;
import com.tsengvn.typekit.TypekitContextWrapper;
import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.nhn.android.maps.NMapOverlayItem;
import com.nhn.android.maps.overlay.NMapPOIitem;
import com.nhn.android.mapviewer.overlay.NMapResourceProvider;

import java.io.*;

import android.os.Bundle;

import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.ContentValues.TAG;

public class Enroll_Store_Detail extends Activity implements ViewPager.OnPageChangeListener, MapView.MapViewEventListener, MapView.POIItemEventListener, MapView.OpenAPIKeyAuthenticationResultListener {


    private ViewPager intro_images;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private Pager_Enroll_Store_Adapter mAdapter;
    private TextView btn_select;
    private RelativeLayout btn_back;
    private String type = null;
    private Double xx, yy;
    private String marker_title;
    private MapPoint mapPoint;
    ProgressDialog loading;
    String myJSON = null;
    JSONArray store = null;
     private int scroll_pro_sol=0;
    private Struct_Enroll struct_enroll;
    private ScrollView scroll;
    private Intent send;
    private String store_name;
    private String post_date;
    private static final String TAG_RESULTS = "result";
    private TextView name, intro, address, tel, time_1, time_2, time_3, time_3_title, discount, discount_title;
    private RelativeLayout rel_main_1;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_enroll_store_detail);

        btn_select = (TextView) findViewById(R.id.btn_select_location);
        btn_back = (RelativeLayout) findViewById(R.id.detail_back_btn);

        name = (TextView) findViewById(R.id.name);
        intro = (TextView) findViewById(R.id.title);
        address = (TextView) findViewById(R.id.ad);
        tel = (TextView) findViewById(R.id.tel);
        time_1 = (TextView) findViewById(R.id.time_1);
        time_2 = (TextView) findViewById(R.id.time_2);
        time_3 = (TextView) findViewById(R.id.time_3);
        time_3_title = (TextView) findViewById(R.id.time_title3);
        discount = (TextView) findViewById(R.id.discount);
        discount_title = (TextView) findViewById(R.id.store_discount_title);
        intro_images = (ViewPager) findViewById(R.id.pager_store);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        rel_main_1 = (RelativeLayout) findViewById(R.id.main1);
        scroll = (ScrollView) findViewById(R.id.scroll);

        type = getIntent().getStringExtra("type");
        Log.e("store_type", "" + type);
        //맵뷰
        final MapView mapView = new MapView(this);
        mapView.setDaumMapApiKey("b075dbee6aae6bb3a5797b603a2e8fd1");
        mapView.setMapViewEventListener(this);
        final ViewGroup mapViewContainer = (ViewGroup) findViewById(R.id.map_view);
        mapViewContainer.addView(mapView);


        if (type == null) {
            struct_enroll = (Struct_Enroll) getIntent().getSerializableExtra("Struct_Enroll");
            if (getIntent().getStringExtra("id") == null) {
                Log.e("Error enroll detail", "store id null");
            } else {
                try {
                    post_date = Integer.toString(struct_enroll.getMonth()) + "-" + Integer.toString(struct_enroll.getDay());
                    myJSON = new Util_PHP(Enroll_Store_Detail.this).execute(getIntent().getStringExtra("id"), post_date, "0", "Store_detail"
                            , "http://175.126.38.139/php/getdata_store_detail.php").get();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            showList(myJSON);
        } else if (type.equals("1")) {
            if (getIntent().getStringExtra("id") == null) {
                Log.e("Error enroll detail", "store id null");
            } else {
                try {
                    post_date = getIntent().getStringExtra("date_time");
                    myJSON = new Util_PHP(Enroll_Store_Detail.this).execute(getIntent().getStringExtra("id"), post_date, "1", "Store_detail"
                            , "http://175.126.38.139/php/getdata_store_detail.php").get();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            btn_select.setVisibility(View.INVISIBLE);
            showList(myJSON);
        } else if (type.equals("2")) {
            if (getIntent().getStringExtra("id") == null) {
                Log.e("Error enroll detail", "store id null");
            } else {
                try {
                    post_date = getIntent().getStringExtra("date");
                    myJSON = new Util_PHP(Enroll_Store_Detail.this).execute(getIntent().getStringExtra("id"), post_date, "2", "Store_detail"
                            , "http://175.126.38.139/php/getdata_store_detail.php").get();
                    Log.e("store_detail", "" + post_date + getIntent().getStringExtra("id"));
                    Log.e("store_detail_json", "" + myJSON);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            btn_select.setVisibility(View.INVISIBLE);
            showList(myJSON);
        }


        btn_select.setOnClickListener(listener);
        btn_back.setOnClickListener(listener);
    }


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.detail_back_btn:
                    if (type == null) {
                        intent = new Intent(Enroll_Store_Detail.this, Page_Main_Matching_Enroll_Store.class);
                        intent.putExtra("from","credit");
                        intent.putExtra("Struct_Enroll", struct_enroll);
                        startActivity(intent);
                        finish();
                    } else
                        finish();
                    break;

                case R.id.btn_select_location:

                    send = new Intent("DETAIL");
                    send.putExtra("result", "success");
                    send.putExtra("all_struct", struct_enroll);
                    sendBroadcast(send);

                    finish();
                    break;
            }
        }
    };


    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getApplicationContext());
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_round));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_round));
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_round));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_round));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    void showList(String myJSON) {
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            store = jsonObj.getJSONArray(TAG_RESULTS);

            if (store.length() == 1) {
                JSONObject c = store.getJSONObject(0);
                name.setText(c.getString("store_name"));
                intro.setText(c.getString("title"));
                address.setText(c.getString("address"));
                tel.setText(c.getString("tel"));
                time_1.setText(c.getString("time_daily"));
                time_2.setText(c.getString("time_weekend"));
                if (c.getString("holiday").equals("0")) {
                    time_3.setVisibility(View.GONE);
                    time_3_title.setVisibility(View.GONE);
                } else {
                    time_3.setText(c.getString("holiday"));
                }
                discount.setText(c.getString("discount"));
                String uri = c.getString("photo_main");
                String uri2 = c.getString("photo_2");
                String uri3 = c.getString("photo_3");
                Uri str_to_uri = Uri.parse(uri);
                Uri str_to_uri2 = Uri.parse(uri2);
                Uri str_to_uri3 = Uri.parse(uri3);
                Uri[] mImageResources = {str_to_uri, str_to_uri2, str_to_uri3};
                mAdapter = new Pager_Enroll_Store_Adapter(getApplicationContext(), mImageResources);
                intro_images.setAdapter(mAdapter);
                intro_images.setCurrentItem(0);
                intro_images.setOnPageChangeListener(this);
                setUiPageViewController();
                xx = c.getDouble("ad_loc_1");
                yy = c.getDouble("ad_loc_2");
                marker_title = c.getString("store_name");
                Log.e("xy", "" + xx + "//" + yy + "//" + marker_title);

            } else {
                Log.e("1개 이상", "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMapViewInitialized(MapView mapView) {
        mapPoint = MapPoint.mapPointWithGeoCoord(xx, yy);
        mapView.setMapCenterPoint(mapPoint, false);
        MapPOIItem marker = new MapPOIItem();
        marker.setItemName("Default Marker");
        marker.setTag(0);
        marker.setMapPoint(mapPoint);
        marker.setMarkerType(MapPOIItem.MarkerType.BluePin); // 기본으로 제공하는 BluePin 마커 모양.
        marker.setSelectedMarkerType(MapPOIItem.MarkerType.BluePin); // 마커를 클릭했을때, 기본으로 제공하는 RedPin 마커 모양.
        mapView.addPOIItem(marker);
    }


    @Override
    public void setFinishOnTouchOutside(boolean finish) {

        super.setFinishOnTouchOutside(finish);
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    public void onDaumMapOpenAPIKeyAuthenticationResult(MapView mapView, int i, String s) {
    }

    @Override
    public void onDraggablePOIItemMoved(MapView mapView, MapPOIItem mapPOIItem, MapPoint mapPoint) {
    }

    @Override
    public void onPOIItemSelected(MapView mapView, MapPOIItem mapPOIItem) {
    }

    @Override
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem) {
    }

    @Override
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem, MapPOIItem.CalloutBalloonButtonType calloutBalloonButtonType) {
    }


    @Override
    public void onMapViewCenterPointMoved(MapView mapView, MapPoint mapPoint) {
        Log.e("mapview_event", "onMapViewCenterPointMoved");
        mapView.setMapCenterPoint(this.mapPoint, true);
    }

    @Override
    public void onMapViewLongPressed(MapView mapView, MapPoint mapPoint) {
    }

    @Override
    public void onMapViewZoomLevelChanged(MapView mapView, int i) {
        if (mapView.getZoomLevel() >= 6) {
            mapView.setZoomLevel(6, false);
        }
        mapView.setMapCenterPoint(this.mapPoint, true);
    }

    @Override
    public void onMapViewMoveFinished(MapView mapView, MapPoint mapPoint) {
        if (this.mapPoint == null) {
        } else {
            mapView.setMapCenterPoint(this.mapPoint, true);
        }

    }

    @Override
    public void onMapViewDoubleTapped(MapView mapView, MapPoint mapPoint) {
        mapView.setMapCenterPoint(this.mapPoint, true);
    }

    @Override
    public void onMapViewSingleTapped(MapView mapView, MapPoint mapPoint) {
        mapView.setMapCenterPoint(this.mapPoint, true);
    }

    @Override
    public void onMapViewDragStarted(MapView mapView, MapPoint mapPoint) {
            mapView.setMapCenterPoint(this.mapPoint, true);
    }

    @Override
    public void onMapViewDragEnded(MapView mapView, MapPoint mapPoint) {
            mapView.setMapCenterPoint(this.mapPoint, true);

    }

}