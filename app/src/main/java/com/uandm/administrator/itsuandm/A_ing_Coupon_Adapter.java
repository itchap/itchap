package com.uandm.administrator.itsuandm;

/**
 * Created by u on 2017-02-16.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.bumptech.glide.RequestManager;
import com.nhn.android.maps.NMapActivity;
import com.nhn.android.maps.NMapContext;
import com.nhn.android.maps.NMapView;
import com.nhn.android.maps.maplib.NGeoPoint;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager;

import java.util.ArrayList;


public class A_ing_Coupon_Adapter extends RecyclerView.Adapter<A_ing_Coupon_Adapter.ViewHolder> {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList//
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private ArrayList<A_ing_Coupon_Item> listViewItemList;
    private Context context;
    public static final int ITEM_TYPE_ONE = 1;
    public static final int ITEM_TYPE_TWO = 2;
    public static final int ITEM_TYPE_THREE = 3;


    int type;

    public A_ing_Coupon_Adapter(ArrayList<A_ing_Coupon_Item> item, Context context) {
        this.context = context;
        this.listViewItemList = item;
    }

    @Override
    public int getItemViewType(int position) {

        if (listViewItemList.get(position).getType() == 1) {
            this.type = 1;
            return ITEM_TYPE_ONE;
        } else if(listViewItemList.get(position).getType() == 2) {
            this.type = 2;
            return ITEM_TYPE_TWO;
        }else{
            this.type = 3;
            return ITEM_TYPE_THREE;
        }
    }


    /**
     * 레이아웃을 만들어서 Holer에 저장
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public A_ing_Coupon_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        if (viewType == ITEM_TYPE_ONE || viewType == ITEM_TYPE_TWO ) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_ing_coupon_item, viewGroup, false);
            return new A_ing_Coupon_Adapter.ViewHolder(view, ITEM_TYPE_ONE);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_ing_coupon_item2, viewGroup, false);
            return new A_ing_Coupon_Adapter.ViewHolder(view, ITEM_TYPE_THREE);
        }
    }

    /**
     * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(final A_ing_Coupon_Adapter.ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);

        viewHolder.name.setText(listViewItemList.get(position).getName());
        viewHolder.date.setText(listViewItemList.get(position).getSp_date());
        viewHolder.discount_title.setText(listViewItemList.get(position).getDiscount_title());
        viewHolder.discount.setText(listViewItemList.get(position).getDiscount());
        viewHolder.discount_no.setText(listViewItemList.get(position).getDiscount_no());
        viewHolder.discount_sub.setText(listViewItemList.get(position).getDiscount_sub());

    }

    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }

    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView name, date, discount_title, discount, discount_no, discount_sub;


        public ViewHolder(View itemView, int type) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            date = (TextView) itemView.findViewById(R.id.date);
            discount_title = (TextView) itemView.findViewById(R.id.discount_title);
            discount = (TextView) itemView.findViewById(R.id.discount);
            discount_no = (TextView) itemView.findViewById(R.id.discount_no);
            discount_sub = (TextView) itemView.findViewById(R.id.discount_sub);


        }
    }

}
