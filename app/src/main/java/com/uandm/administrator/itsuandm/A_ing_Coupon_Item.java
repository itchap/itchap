package com.uandm.administrator.itsuandm;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by u on 2017-02-16.
 */
public class A_ing_Coupon_Item {
    private Context context;
    private String name;
    private String date;
    private String discount_title;
    private String discount;
    private String discount_sub;
    private String discount_no;
    private String sp_date;
    private int type;


    public A_ing_Coupon_Item(Context context,String name,String date,String discount_title,String discount,String discount_sub,String discount_no,int type,String sp_date) {
        this.context = context;
        this.name = name;
        this.date = date;
        this.sp_date = sp_date;
        this.discount_title = discount_title;
        this.discount = discount;
        this.discount_sub = discount_sub;
        this.discount_no = discount_no;
        this.type = type;

    }

    public String getSp_date() {
        return sp_date;
    }

    public void setSp_date(String sp_date) {
        this.sp_date = sp_date;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDiscount_title() {
        return discount_title;
    }

    public void setDiscount_title(String discount_title) {
        this.discount_title = discount_title;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_sub() {
        return discount_sub;
    }

    public void setDiscount_sub(String discount_sub) {
        this.discount_sub = discount_sub;
    }

    public String getDiscount_no() {
        return discount_no;
    }

    public void setDiscount_no(String discount_no) {
        this.discount_no = discount_no;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    }

