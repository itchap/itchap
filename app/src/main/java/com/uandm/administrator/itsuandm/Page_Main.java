package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.*;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.ImageReader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.IntDef;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.tsengvn.typekit.TypekitContextWrapper;

import me.leolin.shortcutbadger.ShortcutBadger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeoutException;


import android.os.Bundle;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;


public class Page_Main extends AppCompatActivity {


    private ActionBar actionbar;
    private Util_Viewpager viewpager;
    private RelativeLayout m1, m2, m3, m4;
    private RelativeLayout home_tab, matching_tab;
    private boolean mIsEnabled;

    private ImageView mAlram_icon, mAlram_icon2, mNavi_icon2, drawer_alram, drawer_alram_, mIng_icon2;
    private RelativeLayout mNavi_icon, mStauts_icon, mIng_icon;

    private LinearLayout drawer_team, drawer_team_ing, drawer_profile, drawer_my_team;
    ;

    private RelativeLayout drawerView;
    private DrawerLayout mDrawerLayout;
    Boolean drawer_open = false;
    private Util_ImageView drawer_image;

    private TextView drawer_error, drawer_withdraw, drawer_logout, drawer_nick, drawer_credit;
    private ImageView drawer_charge_coin;
    private Dialog dialog;
    private Util_Dialog custom_dial;
    private TextView drawer_error_suggestion, drawer_error_report, drawer_error_submit, drawer_error_edit_content, drawer_error_cancel, drawer_withdraw_submit, drawer_withdraw_cancel;
    private LinearLayout error_content, drawer_withdraw_content;
    private EditText drawer_withdraw_edit;
    private String error_type = null;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private String ssesion;
    private String id = null;
    private String nick = null;
    private String json = null;
    private String json2 = null;
    private String credit;
    private String main_img_uri = null;
    private String team_status = null;
    private JSONArray jsonArray;
    private Uri str_to_uri;
    boolean set_error = false;

    private InputMethodManager imm;
    private BroadcastReceiver receiver_drawer, receiver_session, receiver_chatting;
    private String feedback;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver_drawer);
        unregisterReceiver(receiver_session);
        unregisterReceiver(receiver_chatting);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e("onstart", "dd");
        try {
            json = new getData().execute(id).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        parse(json);
        editor.putString("nick", nick);
        editor.commit();

        if (main_img_uri != null) {
            str_to_uri = Uri.parse(main_img_uri);
            Glide.with(this).load(str_to_uri).asBitmap().into(drawer_image);
        }

        drawer_nick.setText(nick);
        drawer_credit.setText(credit);

        FirebaseInstanceId.getInstance().getToken();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if(intent.getStringExtra("from").equals("alram")){
            Intent intent1  = new Intent(Page_Main.this,A_ing.class);
            startActivity(intent1);
        }else if(intent.getStringExtra("from").equals("alram_status")){
            Intent intent1  = new Intent(Page_Main.this,A_Status.class);
            intent1.putExtra("type",intent.getStringExtra("type"));
            Log.e("type2", "" + intent1.getStringExtra("type"));
            startActivity(intent1);

        }

        setContentView(R.layout.l_main_viewpager);

        Log.e("chat_test_batcn_main", "" + FirebaseMessagingService.bagde_count);
    /* 프리퍼런스  */
        pref = getSharedPreferences("login", Activity.MODE_PRIVATE);
        editor = pref.edit();
        id = pref.getString("id", "null");

        if (pref.getBoolean("appFrist", false)) {
            Intent usage = new Intent(Page_Main.this, Page_Usage.class);
            startActivity(usage);
        }

        drawerView = (RelativeLayout) findViewById(R.id.drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);

         /* 액션바 설정   */
        actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(false);            //액션바 아이콘을 업 네비게이션 형태로 표시합니다.
        actionbar.setDisplayShowTitleEnabled(false);        //액션바에 표시되는 제목의 표시유무를 설정합니다.
        actionbar.setDisplayShowHomeEnabled(false);            //홈 아이콘을 숨김처리합니다.
        View customView = LayoutInflater.from(this).inflate(R.layout.l_main_actionbar, null);
        actionbar.setCustomView(customView);

        // 기본 padding 0
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0, 0);

        actionbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#95DEE3")));

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT);


        Findid();


        /* 뷰 페이저 */
        viewpager = (Util_Viewpager) findViewById(R.id.view_pager);
        final Page_Main_Viewpager_Adapter viewpagerAdapter = new Page_Main_Viewpager_Adapter(getSupportFragmentManager());

        viewpager.setAdapter(viewpagerAdapter);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                } else if (position == 1) {
                    viewpagerAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        viewpager.setPagingDisabled();


        IntentFilter filter_drawer = new IntentFilter();
        filter_drawer.addAction("make_team");
        IntentFilter filter_session = new IntentFilter();
        filter_session.addAction("session");
        IntentFilter filter_chatting = new IntentFilter();
        filter_chatting.addAction("CHATTING");
        ;


        receiver_session = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String str = intent.getStringExtra("result");
                // 오너 전지현 // 센더 공유
                if (str.equals("logout")) {
                    Intent to_login = new Intent(Page_Main.this, Page_Login.class);
                    to_login.putExtra("logout", true);
                    ShortcutBadger.removeCount(Page_Main.this);
                    FirebaseMessagingService.bagde_count_chat=0;
                    FirebaseMessagingService.bagde_count_chat=0;
                    FirebaseMessagingService.bagde_count_total=0;
                    startActivity(to_login);
                    finish();
                }
            }
        };

        receiver_chatting = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (FirebaseMessagingService.bagde_count_chat >= 1) {
                    mIng_icon2.setVisibility(View.VISIBLE);
                } else {
                    mIng_icon2.setVisibility(View.GONE);
                }
            }
        };

        registerReceiver(receiver_chatting, filter_chatting);
        receiver_drawer = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String str = intent.getStringExtra("result");
                String msg = intent.getStringExtra("msg");
                Log.e("msg", msg);

                // 오너 전지현 // 센더 공유
                if (str.equals("success")) {
                    if (msg.contains("모두 수락")) {
                        drawer_my_team.setVisibility(View.VISIBLE);
                        drawer_team.setVisibility(View.INVISIBLE);
                        drawer_team_ing.setVisibility(View.INVISIBLE);
                    } else if (msg.contains("거절")) {
                        drawer_my_team.setVisibility(View.INVISIBLE);
                        drawer_team.setVisibility(View.VISIBLE);
                        drawer_team_ing.setVisibility(View.INVISIBLE);
                    } else if (msg.contains("요청")) {
                        drawer_my_team.setVisibility(View.INVISIBLE);
                        drawer_team.setVisibility(View.INVISIBLE);
                        drawer_team_ing.setVisibility(View.VISIBLE);
                    } else if (msg.contains("취소")) {
                        Log.e("here", "5");
                        drawer_my_team.setVisibility(View.INVISIBLE);
                        drawer_team.setVisibility(View.VISIBLE);
                        drawer_team_ing.setVisibility(View.INVISIBLE);
                    } else if (msg.contains("수락")) {
                        drawer_my_team.setVisibility(View.INVISIBLE);
                        drawer_team.setVisibility(View.INVISIBLE);
                        drawer_team_ing.setVisibility(View.VISIBLE);
                    } else if (msg.contains("나가")) {
                        drawer_my_team.setVisibility(View.INVISIBLE);
                        drawer_team.setVisibility(View.VISIBLE);
                        drawer_team_ing.setVisibility(View.INVISIBLE);
                    }
                    if (FirebaseMessagingService.bagde_count >= 1) {
                        mNavi_icon2.setVisibility(View.VISIBLE);
                        mAlram_icon2.setVisibility(View.VISIBLE);
                    } else {
                        mNavi_icon2.setVisibility(View.GONE);
                        mAlram_icon2.setVisibility(View.GONE);
                    }

                }
            }
        };

        registerReceiver(receiver_drawer, filter_drawer);
        registerReceiver(receiver_session, filter_session);
//
    }

    void Findid() {
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        drawer_alram = (ImageView) findViewById(R.id.al_icon);
        drawer_alram.setOnClickListener(btnListener);
        // drawer_alram_ = (ImageView) findViewById(R.id.al_icon2);
        drawer_credit = (TextView) findViewById(R.id.coin_count);


        drawer_image = (Util_ImageView) findViewById(R.id.proile);
        drawer_image.setType(11);
        drawer_nick = (TextView) findViewById(R.id.nickname);
        drawer_profile = (LinearLayout) findViewById(R.id.drawer_profile);
        drawer_profile.setOnClickListener(btnListener);
        drawer_team = (LinearLayout) findViewById(R.id.drawer_team);
        drawer_team.setOnClickListener(btnListener);
        drawer_team_ing = (LinearLayout) findViewById(R.id.drawer_team_ing);
        drawer_team_ing.setOnClickListener(btnListener);
        drawer_my_team = (LinearLayout) findViewById(R.id.drawer_my_team);
        drawer_my_team.setOnClickListener(btnListener);

        drawer_error = (TextView) findViewById(R.id.drawer_error);
        drawer_error.setOnClickListener(btnListener);
        drawer_withdraw = (TextView) findViewById(R.id.drawer_withdraw);
        drawer_withdraw.setOnClickListener(btnListener);
        drawer_logout = (TextView) findViewById(R.id.drawer_logout);
        drawer_logout.setOnClickListener(btnListener);

        drawer_charge_coin = (ImageView) findViewById(R.id.btn_charge_coin);
        drawer_charge_coin.setOnClickListener(btnListener);

        drawer_error_edit_content = (TextView) findViewById(R.id.error_edit_content);

        home_tab = (RelativeLayout) findViewById(R.id.home_tab_layout);
        home_tab.setOnClickListener(btnListener);
        matching_tab = (RelativeLayout) findViewById(R.id.matching_tab_layout);
        matching_tab.setOnClickListener(btnListener);

        mAlram_icon = (ImageView) findViewById(R.id.al_icon);
        mAlram_icon2 = (ImageView) findViewById(R.id.al_icon2);
        mAlram_icon.setOnClickListener(btnListener);

        mNavi_icon = (RelativeLayout) findViewById(R.id.navi_layout);
        mNavi_icon2 = (ImageView) findViewById(R.id.navi_icon2);

        if (FirebaseMessagingService.bagde_count >= 1) {
            mNavi_icon2.setVisibility(View.VISIBLE);
            mAlram_icon2.setVisibility(View.VISIBLE);
        } else {
            mNavi_icon2.setVisibility(View.GONE);
            mAlram_icon2.setVisibility(View.GONE);
        }

        mNavi_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(drawerView);
                    drawer_open = false;
                } else {
                    mDrawerLayout.openDrawer(drawerView);
                    drawer_open = true;
                }

            }
        });

        mStauts_icon = (RelativeLayout) findViewById(R.id.list_layout);
        mStauts_icon.setOnClickListener(btnListener);

        mIng_icon = (RelativeLayout) findViewById(R.id.ing_layout);
        mIng_icon2 = (ImageView) findViewById(R.id.ing_2);
        mIng_icon.setOnClickListener(btnListener);
        if (FirebaseMessagingService.bagde_count_chat >= 1) {
            mIng_icon2.setVisibility(View.VISIBLE);
        } else {
            mIng_icon2.setVisibility(View.GONE);
        }

    }

    private void setCurrentInflateItem(int type) {
        if (type == 0) {
            viewpager.setCurrentItem(0);
        } else if (type == 1) {
            viewpager.setCurrentItem(1);

        }
    }

    private void parse(String data) {
        try {
            JSONObject jsonObj = new JSONObject(data);
            jsonArray = jsonObj.getJSONArray("result");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject c = jsonArray.getJSONObject(i);
                nick = c.getString("nick");
                main_img_uri = c.getString("photo_main");
                team_status = c.getString("team_status");
                credit = c.getString("credit");
            }

            if (team_status.equals("0")) {
                drawer_my_team.setVisibility(View.INVISIBLE);
                drawer_team_ing.setVisibility(View.INVISIBLE);
                drawer_team.setVisibility(View.VISIBLE);
            } else if (team_status.equals("2") || team_status.equals("3") || team_status.equals("4")) {
                drawer_my_team.setVisibility(View.VISIBLE);
                drawer_team_ing.setVisibility(View.INVISIBLE);
                drawer_team.setVisibility(View.INVISIBLE);
            } else {
                drawer_my_team.setVisibility(View.INVISIBLE);
                drawer_team_ing.setVisibility(View.VISIBLE);
                drawer_team.setVisibility(View.INVISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {
            ImageView[] icon = new ImageView[2];
            ImageView[] under = new ImageView[2];
            Intent intent;

            switch (v.getId()) {
                case R.id.ing_layout:
                    Intent intent2 = new Intent(Page_Main.this, A_ing.class);
                    startActivity(intent2);
                    break;
                case R.id.al_icon:
                    mNavi_icon2.setVisibility(View.GONE);
                    mAlram_icon2.setVisibility(View.GONE);
                    intent = new Intent(Page_Main.this, Alram.class);
                    startActivity(intent);
                    ShortcutBadger.removeCount(Page_Main.this);
                    FirebaseMessagingService.bagde_count_total -= FirebaseMessagingService.bagde_count;
                    FirebaseMessagingService.bagde_count = 0;
                    ShortcutBadger.applyCount(Page_Main.this, FirebaseMessagingService.bagde_count_total);
                    break;
                case R.id.drawer_profile:
                    intent = new Intent(Page_Main.this, Modify_Profile.class);
                    startActivity(intent);
                    break;
                case R.id.drawer_team:
                    intent = new Intent(Page_Main.this, Drawer_Organize_Team.class);
                    startActivityForResult(intent, 0);
                    break;
                case R.id.drawer_team_ing:
                    try {
                        json2 = new Util_PHP(Page_Main.this).execute(pref.getString("nick", ""), "org_team_ing", "http://175.126.38.139/php/fcm/org_team_ing.php").get();
                        if (json2.equals("wait")) {
                            custom_dial = new Util_Dialog(Page_Main.this, getResources().getDrawable(R.drawable.icon_exit), "팀 구성 진행중입니다.", "확인", "팀요청 취소", "team_ing_cancel");
                            custom_dial.show();
                        } else if (json2.equals("error")) {
                            custom_dial = new Util_Dialog(Page_Main.this, getResources().getDrawable(R.drawable.icon_exit), "error", "확인", "dismiss");
                            custom_dial.show();
                        } else if (json2.equals("already")) {
                            dialog = new Util_Dialog(Page_Main.this, getResources().getDrawable(R.drawable.icon_lastpage), "이미 수락/거절을 하셨습니다.", "확인", "dismiss");
                            dialog.show();
                        } else {
                            custom_dial = new Util_Dialog(Page_Main.this, getResources().getDrawable(R.drawable.icon_lastpage), json2 + "님의 팀 요청을\n 수락하시겠습니까?", "거절", "수락", "team_waiting");
                            custom_dial.put_data = json2;
                            custom_dial.show();
                        }
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                    break;
                case R.id.drawer_my_team:
                    Intent intent1 = new Intent(Page_Main.this, My_Team_list.class);
                    startActivity(intent1);
                    break;
                case R.id.drawer_error:

                    dialog = new Dialog(Page_Main.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.d_error);
                    dialog.setCancelable(false);

                    error_content = (LinearLayout) dialog.findViewById(R.id.error_content);
                    drawer_error_report = (TextView) dialog.findViewById(R.id.btn_error);
                    drawer_error_suggestion = (TextView) dialog.findViewById(R.id.btn_suggestion);
                    drawer_error_submit = (TextView) dialog.findViewById(R.id.btn_submit);
                    drawer_error_cancel = (TextView) dialog.findViewById(R.id.btn_cancel);
                    drawer_error_edit_content = (EditText) dialog.findViewById(R.id.error_edit_content);
                    drawer_error_suggestion.setTextColor(getResources().getColor(R.color.app_red));
                    set_error = true;
                    error_type = "1";
                    drawer_error_report.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawer_error_report.setTextColor(getResources().getColor(R.color.app_default));
                            drawer_error_suggestion.setTextColor(getResources().getColor(R.color.soft_grey));
                            set_error = true;
                            error_type = "1";
                        }
                    });
                    drawer_error_suggestion.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawer_error_suggestion.setTextColor(getResources().getColor(R.color.app_red));
                            drawer_error_report.setTextColor(getResources().getColor(R.color.soft_grey));
                            set_error = true;
                            error_type = "0";
                        }
                    });
                    drawer_error_submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            Toast.makeText(getApplicationContext(), drawer_error_edit_content.getText().toString(), Toast.LENGTH_SHORT).show();
                            if (!set_error) {
                                custom_dial = new Util_Dialog(Page_Main.this, getResources().getDrawable(R.drawable.icon_lastpage), "제안하기 또는 오류신고를 선택해주세요.", "확인", "error_submit");
                                custom_dial.show();
                            } else if (drawer_error_edit_content.getText().toString() == null || drawer_error_edit_content.getText().toString().equals("")) {
                                custom_dial = new Util_Dialog(Page_Main.this, getResources().getDrawable(R.drawable.icon_lastpage), "내용을 입력해주세요", "확인", "error_submit_no_content");
                                custom_dial.show();
                            } else if (set_error && !drawer_error_edit_content.getText().toString().equals("")) {
                                try {
                                    Log.e("error_type", "" + error_type);
                                    feedback = new Util_PHP(Page_Main.this).execute(pref.getString("nick", ""), error_type, drawer_error_edit_content.getText().toString(), "", "feedback", "http://175.126.38.139/php/feedback.php").get();
                                    dialog.dismiss();
                                    if (feedback.equals("success_coin")) {
                                        custom_dial = new Util_Dialog(Page_Main.this, getResources().getDrawable(R.drawable.icon_check), "소중한 의견 감사합니다.(코인지급완료!)", "확인", "error_submit");
                                        custom_dial.show();
                                    } else if (feedback.equals("success")) {
                                        custom_dial = new Util_Dialog(Page_Main.this, getResources().getDrawable(R.drawable.icon_check), "소중한 의견 감사합니다.", "확인", "error_submit");
                                        custom_dial.show();
                                    } else {
                                        dialog.dismiss();
                                    }
                                } catch (Exception e) {
                                    dialog.dismiss();
                                }
                            }

                        }
                    });

                    drawer_error_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    error_content.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imm.hideSoftInputFromWindow(drawer_error_edit_content.getWindowToken(), 0);

                        }
                    });
                    dialog.show();
                    break;
                //탈퇴하기
                case R.id.drawer_withdraw:
                    dialog = new Dialog(Page_Main.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.d_withdraw);
                    dialog.setCancelable(false);

                    drawer_withdraw_content = (LinearLayout) dialog.findViewById(R.id.withdraw_content);
                    drawer_withdraw_content.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imm.hideSoftInputFromWindow(drawer_withdraw_edit.getWindowToken(), 0);
                        }
                    });
                    drawer_withdraw_edit = (EditText) dialog.findViewById(R.id.withdraw_edit_content);
                    drawer_withdraw_submit = (TextView) dialog.findViewById(R.id.btn_submit);
                    drawer_withdraw_submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String json_;
                            if (drawer_withdraw_edit.getText().toString().equals("") || drawer_withdraw_edit.getText().toString() == null) {
                                custom_dial = new Util_Dialog(Page_Main.this, getResources().getDrawable(R.drawable.icon_lastpage), "내용을 입력해주세요", "확인", "error_submit_no_content");
                                custom_dial.show();
                            } else {
                                try {

                                    json = new Util_PHP(Page_Main.this).execute(nick, "delete_member", "http://175.126.38.139/php/delete_member.php").get();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                if (json.contains("success")) {
                                    custom_dial = new Util_Dialog(Page_Main.this, getResources().getDrawable(R.drawable.icon_check), "이용해 주셔서 감사합니다.", "확인", "withdraw");
                                    ShortcutBadger.removeCount(Page_Main.this);
                                    FirebaseMessagingService.bagde_count_chat=0;
                                    FirebaseMessagingService.bagde_count_chat=0;
                                    FirebaseMessagingService.bagde_count_total=0;
                                    custom_dial.show();
                                } else if (json.contains("fail")) {
                                    // 디비 에러
                                }
                            }
                        }
                    });
                    drawer_withdraw_cancel = (TextView) dialog.findViewById(R.id.btn_cancel);
                    drawer_withdraw_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                    break;
                //로그아웃
                case R.id.drawer_logout:
                    custom_dial = new Util_Dialog(Page_Main.this, getResources().getDrawable(R.drawable.icon_lastpage), "로그아웃을 하시겠습니까?", "취소", "확인", "logout");
                    custom_dial.show();
                    break;
                case R.id.home_tab_layout:
                    icon[0] = (ImageView) findViewById(R.id.btn_home);
                    icon[0].setBackgroundResource(R.drawable.icon_home_sel);
                    under[0] = (ImageView) findViewById(R.id.btn_home_underline);
                    under[0].setBackgroundColor(Color.parseColor("#95DEE3"));

                    icon[1] = (ImageView) findViewById(R.id.btn_matching);
                    icon[1].setBackgroundResource(R.drawable.icon_matching);
                    under[1] = (ImageView) findViewById(R.id.btn_matching_underline);
                    under[1].setBackgroundColor(Color.parseColor("#FFFFFF"));
                    setCurrentInflateItem(0);
                    break;

                case R.id.matching_tab_layout:
                    icon[0] = (ImageView) findViewById(R.id.btn_home);
                    icon[0].setBackgroundResource(R.drawable.icon_home);
                    under[0] = (ImageView) findViewById(R.id.btn_home_underline);
                    under[0].setBackgroundColor(Color.parseColor("#FFFFFF"));

                    icon[1] = (ImageView) findViewById(R.id.btn_matching);
                    icon[1].setBackgroundResource(R.drawable.icon_matching_sel);
                    under[1] = (ImageView) findViewById(R.id.btn_matching_underline);
                    under[1].setBackgroundColor(Color.parseColor("#95DEE3"));
                    setCurrentInflateItem(1);
                    break;
                case R.id.list_layout:
                    Intent intent4 = new Intent(Page_Main.this, A_Status_Type.class);
                    intent4.putExtra("type", "to_status");
                    startActivity(intent4);
                    break;
                case R.id.btn_charge_coin:
                    Intent intent3 = new Intent(Page_Main.this, Page_Credit.class);
                    startActivity(intent3);
                    break;

            }
        }
    };

    class getData extends AsyncTask<String, Void, String> {
        ProgressDialog loading;
        String link = "http://175.126.38.139/php/getdata_main.php";
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = ProgressDialog.show(Page_Main.this, "Please Wait", null, true, true);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loading.dismiss();
            //Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
        }

        private void writeFormField(String fieldName, String fieldValue) {
            try {
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"" + fieldName + "\"" + lineEnd);
                dos.writeBytes(lineEnd);
                dos.write(fieldValue.getBytes());
                dos.writeBytes(lineEnd);
            } catch (Exception e) {
                //System.out.println("AndroidUploader.writeFormField: got: " + e.getMessage());
                Log.e("ASDf", "AndroidUploader.writeFormField: " + e.getMessage());
            }
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                URL url = new URL(link);
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                dos = new DataOutputStream(conn.getOutputStream());

                writeFormField("id", params[0]);

                dos.flush();
                dos.close();
                dos = null;


                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String json;

                while ((json = bufferedReader.readLine()) != null) {
                    sb.append(json + "\n");
                }

                return sb.toString().trim();

            } catch (Exception e) {
                Log.e("dddddd", new String("Exception: " + e.getMessage()));
                return new String("Exception: " + e.getMessage());
            }

        }


    }


}
