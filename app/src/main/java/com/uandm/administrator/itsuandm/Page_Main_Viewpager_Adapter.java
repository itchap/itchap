package com.uandm.administrator.itsuandm;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

public class Page_Main_Viewpager_Adapter extends FragmentStatePagerAdapter {

    /*
	 * 이 클래스의 부모생성자 호출시 인수로 반드시 FragmentManager객체를 넘겨야한다.
	 * 이 객체는 Activity에서만 만들수 있고, 여기서사용중인 Fragment가 v4이기 때문에
	 * Activity중에서도 ActionBarActivity에서 얻어와야한다.
	 */
    private boolean enabled;

    Fragment[] fragments = new Fragment[2];


    public Page_Main_Viewpager_Adapter(FragmentManager fm) {
        super(fm);
        fragments[0] = new Page_Main_HomeView();
        fragments[1] = new Page_Main_Matching();
    }
/////
    //아래의 메서드들의 호출 주체는 ViewPager이다.
    //ListView와 원리가 같다.

    /*
     * 여러 프레그먼트 중 어떤 프레그먼트를 보여줄지 결정
     */
    public Fragment getItem(int arg0) {
        return fragments[arg0];
    }

    /*
     * 보여질 프레그먼트가 몇개인지 결정
     */
    public int getCount() {
        return fragments.length;
    }

    @Override
    public int getItemPosition(Object object) {
        if(object instanceof  Page_Main_Matching){
            return POSITION_NONE;
        }else if(object instanceof  Page_Main_HomeView){
            return POSITION_UNCHANGED;
        }
        return super.getItemPosition(object);

    }
}

