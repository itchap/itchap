package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.innodroid.expandablerecycler.ExpandableRecyclerAdapter;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.List;

import static com.uandm.administrator.itsuandm.R.attr.color;
import static com.uandm.administrator.itsuandm.R.attr.colorPrimary;
import static com.uandm.administrator.itsuandm.R.attr.font;
import static com.uandm.administrator.itsuandm.R.attr.popupMenuStyle;
import static com.uandm.administrator.itsuandm.R.attr.position;
import static java.security.AccessController.getContext;

/**
 * Created by young on 2017-03-02.
 */

public class Filter_Loc_Adapter extends ExpandableRecyclerAdapter<Filter_Loc_Adapter.PeopleListItem> {
    public static final int TYPE_PERSON = 1001;
    public static String loc[] = new String[3];
//    private  static final int position[] =new int[100];

    public Filter_Loc_Adapter(Context context, List<PeopleListItem> items) {
        super(context);
        loc[0] = null;
        loc[1] = null;
        loc[2] = null;
//        for(int i=0;i<100;i++){
//            position[i]=0;
//        }

        setItems(getSampleItems(items));
    }

    public static class PeopleListItem extends ExpandableRecyclerAdapter.ListItem {
        public String Text;
        public String Bool;

        public PeopleListItem(String group) {
            super(TYPE_HEADER);
            Text = group;
        }

        public PeopleListItem(String first, String last) {
            super(TYPE_PERSON);
            Text = last;
            Bool = first;
        }
    }

    public class HeaderViewHolder extends ExpandableRecyclerAdapter.HeaderViewHolder {
        protected TextView name;

        public HeaderViewHolder(View view) {
            super(view, (ImageView) view.findViewById(R.id.item_arrow));
            name = (TextView) view.findViewById(R.id.item_header_name);
        }

        public void bind( final int position) {
            Log.e("bind_header" ,"ss"+position);
            super.bind(position);
            name.setText(visibleItems.get(position).Text);
//            name.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                }
//            });
        }
    }

    public class PersonViewHolder extends ExpandableRecyclerAdapter.ViewHolder {

        protected  TextView name, name2;

        public PersonViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.item_name);
            name2 = (TextView) view.findViewById(R.id.item_name2);
        }

//        (final A_ing_Adapter.ViewHolder viewHolder, final int position) {
        public void bind(final int position) {
            Log.e("bind","ss"+position);
            name.setText(visibleItems.get(position).Text);
            name2.setText(visibleItems.get(position).Bool);

            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("클릭@","ss"+position);
                    if (name2.getText().toString().equals("0")) {
                        if (loc[0] == null) {
                            loc[0] =name.getText().toString();
                            name.setTextColor(Color.parseColor("#FFFFFF"));
                            name.setBackgroundColor(Color.parseColor("#95dee3"));
                            name2.setText("1");
                        } else if (loc[1] == null) {
                            loc[1] = name.getText().toString();
                            name.setTextColor(Color.parseColor("#FFFFFF"));
                            name.setBackgroundColor(Color.parseColor("#95dee3"));
                            name2.setText("1");
                        } else if (loc[2] == null) {
                            loc[2] = name.getText().toString();
                            name.setTextColor(Color.parseColor("#FFFFFF"));
                            name.setBackgroundColor(Color.parseColor("#95dee3"));
                            name2.setText("1");
                        } else {

                        }
                    } else {
                        name.setTextColor(Color.parseColor("#6B6B6B"));
                        name.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        name2.setText("0");
                        if (loc[2] != null) {
                            loc[2] = null;
                        } else if (loc[1] != null) {
                            loc[1] = null;
                        } else {
                            loc[0] = null;
                        }

                    }
                }
            });
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("onCreateViewHolder","ss"+viewType);
        switch (viewType) {
            case TYPE_HEADER:
                return new HeaderViewHolder(inflate(R.layout.filter_item_header, parent));
            case TYPE_PERSON:
                return new PersonViewHolder(inflate(R.layout.filter_item, parent));
        }return  new PersonViewHolder(inflate(R.layout.filter_item, parent));
    }

    @Override
    public void onBindViewHolder(ExpandableRecyclerAdapter.ViewHolder holder, final int position) {
        Log.e("onBindViewHolder","ss"+position);
        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                ((HeaderViewHolder) holder).bind(position);
                break;
            case TYPE_PERSON:
                ((PersonViewHolder) holder).bind(position);
                break;
        }
    }

    public static List<PeopleListItem> getSampleItems(List<PeopleListItem> items) {
        return items;
    }
}