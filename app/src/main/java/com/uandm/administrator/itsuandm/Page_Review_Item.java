package com.uandm.administrator.itsuandm;

import android.content.Context;

/**
 * Created by u on 2017-01-24.
 */
public class Page_Review_Item{

    String profile;
    String nick;
    String img;
    String contents;
    String no;
    String date;
    Context context;
    int type;

    //        array_push($result,  array('no'=>$row[0],'writer'=>$row[1],'date'=>$row[2],'img'=>$row[3],'content'=>$row[4],'profile'=>$row2[0]));
    public Page_Review_Item(Context context,  String no , String nick, String date,  String img, String contents, String profile) {
        this.context = context;
        this.profile = profile;
        this.date = date;
        this.nick = nick;
        this.img = img;
        this.no= no;
        this.contents = contents;
        this.type = 1;
    }


    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}

