package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by u on 2017-02-17.
 */
public class Credit_List_Adapter extends RecyclerView.Adapter<Credit_List_Adapter.ViewHolder> {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList//
    private ArrayList<Credit_List_Item> listViewItemList;
    private Context context;
    ////
    private Util_Dialog dialog;
    public static final int ITEM_TYPE_ONE = 1;
    public static final int ITEM_TYPE_TWO = 2;

    int type;

    public Credit_List_Adapter(ArrayList<Credit_List_Item> item, Context context) {
        this.context = context;
        this.listViewItemList = item;
    }

    @Override
    public int getItemViewType(int position) {
        return ITEM_TYPE_ONE;
    }

    @Override
    public Credit_List_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_credit_list_item, viewGroup, false);
        return new Credit_List_Adapter.ViewHolder(view);
    }


    /**
     * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(final Credit_List_Adapter.ViewHolder viewHolder, final int position) {

        viewHolder.p_m.setText(listViewItemList.get(position).getP_m());
        viewHolder.total.setText(listViewItemList.get(position).getTotal());
        viewHolder.date.setText(listViewItemList.get(position).getDate());
        viewHolder.type.setText(listViewItemList.get(position).getType());
    }


    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }


    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView p_m, total, date, type;

        public ViewHolder(View itemView) {
            super(itemView);
            p_m = (TextView) itemView.findViewById(R.id.credit_list_pm);
            total = (TextView) itemView.findViewById(R.id.credit_list_current);
            date = (TextView) itemView.findViewById(R.id.credit_list_date);
            type = (TextView)itemView.findViewById(R.id.credit_list_title);
        }
    }
}



