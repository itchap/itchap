package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import com.bumptech.glide.RequestManager;

import java.util.ArrayList;

/**
 * Created by young on 2017-01-22.
 */

public class Page_Chatting_Adapter extends RecyclerView.Adapter<Page_Chatting_Adapter.ViewHolder> {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList//
    private ArrayList<Page_Chatting_Item> listViewItemList;
    private Context context;
    private RequestManager requestManager;

    public static final int ITEM_TYPE_RIGHT = 1;
    public static final int ITEM_TYPE_LEFT = 2;

    int type;

    public Page_Chatting_Adapter(ArrayList<Page_Chatting_Item> item, Context context, RequestManager requestManager) {
        this.context = context;
        this.listViewItemList = item;
        this.requestManager = requestManager;



    }
    private String convert(String date){
        if(date == null){
            return null;
        }else{
            Log.e("date", date) ;
            String year = date.substring(0,4);
            String month = date.substring(4,6);
            String day = date.substring(6,8);
            String hour = date.substring(8,10);
            String min = date.substring(10,12);

            if( Integer.parseInt(hour)> 12){
                return "오후 "+(Integer.parseInt(hour)-12)+":"+min;
            } else if( Integer.parseInt(hour) < 12){
                return "오전 "+hour+":"+min;
            }else
                return "오후 "+hour+":"+min;
        }

    }



    @Override
    public int getItemViewType(int position) {

        if (listViewItemList.get(position).isIsme()) {
            this.type = 1;
            return ITEM_TYPE_RIGHT;
        }else{
            this.type = 2;
            return ITEM_TYPE_LEFT;
        }



    }



     public Page_Chatting_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
         View view;
         if(viewType == ITEM_TYPE_RIGHT){
             view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_chat_item_right, viewGroup, false);
             return new Page_Chatting_Adapter.ViewHolder(view,1 );
         }else{
             view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_chat_item_left, viewGroup, false);
             return new Page_Chatting_Adapter.ViewHolder(view,2);
         }


     }


     /**
      * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(final Page_Chatting_Adapter.ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);

        if(itemtype == ITEM_TYPE_RIGHT) {
            viewHolder.dateTime.setText(convert(listViewItemList.get(position).getDateTime()));
            viewHolder.msg.setText(listViewItemList.get(position).getTxt());
        }else{
            viewHolder.dateTime.setText(convert(listViewItemList.get(position).getDateTime()));
            viewHolder.msg.setText(listViewItemList.get(position).getTxt());
            requestManager.load(listViewItemList.get(position).getImg()).asBitmap().into(viewHolder.img);

        }


    }


    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }


    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView msg, dateTime;
        protected Util_ImageView img;

        public ViewHolder(View itemView, int type) {
            super(itemView);
            if(type == ITEM_TYPE_RIGHT){
                msg = (TextView)itemView.findViewById(R.id.msg);
                dateTime = (TextView)itemView.findViewById(R.id.txt_time);
                img = null;
            }else{
                msg = (TextView)itemView.findViewById(R.id.msg);
                dateTime = (TextView)itemView.findViewById(R.id.txt_time);
                img = (Util_ImageView)itemView.findViewById(R.id.profile_img);
                img.setType(11);
            }



        }
    }
}



