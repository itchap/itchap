package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tsengvn.typekit.TypekitContextWrapper;

public class Page_Splash extends AppCompatActivity {
    private ImageView main;
    private Util_Dialog dialog;
    private NetworkInfo mobile;
    private NetworkInfo wifi;
    private ConnectivityManager manager;

    private Intent getintent;
    private Intent internet;

    private String store_version;
    private String device_version;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        manager = (ConnectivityManager) Page_Splash.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        dialog = new Util_Dialog(Page_Splash.this, getResources().getDrawable(R.drawable.icon_warning), "인터넷 연결을 확인해주세요.", "나가기", "확인", "network");

        getintent = getIntent();
        internet = getIntent();


        if (wifi.isConnected() || mobile.isConnected()) {
            store_version = MarketVersionChecker.getMarketVersion(getPackageName());
            try {
                device_version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if (store_version.compareTo(device_version) > 0) {
                dialog = new Util_Dialog(Page_Splash.this, getResources().getDrawable(R.drawable.icon_warning), "최신버전으로 업데이트 해주세요", "나가기", "확인", "update");
                dialog.show();
            } else {
                setContentView(R.layout.l_splash);
                main = (ImageView) findViewById(R.id.loding_logo);
                Glide.with(Page_Splash.this).load("http://175.126.38.139/img/itchap/splash_img.png")
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true).into(main);
                Handler hd = new Handler();
                hd.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent = new Intent(Page_Splash.this, Page_Login.class);
                        if (getintent != null) {
                            intent.putExtra("logout", getintent.getBooleanExtra("logout", false));
                        }
                        startActivity(intent);
                        finish();
                    }
                }, 3000); // 3초 후에 hd Handler 실행

            }
        } else if (internet.getBooleanExtra("finish", false)) {
            moveTaskToBack(true);
            finish();
            android.os.Process.killProcess(android.os.Process.myPid());
        } else {
            dialog.show();
        }

    }


}


