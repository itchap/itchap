package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.renderscript.Type;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by Administrator on 2016-08-20.
 */

public class Util_SpinnerAdapter extends ArrayAdapter<String> {
    Context context;
    String[] items = new String[] {};
    Typeface typeface;
    String data = null;
    int type = -1;

    public Util_SpinnerAdapter(final Context context,
                          final int textViewResourceId, final String[] objects) {
        super(context, textViewResourceId, objects);
        typeface = Typeface.createFromAsset(context.getAssets(), "NotoSans-Medium.otf");
        this.items = objects;
        this.context = context;
    }

    public Util_SpinnerAdapter(final Context context,
                               final int textViewResourceId, final String[] objects, String data, int type) {
        super(context, textViewResourceId, objects);
        typeface = Typeface.createFromAsset(context.getAssets(), "NotoSans-Medium.otf");
        this.items = objects;
        this.context = context;
        this.data = data;
        this.type = type;

    }

    /**
     * 스피너 클릭시 보여지는 View의 정의
     */
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(
                    android.R.layout.simple_spinner_dropdown_item, parent, false);

        }

        TextView tv = (TextView) convertView
                .findViewById(android.R.id.text1);
        tv.setText(items[position]);
        //tv.setTextColor(Color.parseColor("#C5C5C5"));
        tv.setTextColor(convertView.getResources().getColor(R.color.text_default));
       // tv.setBackgroundColor(Color.parseColor("#FFFFFF"));
        tv.setBackgroundColor(convertView.getResources().getColor(R.color.white));
        tv.setGravity(Gravity.CENTER);
        tv.setTextSize(16);
        tv.setTypeface(typeface);

        return convertView;
    }

    /**
     * 기본 스피너 View 정의
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(
                    android.R.layout.simple_spinner_item, parent, false);
        }
        // android.r.id.text1  >>  사이즈
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView tv = (TextView) convertView
                .findViewById(android.R.id.text1);
        tv.setPadding(0,5,0,0);
        tv.setTextSize(18);
        tv.setGravity(Gravity.CENTER_VERTICAL);
        tv.setTypeface(typeface);
        tv.setTextColor(convertView.getResources().getColor(R.color.text_default));
        if(type == 0 && position == 0){
            tv.setText(data);
        }else if(type ==1 && position == 0){
            tv.setText(data);
        }else
            tv.setText(items[position]);
        //tv.setTextColor(Color.parseColor("#D1D1D1"));

/*
        if(items[position].equals("@kyonggi.ac.kr")){
            tv.setText(items[position]);
            tv.setTextColor(Color.parseColor("#D1D1D1"));
        }else {
            tv.setTextColor(Color.parseColor("#000000"));
            tv.setText(items[position]);
        }
*/
        tv.setLayoutParams(lp);
        return convertView;
    }
}