package com.uandm.administrator.itsuandm;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tsengvn.typekit.Typekit;

/**
 * Created by Administrator on 2016-08-22.
 */
public class Init_Font extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Typekit.getInstance()
                .addCustom4(Typekit.createFromAsset(this, "NotoSans-Bold.otf"))
                .addCustom2(Typekit.createFromAsset(this, "NotoSans-Medium.otf"))
                .addCustom1(Typekit.createFromAsset(this, "NotoSans-Regular.otf"))
                .addCustom3(Typekit.createFromAsset(this, "NotoSans-Light.otf"))
                .addCustom5(Typekit.createFromAsset(this, "BMJUA_ttf.ttf"));

    }
}
