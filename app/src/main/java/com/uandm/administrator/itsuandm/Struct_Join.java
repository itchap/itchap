package com.uandm.administrator.itsuandm;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.net.URI;

/**
 * Created by Administrator on 2016-11-21.
 */

public class Struct_Join  implements  Serializable{

    /* like , photo ,phone인증 추가 예정 */
    int i;
    String id;
    String pw;
    String nick;
    String name;
    String phone;
    int gender;
    int nation;
    String job;
    int status = -1;
    int organize_team = -1;
    String height;
    String weght;
    String like1;
    String like2;
    String like3;
    String img1;
    String img2;
    String img3;
    int age;

    Struct_Join(){
        for(int i=0; i<check.length; i++)
            check[i] = false;
    }


    void print(){
        System.out.println(id + pw + nick + height + weght+ like1 + like2 +like3 + img1 +" " +img2+ " "+ img3+ name + phone + gender+ nation + age + job);
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }


    boolean check[] = new boolean[11];

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getNation() {
        return nation;
    }

    public void setNation(int nation) {
        this.nation = nation;
    }


    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    public String getLike1() {
        return like1;
    }

    public void setLike1(String like1) {
        this.like1 = like1;
    }

    public String getLike2() {
        return like2;
    }

    public void setLike2(String like2) {
        this.like2 = like2;
    }

    public String getLike3() {
        return like3;
    }

    public void setLike3(String like3) {
        this.like3 = like3;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }


    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeght() {
        return weght;
    }

    public void setWeght(String weght) {
        this.weght = weght;
    }




}
