package com.uandm.administrator.itsuandm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.*;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ScrollingTabContainerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.*;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;
import com.tsengvn.typekit.TypekitContextWrapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class Drawer_Organize_Team extends AppCompatActivity {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
//    private BroadcastReceiver receiver_am;
    private AlarmManager am;
    private ArrayList<Drawer_Organize_Team_Item> team_item;
    private Drawer_Organize_Team_Adapter adapter;
    public RecyclerView recyclerview;

    public Util_Dialog dialog;
    private RelativeLayout rel;
    private ImageView add_team_list;
    private EditText search_team_list;
    private ImageView back;

    private RelativeLayout no_item;

    ProgressDialog loading;
    private String json;
    String myJSON = null;
    JSONArray team_list = null;

    private static final String TAG_RESULTS = "result";
    private static final String TAG_NICK = "nick";
    private static final String TAG_PHOTO_MAIN = "photo_main";

    private Util_ImageView team_2, team_3, team_4, my_photo;
    private ImageView team_21, team_31, team_41;

    private String aa, bb, cc;
    private TextView next;

    private String selected_nick;

    public static String[] temp_team_list = new String[3];
    public static String[] temp_team_list_photo = new String[3];

    PendingIntent sender;
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        unregisterReceiver(receiver_am);
//    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.d_organize_team);

        IntentFilter filter_am = new IntentFilter();
        filter_am.addAction("am_cancel");


        no_item = (RelativeLayout) findViewById(R.id.no_item);
        next = (TextView) findViewById(R.id.next);
        pref = this.getSharedPreferences("login", 0);
        editor = pref.edit();

        team_2 = (Util_ImageView) findViewById(R.id.team_2);
        team_3 = (Util_ImageView) findViewById(R.id.team_3);
        team_4 = (Util_ImageView) findViewById(R.id.team_4);

        team_2.setType(11);
        team_3.setType(11);
        team_4.setType(11);

        team_2.setOnClickListener(del_team);
        team_3.setOnClickListener(del_team);
        team_4.setOnClickListener(del_team);

        team_21 = (ImageView) findViewById(R.id.team_21);
        team_31 = (ImageView) findViewById(R.id.team_31);
        team_41 = (ImageView) findViewById(R.id.team_41);

        my_photo = (Util_ImageView) findViewById(R.id.my_photo);
        my_photo.setType(11);

        recyclerview = (RecyclerView) findViewById(R.id.list);
        recyclerview.setOverScrollMode(View.OVER_SCROLL_NEVER);
        recyclerview.setNestedScrollingEnabled(false);
        LinearLayoutManager layout = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(layout);
        team_item = new ArrayList<>();
        back=(ImageView)findViewById(R.id.back_btn_img) ;
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //화면 밖 클릭 시 죽이고
        rel = (RelativeLayout) findViewById(R.id.rel);
        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(rel.getWindowToken(), 0);
            }
        });

        //

        search_team_list = (EditText) findViewById(R.id.search_nick);
        selected_nick = search_team_list.getText().toString();

        //내가 추가한 팀 리스트
        String result2 = getData("http://175.126.38.139/php/getdata_organize_team_rendering.php");
        if (result2.equals("no_list")) {
            Log.e("no_list", "");
        } else {
            rendering(result2);
        }


        //내 프로필 사진_set
        String my_profile = getData("http://175.126.38.139/php/my_profile_set.php");
        my_profile(my_profile);

        //친구 리스트 추가
        add_team_list = (ImageView) findViewById(R.id.add_team_list);
        add_team_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(rel.getWindowToken(), 0);
                selected_nick = search_team_list.getText().toString();
                if (!selected_nick.equals("")) {
                    String cc = getData("http://175.126.38.139/php/getdata_organize_team.php");
                    addList(cc);
                } else if (search_team_list.getText().toString().equals("")) {
                    dialog = new Util_Dialog(Drawer_Organize_Team.this, getResources().getDrawable(R.drawable.icon_lastpage), "닉네임을 입력해주세요", "확인", "dismiss");
                    dialog.show();
                }
            }

        });

        team_2.setOnClickListener(del_team);
        team_3.setOnClickListener(del_team);
        team_4.setOnClickListener(del_team);


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (team_2.getVisibility() == INVISIBLE) {
                    dialog = new Util_Dialog(Drawer_Organize_Team.this, getResources().getDrawable(R.drawable.icon_lastpage), "최소 한명의 팀원을 추가해야합니다.", "확인", "dismiss");
                    dialog.show();
                } else {
                    if (team_4.getVisibility() == VISIBLE) {
                        try {
                            json = new Util_PHP(Drawer_Organize_Team.this).execute(pref.getString("nick", ""), temp_team_list[2], temp_team_list[1], temp_team_list[0], "add_team_4", "http://175.126.38.139/php/fcm/add_team.php").get();
                        } catch (Exception e) {
                            e.getStackTrace();
                        }


                    } else if (team_3.getVisibility() == VISIBLE) {
                        try {
                            json = new Util_PHP(Drawer_Organize_Team.this).execute(pref.getString("nick", ""), temp_team_list[1], temp_team_list[0], "add_team_3", "http://175.126.38.139/php/fcm/add_team.php").get();
                        } catch (Exception e) {
                            e.getStackTrace();
                        }
                    } else if (team_2.getVisibility() == VISIBLE) {
                        try {
                            json = new Util_PHP(Drawer_Organize_Team.this).execute(pref.getString("nick", ""), temp_team_list[0], "add_team_2", "http://175.126.38.139/php/fcm/add_team.php").get();
                        } catch (Exception e) {
                            e.getStackTrace();
                        }
                    }

                    finish();

                }
            }
        });

    }


    protected void addList(String bb) {
        // 본인추가할경우 에러발생
        try {
            no_item.setVisibility(GONE);
            recyclerview.setVisibility(VISIBLE);
            JSONObject jsonObj2 = new JSONObject(bb);
            team_list = jsonObj2.getJSONArray(TAG_RESULTS);
            if (team_list.length() == 1) {
                JSONObject c = team_list.getJSONObject(0);
                String uri = c.getString(TAG_PHOTO_MAIN);
                Uri str_to_uri = Uri.parse(uri);
                team_item.add(new Drawer_Organize_Team_Item(Drawer_Organize_Team.this, c.getString(TAG_NICK), str_to_uri, 1));
                adapter = new Drawer_Organize_Team_Adapter(team_item, Drawer_Organize_Team.this, team_2, team_3, team_4, team_21, team_31, team_41);
                recyclerview.setAdapter(adapter);
            } else {
                Log.e("ERROR", "동일 닉네임 한명 이상");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    //내 친구 리스트 뿌리기
    protected void rendering(String my_friend_list) {
        try {
            no_item.setVisibility(GONE);
            recyclerview.setVisibility(VISIBLE);
            JSONObject jsonObj = new JSONObject(my_friend_list);
            team_list = jsonObj.getJSONArray(TAG_RESULTS);
            for (int i = 0; i < team_list.length(); i++) {
                JSONObject c = team_list.getJSONObject(i);
                String uri = c.getString(TAG_PHOTO_MAIN);
                Uri str_to_uri = Uri.parse(uri);
                team_item.add(new Drawer_Organize_Team_Item(Drawer_Organize_Team.this, c.getString(TAG_NICK), str_to_uri, 1));
                adapter = new Drawer_Organize_Team_Adapter(team_item, Drawer_Organize_Team.this, team_2, team_3, team_4, team_21, team_31, team_41);
                recyclerview.setAdapter(adapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void my_profile(String bb) {
        try {
            JSONObject jsonObj2 = new JSONObject(bb);
            team_list = jsonObj2.getJSONArray(TAG_RESULTS);
            if (team_list.length() == 1) {
                JSONObject c = team_list.getJSONObject(0);
                String uri = c.getString(TAG_PHOTO_MAIN);
                Uri str_to_uri = Uri.parse(uri);
                Glide.with(Drawer_Organize_Team.this).load(str_to_uri).asBitmap().into(my_photo);
            } else {
                Log.e("ERROR", "동일 닉네임 한명 이상");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getData(String url) {
        class GetDataJSON extends AsyncTask<String, Void, String> {
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";

            @Override
            protected String doInBackground(String... params) {
                DataOutputStream os;
                String uri = params[0];
                BufferedReader bufferedReader = null;
                StringBuilder sb = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Connection", "Keep-Alive");
                    con.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    os = new DataOutputStream(con.getOutputStream());


                    String nick = selected_nick;
                    os.writeBytes(twoHyphens + boundary + lineEnd);
                    os.writeBytes("Content-Disposition: form-data; name=\"nick\"" + lineEnd);
                    os.writeBytes(lineEnd);
                    os.write(nick.getBytes());
                    os.writeBytes(lineEnd);


                    String my_nick = pref.getString("nick", "");
                    os.writeBytes(twoHyphens + boundary + lineEnd);
                    os.writeBytes("Content-Disposition: form-data; name=\"my_nick\"" + lineEnd);
                    os.writeBytes(lineEnd);
                    os.write(my_nick.getBytes());
                    os.writeBytes(lineEnd);

                    os.flush();
                    os.close();

                    BufferedReader rd = null;
                    rd = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                    sb = new StringBuilder();
                    String json;
                    while ((json = rd.readLine()) != null) {
                        sb.append(json + "\n");
                    }
                    Log.e("exper", sb.toString().trim());
                    return sb.toString().trim();
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }


            @Override
            protected void onPostExecute(String result) {
            }
        }


        String result2 = null;
        GetDataJSON g = new GetDataJSON();
        try {
            result2 = g.execute(url).get();
            if (result2.equals("no_value")) {
                dialog = new Util_Dialog(Drawer_Organize_Team.this, getResources().getDrawable(R.drawable.icon_lastpage), "해당 닉네임이 없습니다.", "확인", "dismiss");
                dialog.show();
            } else if (result2.equals("different_gender")) {
                dialog = new Util_Dialog(Drawer_Organize_Team.this, getResources().getDrawable(R.drawable.icon_lastpage), "이성은 추가할 수 없습니다.", "확인", "dismiss");
                dialog.show();
            } else if (result2.equals("already")) {
                dialog = new Util_Dialog(Drawer_Organize_Team.this, getResources().getDrawable(R.drawable.icon_lastpage), "이미 친구목록에 있습니다.", "확인", "dismiss");
                dialog.show();
            } else if (result2.equals("meme")) {
                dialog = new Util_Dialog(Drawer_Organize_Team.this, getResources().getDrawable(R.drawable.icon_lastpage), "자기 자신은 추가할 수 없습니다.", "확인", "dismiss");
                dialog.show();
            } else if (result2.equals("no_list")) {
                Log.e("no_item", "");
            } else if (result2.equals("ERROR")) {
                Log.e("ERROR", "");
            } else {
                Log.e("SUCCESS", "");
            }
        } catch (Exception e) {
            result2 = null;
        }
        return result2;
    }

    View.OnClickListener del_team = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.team_2:
                    if (team_4.getVisibility() == VISIBLE) {
                        temp_team_list[0] = temp_team_list[1];
                        temp_team_list[1] = temp_team_list[2];
                        temp_team_list[2] = "";
                        temp_team_list_photo[0] = temp_team_list_photo[1];
                        temp_team_list_photo[1] = temp_team_list_photo[2];
                        temp_team_list_photo[2] = "";
                        Glide.with(Drawer_Organize_Team.this).load(temp_team_list_photo[0]).asBitmap().into(team_2);
                        Glide.with(Drawer_Organize_Team.this).load(temp_team_list_photo[1]).asBitmap().into(team_3);
                        team_4.setVisibility(INVISIBLE);
                        team_41.setVisibility(INVISIBLE);
                    } else if (team_3.getVisibility() == VISIBLE) {
                        temp_team_list[0] = temp_team_list[1];
                        temp_team_list[1] = "";
                        temp_team_list_photo[0] = temp_team_list_photo[1];
                        temp_team_list_photo[1] = "";
                        Glide.with(Drawer_Organize_Team.this).load(temp_team_list_photo[0]).asBitmap().into(team_2);
                        team_3.setVisibility(INVISIBLE);
                        team_31.setVisibility(INVISIBLE);
                    } else {
                        temp_team_list[0] = "";
                        temp_team_list_photo[0] = "";
                        team_2.setVisibility(INVISIBLE);
                        team_21.setVisibility(INVISIBLE);
                    }
                    break;
                case R.id.team_3:
                    if (team_4.getVisibility() == VISIBLE) {
                        temp_team_list[1] = temp_team_list[2];
                        temp_team_list[2] = "";
                        temp_team_list_photo[1] = temp_team_list_photo[2];
                        temp_team_list_photo[2] = "";
                        Glide.with(Drawer_Organize_Team.this).load(temp_team_list_photo[1]).asBitmap().into(team_3);
                        team_4.setVisibility(INVISIBLE);
                        team_41.setVisibility(INVISIBLE);
                    } else {
                        temp_team_list[1] = "";
                        temp_team_list_photo[1] = "";
                        team_3.setVisibility(INVISIBLE);
                        team_31.setVisibility(INVISIBLE);
                    }
                    break;
                case R.id.team_4:
                    temp_team_list[2] = "";
                    temp_team_list_photo[2] = "";
                    team_4.setVisibility(INVISIBLE);
                    team_41.setVisibility(INVISIBLE);
                    break;

//                    getData("http://175.126.38.139/php/del_team.php");
//                    String result4 = getData("http://175.126.38.139/php/rendering_my_team_list.php");
//                    Log.e("bbb",""+result4);
//                    rendering_my_team(result4);


            }
        }
    };


}
