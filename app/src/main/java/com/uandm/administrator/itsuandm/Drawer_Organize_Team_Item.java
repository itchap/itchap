package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;

/**
 * Created by u on 2016-12-27.
 */
public class Drawer_Organize_Team_Item {


    Uri uri;
    String nick;
    Context context;


    public Drawer_Organize_Team_Item(Context context, String nick, Uri uri_for_main_photo ,  int type) {
        this.context =  context;
        this.uri = uri_for_main_photo;
        this.nick = nick;
        this.type = type;
    }
    public Drawer_Organize_Team_Item(Context context,  int type) {
        this.context =  context;
        this.uri = null;
        this.nick = null;
        this.type = type;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    int type;
}

