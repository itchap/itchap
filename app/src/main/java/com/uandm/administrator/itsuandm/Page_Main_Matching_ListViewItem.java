package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by young on 2016-11-22.
 */

public class Page_Main_Matching_ListViewItem {

    String img[] = new String[4];
    Bitmap init;
    String loc;
    String date;
    String time;
    String title;
    String cnt_like;
    String age;
    String owner_name;
    int type;
    String date_time;

//


    public Page_Main_Matching_ListViewItem(Context context, String owner_name,String img[], String title, String loc, String date_time, String age, String cnt_like, int type) {


        for (int i = 0; i < img.length; i++) {
            if (img[i] != null)
                this.img[i] = img[i];
            else
                this.img[i] = null;
        }

//        BitmapDrawable drawable = (BitmapDrawable) context.getResources().getDrawable(R.drawable.white);
//        init= drawable.getBitmap();
//
//        for (int i = 0; i < img.length; i++) {
//            if (img[i] != null)
//                this.img_[i] = img[i];
//            else
//                this.img_[i] = init;
//        }

        this.owner_name = owner_name;
        this.title = title;
        this.loc = loc;
        this.date = date_time.substring(4,6)+"월"+date_time.substring(6,8)+"일";
        this.time = date_time.substring(8,10)+":"+date_time.substring(10);
        this.age = age;
        this.cnt_like = cnt_like;
        this.type = type;
        this.date_time = date_time;


    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }
    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCnt_like() {
        return cnt_like;
    }

    public void setCnt_like(String cnt_like) {
        this.cnt_like = cnt_like;
    }


    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String[] getImg() {
        return img;
    }

    public void setImg(String[] img) {

        for (int i = 0; i < img.length; i++) {
            if (img[i] != null)
                this.img[i] = img[i];
        }
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }





}

