package com.uandm.administrator.itsuandm;

import android.*;
import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.graphics.*;

import android.graphics.drawable.Drawable;
import android.net.Uri;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;

import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.tsengvn.typekit.TypekitContextWrapper;
import org.w3c.dom.Text;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import java.util.regex.Pattern;

import static android.view.View.VISIBLE;

public class Page_Join extends AppCompatActivity {

    public Struct_Join struct_join;
    public Util_Dialog custom_dial;
    public Bitmap img[] = new Bitmap[3];

    private RelativeLayout btn_next, btn_back;
    private EditText edit_id, edit_pw, edit_repw, edit_nick, edit_job;
    private EditText edit_tag_like;
    private TextView like_1, like_2, like_3;
    private ImageView del_1, del_2, del_3;
    private int input_no = 0, input_no_ =0;
    private TextView is_able_id, is_able_repw, is_able_nick, is_able_job;

    private RelativeLayout layout_content;
    private ImageView btn_girl, btn_boy;

    private Spinner spin_like, spin_height, spin_weight;
    private Util_SpinnerAdapter spin_height_adapter, spin_weight_adapter;

    private boolean on_keyboard = false;
    private boolean set_photo[] = new boolean[3];

    private Util_ImageView photo_1, photo_2, photo_3;
    public static Uri mImageCaptureUri;
    public static Uri outputFileUri;
    private Uri photouri[] = new Uri[3];
    private static final int PICK_FROM_CAMERA = 0;
    private static final int PICK_FROM_ALBUM = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE =  1;
    private static final int PERMISSIONS_REQUEST_CAMERA = 0;

    private int sel_photo = 0;


    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_join_form);

//        Init_Font.setGlobalFont(this, getWindow().getDecorView());

        findid();
        for(int i =0 ; i<photouri.length; i++){
            photouri[i] = null;
        }

        struct_join = new Struct_Join();

        String[] spin_height_item = new Spinner_data_init().init_height();
        String[] spin_weight_item = new Spinner_data_init().init_weight();

        spin_height_adapter = new Util_SpinnerAdapter(this,
                android.R.layout.simple_spinner_dropdown_item, spin_height_item);
        spin_weight_adapter = new Util_SpinnerAdapter(this,
                android.R.layout.simple_spinner_dropdown_item, spin_weight_item);

        spin_height.setAdapter(spin_height_adapter);
        spin_weight.setAdapter(spin_weight_adapter);

        String filterid = "^[a-zA-Z]{1}[a-zA-Z0-9_]{4,11}$";
        String filterpw = "^(?=.*[a-zA-Z]+)(?=.*[!@#$%^*+=-]|.*[0-9]+).{6,20}$";
        String filternick = "^[가-힣]{2,5}$";
        final Pattern psid = Pattern.compile(filterid);
        final Pattern pspw = Pattern.compile(filterpw);
        final Pattern psnick = Pattern.compile(filternick);

        edit_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edit_id.getText().toString().length() < 6 || edit_id.getText().toString().length() > 11) {
                    is_able_id.setText("* Id는 6~12자로 해주세요");
                    is_able_id.setVisibility(VISIBLE);
                    struct_join.check[0] = false;
                } else if (!psid.matcher(edit_id.getText().toString()).matches()) {
                    is_able_id.setText("* Id는 영문,숫자의 조합으로만 가능합니다.");
                    is_able_id.setVisibility(VISIBLE);
                    struct_join.check[0] = false;
                } else {
                    is_able_id.setVisibility(View.INVISIBLE);
                    struct_join.check[0] = true;
                }
            }
        });
        edit_pw.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            /* 비밀번호 일치 체크 */
                if (!edit_pw.getText().toString().equals(edit_repw.getText().toString())) {
                    is_able_repw.setText("* Pw가 올바르지 않습니다.");
                    is_able_repw.setVisibility(VISIBLE);
                    struct_join.check[1] = false;
                } else if (!pspw.matcher(edit_pw.getText().toString()).matches()) {
                    is_able_repw.setText("* Pw는 6자 이상, 영어,숫자 조합입니다.");
                    is_able_repw.setVisibility(VISIBLE);
                    struct_join.check[1] = false;
                } else {
                    is_able_repw.setVisibility(View.INVISIBLE);
                    struct_join.check[1] = true;
                }
            }
        });
        edit_repw.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            /* 비밀번호 일치 체크 */
                if (!edit_pw.getText().toString().equals(edit_repw.getText().toString())) {
                    is_able_repw.setText("* Pw가 일치하지 않습니다.");
                    is_able_repw.setVisibility(VISIBLE);
                    struct_join.check[1] = false;
                } else if (!pspw.matcher(edit_repw.getText().toString()).matches()) {
                    is_able_repw.setText("* Pw는 6자 이상, 영어,숫자 조합입니다.");
                    is_able_repw.setVisibility(VISIBLE);
                    struct_join.check[1] = false;
                } else {
                    is_able_repw.setVisibility(View.INVISIBLE);
                    struct_join.check[1] = true;
                }
            }
        });
        edit_nick.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            /* 비밀번호 일치 체크 */
                if (!psnick.matcher(edit_nick.getText().toString()).matches()) {
                    is_able_nick.setText("* 닉네임은 2~5자 한글만 입력 가능합니다.");
                    is_able_nick.setVisibility(VISIBLE);
                    struct_join.check[2] = false;
                } else {
                    is_able_nick.setVisibility(View.INVISIBLE);
                    struct_join.check[2] = true;
                }
            }
        });

        edit_job.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!psnick.matcher(edit_job.getText().toString()).matches()) {
                    is_able_job.setText("* 직업은 한글만 입력 가능합니다.");
                    is_able_job.setVisibility(VISIBLE);
                    struct_join.check[5] = false;
                } else {
                    is_able_job.setVisibility(View.INVISIBLE);
                    struct_join.check[5] = true;
                }
            }
        });

//
//        //본인인증(이름/성별)
//        struct_join.check[3] = true;
//        struct_join.check[4] = true;
    }

    void findid() {

        findViewById(R.id.content).setOnClickListener(btnlistener);
        findViewById(R.id.content_layout_height).setOnClickListener(btnlistener);
        findViewById(R.id.content_layout_weight).setOnClickListener(btnlistener);
        findViewById(R.id.join_btn_next).setOnClickListener(btnlistener);

        edit_id = (EditText) findViewById(R.id.edit_input_id);
        edit_pw = (EditText) findViewById(R.id.edit_input_pw);
        edit_repw = (EditText) findViewById(R.id.edit_input_repw);
        edit_nick = (EditText) findViewById(R.id.edit_input_nick);

        edit_tag_like = (EditText) findViewById(R.id.edit_input_like);
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter.LengthFilter(6);
        edit_tag_like.setFilters(filters);

        edit_tag_like.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm2.hideSoftInputFromWindow(edit_tag_like.getWindowToken(), 0);
                    if(edit_tag_like.getText().toString() == null || edit_tag_like.getText().toString().equals("")){

                    }else{
                        if (input_no == 0) {
                            like_1.setText(edit_tag_like.getText());
                            like_1.setVisibility(VISIBLE);
                            del_1.setVisibility(VISIBLE);
                            del_1.setOnClickListener(del_onclick);
                            edit_tag_like.setText("");
                            edit_tag_like.setHint("");
                            input_no = 1;
                            input_no_ = 1;
                        } else if (input_no == 1) {
                            like_2.setText(edit_tag_like.getText());
                            like_2.setVisibility(VISIBLE);
                            del_2.setVisibility(VISIBLE);
                            del_2.setOnClickListener(del_onclick);
                            edit_tag_like.setText("");
                            edit_tag_like.setHint("");
                            input_no_ = 2;
                            input_no = 2;
                        } else if (input_no == 2) {
                            like_3.setText(edit_tag_like.getText());
                            like_3.setVisibility(VISIBLE);
                            del_3.setVisibility(VISIBLE);
                            del_3.setOnClickListener(del_onclick);
                            edit_tag_like.setText("");
                            edit_tag_like.setHint("");
                            input_no = 3;
                            input_no_ = 3;
                        } else if (input_no == 3) {
                            edit_tag_like.setText("");
                            edit_tag_like.setHint("");
                            custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_exit), "관심사는 3가지만 입력해주세요", "확인", "dismiss");
                            custom_dial.show();
                        }
                    }
                }
                return false;
            }
        });
        edit_tag_like.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_DEL){

                }return false;
            }
        });
        edit_tag_like.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                on_keyboard = true;
                return false;
            }
        });
        like_1 = (TextView) findViewById(R.id.like_1);
        like_2 = (TextView) findViewById(R.id.like_2);
        like_3 = (TextView) findViewById(R.id.like_3);
        del_1 = (ImageView) findViewById(R.id.del_1);
        del_2 = (ImageView) findViewById(R.id.del_2);
        del_3 = (ImageView) findViewById(R.id.del_3);

        is_able_id = (TextView) findViewById(R.id.is_able_id);
        is_able_repw = (TextView) findViewById(R.id.is_able_repw);
        is_able_nick = (TextView) findViewById(R.id.is_able_nick);
        is_able_job  =(TextView)findViewById(R.id.is_able_job);

        spin_height = (Spinner) findViewById(R.id.spinner_height);
        spin_height.setOnTouchListener(touchListener);
        spin_weight = (Spinner) findViewById(R.id.spinner_weight);
        spin_weight.setOnTouchListener(touchListener);


        btn_back = (RelativeLayout)findViewById(R.id.join_back_btn);
        btn_back.setOnClickListener(btnlistener);

        //사진
        photo_1 = (Util_ImageView) findViewById(R.id.photo_1);
        photo_1.setOnClickListener(btnlistener);
        photo_2 = (Util_ImageView) findViewById(R.id.photo_2);
        photo_2.setOnClickListener(btnlistener);
        photo_3 = (Util_ImageView) findViewById(R.id.photo_3);
        photo_3.setOnClickListener(btnlistener);

        edit_job = (EditText)findViewById(R.id.edit_input_job);




    }

    void hide_keyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit_id.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(edit_pw.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(edit_repw.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(edit_nick.getWindowToken(), 0);
    }

    void check_avail(int i) {
        switch (i) {
            case -1:
                break;
            case 0:
                custom_dial = new Util_Dialog(Page_Join.this,getResources().getDrawable(R.drawable.icon_exit) ,"아이디가 올바르지 않습니다.","확인" ,"dismiss");
                custom_dial.show();
                //아이디 오류
                break;
            case 1:
                custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_exit) ,"비밀번호가 올바르지 않습니다.","확인" ,"dismiss");
                custom_dial.show();
                break;
            case 2:
                custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_exit) ,"닉네임이 올바르지 않습니다.","확인" ,"dismiss");
                custom_dial.show();
                break;
            case 3:
                custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_exit) ,"본인의 키를 선택해 주세요.", "확인" ,"dismiss");
                custom_dial.show();
                break;
            case 4:
                custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_exit) ,"본인의 체형을 선택해 주세요.", "확인" ,"dismiss");
                custom_dial.show();
                break;
            case 5:
                custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_exit) ,"직업이 올바르지 않습니다.", "확인" ,"dismiss");
                custom_dial.show();
                break;
            case 6:
                custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_exit) ,"관심사가 올바르지 않습니다.", "확인" ,"dismiss");
                custom_dial.show();
                break;

            case 7:
                custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_exit) ,"프로필 사진 3장을 올려주세요.", "확인" ,"dismiss");
                custom_dial.show();
                break;
            case 8:
                custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_exit) ,"회원정보를 다시 확인해 주세요.", "확인" ,"dismiss");
                custom_dial.show();
                break;

        }
    }

    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            hide_keyboard();
            return false;
        }
    };

    View.OnClickListener btnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            int chekc_count = 0;
            int get_false = -1;
            switch (v.getId()) {
                case R.id.photo_1:
                    sel_photo = 1;
                    addPhoto(v);
                    break;
                case R.id.photo_2:
                    sel_photo = 2;
                    addPhoto(v);
                    break;
                case R.id.photo_3:
                    sel_photo = 3;
                    addPhoto(v);
                    break;
                case R.id.join_btn_next:
                    String result = null;
                    /*
                    여기까지 내용 db 저장
                     */
                    if (filtering(edit_id.getText().toString(), "id")) {
                        is_able_id.setVisibility(VISIBLE);
                        struct_join.check[0] = false;
                    } else {
                        struct_join.setId(edit_id.getText().toString());
                        struct_join.check[0] = true;
                    }

                    if (filtering(edit_pw.getText().toString(), "pw") ||
                            filtering(edit_repw.getText().toString(), "pw")) {
                        is_able_repw.setVisibility(VISIBLE);
                        struct_join.check[1] = false;
                    } else {
                        struct_join.setPw(edit_pw.getText().toString());
                        struct_join.check[1] = true;
                    }

                    if (filtering(edit_nick.getText().toString(), "nick")) {

                        is_able_nick.setVisibility(VISIBLE);
                        struct_join.check[2] = false;
                    } else {
                        struct_join.setNick(edit_nick.getText().toString());
                        struct_join.check[2] = true;
                    }

                    if (spin_height.getSelectedItem().toString().equals("키")) {

                        struct_join.check[3] = false;
                    } else {

                        struct_join.setHeight(spin_height.getSelectedItem().toString());
                        struct_join.check[3] = true;
                    }

                    if (spin_weight.getSelectedItem().toString().equals("체형")) {
                        struct_join.check[4] = false;
                    } else {
                        struct_join.setWeght(spin_weight.getSelectedItem().toString());
                        struct_join.check[4] = true;
                    }

                    if(like_3.getVisibility() == View.GONE){
                        struct_join.check[6] = false;
                    }else{
                        struct_join.check[6] = true;
                        struct_join.setLike1(like_1.getText().toString());
                        struct_join.setLike2(like_2.getText().toString());
                        struct_join.setLike3(like_3.getText().toString());
                    }

                    if(set_photo[0] && set_photo[1] && set_photo[2]){
                        struct_join.setImg1(photouri[0].getPath().toString());
                        struct_join.setImg2(photouri[1].getPath().toString());
                        struct_join.setImg3(photouri[2].getPath().toString());
                        struct_join.check[7] = true;
                    }else{
                        struct_join.check[7] = false;
                    }

                    if(is_able_id.getVisibility() == View.VISIBLE || is_able_nick.getVisibility() == View.VISIBLE
                            || is_able_repw.getVisibility() == View.VISIBLE || is_able_job.getVisibility() == View.VISIBLE){
                        struct_join.check[8] = false;
                    }else
                        struct_join.check[8] = true;


                    if (filtering(edit_job.getText().toString(), "job")) {
                        is_able_job.setVisibility(VISIBLE);
                        struct_join.check[5] = false;
                    } else {
                        struct_join.setJob(edit_job.getText().toString());
                        struct_join.check[5] = true;
                    }


                    for (int i = 0; i < 9; i++) {
                        if (!struct_join.check[i]) {
                            chekc_count++;
                            get_false = i;
                            break;
                        }
                    }

                    if (chekc_count > 0) {
                        check_avail(get_false);
                    }else {
                        try {
                            result = new Check_id_nick().execute(struct_join.getId(), struct_join.getNick()).get();
                            Log.e("result", result);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (result.equals("id_fail")) {
                            custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_lastpage), "이미 존재하는 아이디 입니다.", "확인", "dismiss");
                            custom_dial.show();
                        } else if (result.equals("nick_fail")) {
                            custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_lastpage), "이미 존재하는 닉네임 입니다.", "확인", "dismiss");
                            custom_dial.show();
                        } else {
                            intent = new Intent(Page_Join.this, Page_Join1.class);
                            intent.putExtra("tt", struct_join);
                            startActivity(intent);
                        }
                    }

                    break;
                case R.id.content:
                    hide_keyboard();
                    if(on_keyboard) {
                        on_keyboard = false;
                        if (edit_tag_like.getText().toString() == null || edit_tag_like.getText().toString().equals("")) {

                        } else {
                            if (input_no_ == 0) {
                                like_1.setText(edit_tag_like.getText());
                                like_1.setVisibility(VISIBLE);
                                del_1.setVisibility(VISIBLE);
                                del_1.setOnClickListener(del_onclick);
                                edit_tag_like.setText("");
                                edit_tag_like.setHint("");
                                input_no = 1;
                                input_no_ = 1;
                            } else if (input_no_ == 1) {
                                like_2.setText(edit_tag_like.getText());
                                like_2.setVisibility(VISIBLE);
                                del_2.setVisibility(VISIBLE);
                                del_2.setOnClickListener(del_onclick);
                                edit_tag_like.setText("");
                                edit_tag_like.setHint("");
                                input_no = 2;
                                input_no_ = 2;
                            } else if (input_no_ == 2) {
                                like_3.setText(edit_tag_like.getText());
                                like_3.setVisibility(VISIBLE);
                                del_3.setVisibility(VISIBLE);
                                del_3.setOnClickListener(del_onclick);
                                edit_tag_like.setText("");
                                edit_tag_like.setHint("");
                                input_no = 3;
                                input_no_ = 3;
                            } else if (input_no_ == 3) {
                                edit_tag_like.setText("");
                                edit_tag_like.setHint("");
                                custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_lastpage), "관심사는 3가지만 입력해주세요", "확인", "dismiss");
                                custom_dial.show();
                            }
                        }
                    }
                    break;
                case R.id.join_back_btn:
                    finish();
                    break;
            }
        }
    };
    View.OnClickListener dialoglistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            custom_dial.dismiss();
        }
    };
    View.OnClickListener del_onclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.del_1:
                    if (like_3.getVisibility()==View.VISIBLE) {
                        like_3.setVisibility(View.GONE);
                        del_3.setVisibility(View.GONE);
                        like_1.setText(like_2.getText());
                        like_2.setText(like_3.getText());
                        input_no = 2;
                        input_no_ =2 ;
                    }else if(like_2.getVisibility()==View.VISIBLE){
                        like_2.setVisibility(View.GONE);
                        del_2.setVisibility(View.GONE);
                        like_1.setText(like_2.getText());
                        input_no = 1;
                        input_no_ =1 ;
                    }else{
                        like_1.setVisibility(View.GONE);
                        del_1.setVisibility(View.GONE);
                        like_1.setText("");
                        input_no = 0;
                        input_no_ =0 ;
                    }
                    break;
                case R.id.del_2:
                    if (like_3.getVisibility()==View.VISIBLE) {
                        like_3.setVisibility(View.GONE);
                        del_3.setVisibility(View.GONE);
                        like_2.setText(like_3.getText());
                        input_no = 2;
                        input_no_ =2 ;
                    }else {
                        like_2.setVisibility(View.GONE);
                        del_2.setVisibility(View.GONE);
                        like_2.setText("");
                        input_no = 1;
                        input_no_ =1 ;
                    }
                    break;
                case R.id.del_3:
                    like_3.setVisibility(View.GONE);
                    like_3.setText("");
                    del_3.setVisibility(View.GONE);
                    input_no = 2;
                    input_no_ =2 ;
                    break;
            }
        }
    };

    //
    public boolean filtering(String txt, String type) {
        switch (type) {
            case "id":
                if (txt == null || txt.equals("")) {
                    is_able_id.setText("* 아이디를 입력해 주세요.");
                    return true;
                }

                break;
            case "pw":
                if (txt == null || txt.equals("")) {
                    is_able_repw.setText("* 패스워드를 입력해 주세요.");
                    return true;
                }

                break;
            case "nick":
                if (txt == null || txt.equals("")) {
                    return true;
                }
                break;

            case "job":
                if (txt == null || txt.equals("")) {
                    return true;
                }
                break;
        }

        return false;
    }

    private void addPhoto(View v){
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();

        // 폴더명 및 파일명
        String folderPath = path + File.separator + "itchap";
        String filePath = path + File.separator + "itchap" + File.separator +  "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        File fileFolderPath = new File(folderPath);
        fileFolderPath.mkdir();

        // 파일 이름 지정
        File file = new File(filePath);
        outputFileUri = Uri.fromFile(file);

        final CharSequence[] items = new CharSequence[]{"사진촬영", "앨범선택", "취소"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(v.getContext());
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (items[which].equals("사진촬영")) {

                    if(ContextCompat.checkSelfPermission(Page_Join.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                        if (ActivityCompat.shouldShowRequestPermissionRationale(Page_Join.this, android.Manifest.permission.CAMERA)) {

                            // ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                            // 사용자가 임의로 권한을 취소
                            // 다이어로그같은것을 띄워서 사용자에게 해당 권한이 필요한 이유에 대해 설명합니다
                            // 해당 설명이 끝난뒤 requestPermissions()함수를 호출하여 권한허가를 요청해야 합니다
                            ActivityCompat.requestPermissions(Page_Join.this,
                                    new String[]{android.Manifest.permission.CAMERA},
                                    PERMISSIONS_REQUEST_CAMERA);
                        } else {
                            // 최초 권한 요청
                            ActivityCompat.requestPermissions(Page_Join.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    PERMISSIONS_REQUEST_CAMERA);
                            // 필요한 권한과 요청 코드를 넣어서 권한허가요청에 대한 결과를 받아야 합니다
                        }
                    }else{
                        // 권한이 존재하면
                        doTakePhotoAction();
                    }
                } else if (items[which].equals("앨범선택")) {
                    if(ContextCompat.checkSelfPermission(Page_Join.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        if (ActivityCompat.shouldShowRequestPermissionRationale(Page_Join.this,Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            // ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                            // 사용자가 임의로 권한을 취소
                            // 다이어로그같은것을 띄워서 사용자에게 해당 권한이 필요한 이유에 대해 설명합니다
                            // 해당 설명이 끝난뒤 requestPermissions()함수를 호출하여 권한허가를 요청해야 합니다
                            ActivityCompat.requestPermissions(Page_Join.this,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE);
                        } else {
                            // 최초 권한 요청
                            ActivityCompat.requestPermissions(Page_Join.this,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE);
                            // 필요한 권한과 요청 코드를 넣어서 권한허가요청에 대한 결과를 받아야 합니다
                        }
                    }else{
                        // 권한이 존재하면
                        doTakeAlbumAction();
                    }

                } else
                    dialog.dismiss();
            }
        });
        dialog.show();
    }

    // 카메라에서 가져오기
    private void doTakePhotoAction() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // 임시로 사용할 파일의 경로를 생성
       // String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";

        //mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));


        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, outputFileUri);

        // 특정기기에서 사진을 저장못하는 문제가 있어 다음을 주석처리 합니다.
        //intent.putExtra("return-data", true);
        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    // 앨범에서 가져오기
    private void doTakeAlbumAction() {
        // 앨범 호출
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case CROP_FROM_CAMERA: {
                // 크롭이 된 이후의 이미지를 넘겨 받습니다.
                // 이미지뷰에 이미지를 보여준다거나 부가적인 작업 이후에
                // 임시 파일을 삭제합니다.
                Log.e("td"," "+  outputFileUri.toString());
                final Bundle extras = data.getExtras();
                Drawable tmp1, tmp2, tmp3, temp;
                Bitmap tmpbitmap1, tmpbitmap2, tmpbitmap3, tempBitmap;
                if (extras != null) {
                    Bitmap photo = extras.getParcelable("data");

//                    tmp1 = photo_1.getBackground();
//                    tmp2 = photo_2.getBackground();
//                    tmp3 = photo_3.getBackground();
//
//                    tmpbitmap1 = ((BitmapDrawable) tmp1).getBitmap();
//                    tmpbitmap2 = ((BitmapDrawable) tmp2).getBitmap();
//                    tmpbitmap3 = ((BitmapDrawable) tmp3).getBitmap();
//
//                    temp = getApplicationContext().getResources().getDrawable(R.drawable.join_add_photo1);
//                    tempBitmap = ((BitmapDrawable) temp).getBitmap();



//                    tmpbitmap1.equals(tempBitmap) && tmpbitmap2.equals(tempBitmap)
//                            && tmpbitmap3.equals(tempBitmap)

                    if (sel_photo == 1) {
                        photo_1.setImageBitmap(photo, 1);
                        photo_1.setBackgroundResource(R.drawable.white);
                        //img[0] = photo;
                        set_photo[0] = true;
                        photouri[0] = outputFileUri;
                    } else if (sel_photo == 2) {
                        photo_2.setImageBitmap(photo, 1);
                        photo_2.setBackgroundResource(R.drawable.white);
                        //img[1] = photo;
                        set_photo[1] = true;
                       photouri[1] = outputFileUri;
                    } else if (sel_photo == 3) {
                        photo_3.setImageBitmap(photo, 1);
                        photo_3.setBackgroundResource(R.drawable.white);
                        //img[2] = photo;
                        set_photo[2] = true;
                       photouri[2] = outputFileUri;
                    }
                }



                break;
            }

            case PICK_FROM_ALBUM: {
                // 이후의 처리가 카메라와 같으므로 일단  break없이 진행합니다.
                // 실제 코드에서는 좀더 합리적인 방법을 선택하시기 바랍니다.
                mImageCaptureUri = data.getData();

                Intent intent = new Intent("com.android.camera.action.CROP");
                intent.setDataAndType(mImageCaptureUri, "image/*");

                intent.putExtra("outputX", 400);
                intent.putExtra("outputY", 400);
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("scale", true);
                intent.putExtra("return-data", true);

                intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, outputFileUri);


                startActivityForResult(intent, CROP_FROM_CAMERA);

                break;

            }

            case PICK_FROM_CAMERA: {
                // 이미지를 가져온 이후의 리사이즈할 이미지 크기를 결정합니다.
                // 이후에 이미지 크롭 어플리케이션을 호출하게 됩니다.

                Intent intent = new Intent("com.android.camera.action.CROP");
                intent.setDataAndType(outputFileUri, "image/*");

                intent.putExtra("outputX", 400);
                intent.putExtra("outputY", 400);
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("scale", true);
                intent.putExtra("return-data", true);

                intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, outputFileUri);

                startActivityForResult(intent, CROP_FROM_CAMERA);

                break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSIONS_REQUEST_CAMERA :
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // 권한 허가
                    // 해당 권한을 사용해서 작업을 진행할 수 있습니다
                    doTakePhotoAction();
                } else {
                    // 권한 거부
                    // 사용자가 해당권한을 거부했을때 해주어야 할 동작을 수행합니다
                    custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_warning), "동의해주셔야 이용이 가능합니다. ", "확인","dismiss");
                    custom_dial.show();
                }
                return;
            case PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE:
                for(int i=0; i<permissions.length; i++){
                    String permi = permissions[i];
                    int result = grantResults[i];

                    if (permi.equals(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if(result == PackageManager.PERMISSION_GRANTED){
                            doTakeAlbumAction();
                        }else{

                        }
                    } else {
                        // 권한 거부
                        // 사용자가 해당권한을 거부했을때 해주어야 할 동작을 수행합니다
                        custom_dial = new Util_Dialog(Page_Join.this, getResources().getDrawable(R.drawable.icon_warning), "동의해주셔야 이용이 가능합니다. ", "확인","dismiss");
                        custom_dial.show();
                    }
                    return;
                }

        }


    }


        // return 값이 true면 오류
        // 오류 종류
        // 1. 존재한다.
        // 2. 오류.
        //
        // return 값이 false면 이상 없다.

    class Check_id_nick extends AsyncTask<String, Void, String> {
        ProgressDialog loading;
        String link="http://175.126.38.139/php/check_id_nick.php";
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = ProgressDialog.show(Page_Join.this, "Please Wait", null, true, true);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loading.dismiss();
                //Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
        }

        private void writeFormField(String fieldName, String fieldValue)  {
            try  {
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"" + fieldName + "\"" + lineEnd);
                dos.writeBytes(lineEnd);
                dos.write(fieldValue.getBytes());
                dos.writeBytes(lineEnd);
            }
            catch(Exception e)
            {
                    //System.out.println("AndroidUploader.writeFormField: got: " + e.getMessage());
                Log.e("ASDf", "AndroidUploader.writeFormField: " + e.getMessage());
            }
        }

        @Override
        protected String doInBackground(String... params) {

            try{

                URL url = new URL(link);
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                dos = new DataOutputStream(conn.getOutputStream());

                writeFormField("id",params[0]);
                writeFormField("nick",params[1]);

                dos.flush();
                dos.close();
                dos = null;


                BufferedReader rd = null;

                rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line1 = rd.readLine();


                return line1;
            } catch(Exception e){
                Log.e("dddddd", new String("Exception: " + e.getMessage()));
                return new String("Exception: " + e.getMessage());
            }

        }

    }


}


