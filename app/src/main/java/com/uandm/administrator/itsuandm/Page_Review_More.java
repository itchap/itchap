package com.uandm.administrator.itsuandm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.tsengvn.typekit.TypekitContextWrapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class Page_Review_More extends AppCompatActivity {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private ArrayList<Page_Review_Item> review_items;
    private Page_Review_Adapter adapter;
    public RecyclerView recyclerview;
    private SwipeRefreshLayout swipe;

    public Util_Dialog dialog;
    private  ImageView back, intro;

    private String json;
    String myJSON = null;
    JSONArray review_list = null;
    private String no = "0" ;

    private static final String TAG_RESULTS = "result";
    private static final String TAG_NICK = "nick";
    private static final String TAG_PHOTO_MAIN = "photo_main";
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_review_more);
        pref = this.getSharedPreferences("login", 0);
        editor = pref.edit();
        back =(ImageView)findViewById(R.id.back_btn_img);
        intro = (ImageView)findViewById(R.id.huhu);
        swipe=(SwipeRefreshLayout)findViewById(R.id.layout);
        Glide.with(this).load("http://175.126.38.139/img/itchap/review_intro.png").asBitmap().into(intro);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

        recyclerview = (RecyclerView) findViewById(R.id.recycler_review);
        recyclerview.setOverScrollMode(View.OVER_SCROLL_NEVER);
        recyclerview.setNestedScrollingEnabled(false);
        LinearLayoutManager layout = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(layout);
        review_items = new ArrayList<>();

        try {
            json = new Util_PHP(Page_Review_More.this).execute( no,"getdata_review", "http://175.126.38.139/php/getdata_review.php").get();
            rendering_review(json);
        }catch (Exception e){
            e.getStackTrace();
        }
        adapter = new Page_Review_Adapter(review_items, Page_Review_More.this);
        recyclerview.setAdapter(adapter);

    }


    protected void rendering_review(String json) {
        try {
            JSONObject jsonObj = new JSONObject(json);
            review_list = jsonObj.getJSONArray(TAG_RESULTS);
            for (int i = 0; i < review_list.length(); i++) {
                JSONObject c = review_list.getJSONObject(i);
                Log.e("review)list.length",""+review_list);
                Log.e("c",""+c);
                Log.e("c",""+c.getString("no")+c.getString("writer")+c.getString("date"));
//        array_push($result,  array('no'=>$row[0],'writer'=>$row[1],'date'=>$row[2],'img'=>$row[3],'content'=>$row[4],'profile'=>$row2[0]));
                review_items.add(new Page_Review_Item(Page_Review_More.this, c.getString("no"),c.getString("writer"),c.getString("date"),c.getString("img"),c.getString("content"),c.getString("profile")));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
