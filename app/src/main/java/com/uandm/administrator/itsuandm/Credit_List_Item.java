package com.uandm.administrator.itsuandm;

import android.content.Context;

/**
 * Created by u on 2017-02-17.
 */
public class Credit_List_Item {

    //
    String p_m;

    String total;
    String date;
    String type;
    Context context;


    public Credit_List_Item(Context context, String p_m, String total, String type, String date) {
        this.context = context;
        this.p_m = p_m;
        this.total = total;
        this.date = date;
        this.type = type;
    }
    public String getP_m() {
        return p_m;
    }

    public void setP_m(String p_m) {
        this.p_m = p_m;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

}