package com.uandm.administrator.itsuandm;

import android.util.Log;

import java.io.Serializable;
import java.util.StringTokenizer;

/**
 * Created by u on 2017-01-04.
 */
public class Struct_Filter implements Serializable {
    boolean team_count[];
    int start_age;
    int end_age;
    String loc[]=null;
    String date[];

    public String getTeam_count() {
        String str="";
        if(!team_count[0] && !team_count[1] && !team_count[2] && !team_count[3]){
            team_count[0] = true;
            team_count[1] = true;
            team_count[2] = true;
            team_count[3] = true;
        }
        for(int i=0; i<team_count.length; i++){
            if(i == team_count.length-1){
                str += team_count[i];
            }else
                str += team_count[i]+"/";
        }
        return str;
    }

    public void setTeam_count(boolean[] team_count) {
        this.team_count = team_count;
    }

    public int getStart_age() {
        return start_age;
    }

    public void setStart_age(int start_age) {
        this.start_age = start_age;
    }

    public int getEnd_age() {
        return end_age;
    }

    public void setEnd_age(int end_age) {
        this.end_age = end_age;
    }

    public String getLoc() {
        String str="";
        if(this.loc == null){
            return str;
        }else {
            for (int i = 0; i < loc.length; i++) {
                if (i == loc.length - 1) {
                    str += loc[i];
                } else
                    str += loc[i] + "/";
            }
            return str;
        }


    }

    public void setLoc(String loc) {

        int count = 0;
        String tmp[] = loc.split("/");
        this.loc = new String[tmp.length];
        for (int i = 0; i < tmp.length; i++) {
            this.loc[i] = tmp[i];
            Log.e("filter",this.loc[i]);
        }

    }

    public String getDate() {
        String str="";
        for(int i=0; i<date.length; i++){
            if(i == date.length-1){
                str += date[i];
            }else
                str += date[i]+"/";
        }
        return str;
    }

    public void setDate(String date) {
        String tmp[] = date.split(" ");
        this.date = new String[tmp.length];
        for(int i=0; i<tmp.length; i++){
            this.date[i] = tmp[i];
        }



    }

    Struct_Filter() {

    }

    void print() {
        System.out.println("test struct " + start_age + " " +end_age + " "+date[0]+ " " + date[1]+" "+ loc[0]+ " " + loc[1]+ team_count[0]+ " "+team_count[1]);
    }



}