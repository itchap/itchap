package com.uandm.administrator.itsuandm;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.*;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.*;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.LogTime;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import com.tsengvn.typekit.TypekitContextWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeoutException;


import android.os.Bundle;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

public class A_Status extends AppCompatActivity {

    ////
    private ViewPager viewpager;
    private RelativeLayout send_tab, receive_tab;
    private ImageView under_1, under_2;
    private RelativeLayout back;
    private String type;

    TextView[] title = new TextView[2];
    ImageView[] under = new ImageView[2];

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.l_status);

        Intent intent = getIntent();
        type = intent.getStringExtra("type");
        Log.e("type3", "" + type);
        Findid();

        viewpager = (ViewPager) findViewById(R.id.view_pager1);
        VIewPager_Adapter viewpagerAdapter = new VIewPager_Adapter(getSupportFragmentManager(), "status", type);
        viewpager.setAdapter(viewpagerAdapter);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    title[0] = (TextView) findViewById(R.id.btn_1);
                    title[0].setTextColor(getResources().getColor(R.color.app_red));
                    under[0] = (ImageView) findViewById(R.id.underline_1);
                    under[0].setBackgroundColor(getResources().getColor(R.color.app_red));

                    title[1] = (TextView) findViewById(R.id.btn_2);
                    title[1].setTextColor(getResources().getColor(R.color.soft_grey));
                    under[1] = (ImageView) findViewById(R.id.underline_2);
                    under[1].setBackgroundColor(getResources().getColor(R.color.soft_grey));

                } else if (position == 1) {
                    title[1] = (TextView) findViewById(R.id.btn_2);
                    title[1].setTextColor(getResources().getColor(R.color.app_red));
                    under[1] = (ImageView) findViewById(R.id.underline_2);
                    under[1].setBackgroundColor(getResources().getColor(R.color.app_red));

                    title[0] = (TextView) findViewById(R.id.btn_1);
                    title[0].setTextColor(getResources().getColor(R.color.soft_grey));
                    under[0] = (ImageView) findViewById(R.id.underline_1);
                    under[0].setBackgroundColor(getResources().getColor(R.color.soft_grey));
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    void Findid() {
        viewpager = (ViewPager) findViewById(R.id.view_pager1);

        send_tab = (RelativeLayout) findViewById(R.id.send_tab_layout);
        send_tab.setOnClickListener(btnListener);
        receive_tab = (RelativeLayout) findViewById(R.id.receive_tab_layout);
        receive_tab.setOnClickListener(btnListener);


        back = (RelativeLayout) findViewById(R.id.status_back_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }


    private void setCurrentInflateItem(int type) {
        if (type == 0) {
            viewpager.setCurrentItem(0);

        } else if (type == 1) {
            viewpager.setCurrentItem(1);
        }
    }


    View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.send_tab_layout:
                    title[0] = (TextView) findViewById(R.id.btn_1);
                    title[0].setTextColor(getResources().getColor(R.color.app_red));
                    under[0] = (ImageView) findViewById(R.id.underline_1);
                    under[0].setBackgroundColor(getResources().getColor(R.color.app_red));

                    title[1] = (TextView) findViewById(R.id.btn_2);
                    title[1].setTextColor(getResources().getColor(R.color.soft_grey));
                    under[1] = (ImageView) findViewById(R.id.underline_2);
                    under[1].setBackgroundColor(getResources().getColor(R.color.soft_grey));

                    setCurrentInflateItem(0);
                    break;

                case R.id.receive_tab_layout:
                    title[1] = (TextView) findViewById(R.id.btn_2);
                    title[1].setTextColor(getResources().getColor(R.color.app_red));
                    under[1] = (ImageView) findViewById(R.id.underline_2);
                    under[1].setBackgroundColor(getResources().getColor(R.color.app_red));

                    title[0] = (TextView) findViewById(R.id.btn_1);
                    title[0].setTextColor(getResources().getColor(R.color.soft_grey));
                    under[0] = (ImageView) findViewById(R.id.underline_1);
                    under[0].setBackgroundColor(getResources().getColor(R.color.soft_grey));
                    setCurrentInflateItem(1);
                    break;
            }
        }
    };


}




