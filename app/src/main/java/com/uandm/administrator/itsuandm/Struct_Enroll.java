package com.uandm.administrator.itsuandm;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by u on 2017-01-04.
 */
public class Struct_Enroll implements Serializable {

    int hour, minute;
    int month;
    int day;
    String week;
    int max_no;
    String loc1;
    String store_id;
    String name;
    // 개인 0 // 팀 1

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    int type = 0;

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLoc1() {
        return loc1;
    }

    public void setLoc1(String loc1) {
        this.loc1 = loc1;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }


    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public int getMax_no() {
        return max_no;
    }

    public void setMax_no(int max_no) {
        this.max_no = max_no;
    }

    boolean check[] = new boolean[4];

    Struct_Enroll() {
        for (int i = 0; i < check.length; i++)
            check[i] = false;
    }

    void print() {
        System.out.println("test struct " + month+ day + week +hour + minute + max_no + loc1  +"    "+ type);
    }



}
