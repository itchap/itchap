package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

/**
 * Created by young on 2016-11-22.
 */

public class Team_info_ListItem {
    String age;
    String job;
    String height;
    String weight;
    String like[];
    String nick;
    String img[];
    int team_count;


    Team_info_ListItem(Context context, String nick, String age, String job, String height, String weight, String like[],  String img[], int team_count){


        this.nick = nick;
        this.age = age;
        this.job = job;
        this.height = height;
        this.weight = weight;
        this.like = like;
        this.img = img;
        this.team_count = team_count;


    }


    public String[] getLike() {
        return like;
    }

    public void setLike(String[] like) {
        this.like = like;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }



    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String[] getImg() {
        return img;
    }

    public void setImg(String[] img) {
        this.img = img;
    }

    public int getTeam_count() {
        return team_count;
    }

    public void setTeam_count(int team_count) {
        this.team_count = team_count;
    }



}

