package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

/**
 * Created by young on 2016-11-22.
 */

public class Util_Date_Item {

    String intweek;
    int day;
    int type;
    int month;
    String full_date;
    String status;
    String type_p_g;

    Util_Date_Item(String intweek, int day, int month,int type){
        this.intweek = intweek;
        this.day = day;
        this.type =type;
        this.month = month;
    }
    Util_Date_Item(String type_p_g, int day,String full_date, String status,int type){
        this.type =type;
        this.day = day;
        this.full_date = full_date;
        this.status = status;
        this.type_p_g = type_p_g;
    }

    Util_Date_Item( int day, int type){
        this.type =type;
        this.day = day;
    }


    public String getFull_date() {
        return full_date;
    }

    public void setFull_date(String full_date) {
        this.full_date = full_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType_p_g() {
        return type_p_g;
    }

    public void setType_p_g(String type_p_g) {
        this.type_p_g = type_p_g;
    }


    public String getIntweek() {
        return intweek;
    }

    public void setIntweek(String intweek) {
        this.intweek = intweek;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public String getDay() {
        return Integer.toString(day);
    }

    public void setDay(int day) {
        this.day = day;
    }



    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

}

