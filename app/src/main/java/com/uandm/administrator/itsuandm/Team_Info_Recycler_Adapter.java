package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by young on 2016-11-22.
 */

public class Team_Info_Recycler_Adapter extends RecyclerView.Adapter<Team_Info_Recycler_Adapter.ViewHolder> {

    private ArrayList<Team_info_ListItem> listViewItemList;
    private Context context;


    public Team_Info_Recycler_Adapter(ArrayList<Team_info_ListItem> item, Context context) {
        this.context = context;
        this.listViewItemList = item;

    }

    /**
     * 레이아웃을 만들어서 Holer에 저장
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_team_info_item, viewGroup, false);

        return new ViewHolder(view);


    }


    /**
     * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);
        Log.e("onBindViewHolder", "" + "??");
        ViewGroup.LayoutParams params_ = viewHolder.viewPager.getLayoutParams();
        params_.width = viewHolder.width2;
        params_.height = viewHolder.width2;
        viewHolder.viewPager.setLayoutParams(params_);
        viewHolder.adapter = new Team_Info_Pager_Adapter(context, listViewItemList.get(position).getImg());
        viewHolder.viewPager.setCurrentItem(0);
        viewHolder.viewPager.setAdapter(viewHolder.adapter);

        viewHolder.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.e("onPageScrolled", "" + position + "/" + positionOffset + "/" + positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                Log.e("onPageSelected", "" + position);
                for (int i = 0; i < 3; i++) {
                    viewHolder.dots[i].setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_round));
                }
                viewHolder.dots[position].setImageDrawable(context.getResources().getDrawable(R.drawable.selected_round));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.e("onPageScrollStateed", "" + state);
            }
        });
        if (viewHolder.ini_pager == false) {
            //초기셋팅
            int dotsCount = 3;
            viewHolder.dots = new ImageView[dotsCount];

            for (int i = 0; i < dotsCount; i++) {
                viewHolder.dots[i] = new ImageView(context);
                viewHolder.dots[i].setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_round));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                params.setMargins(4, 0, 4, 0);

                viewHolder.indicator.addView(viewHolder.dots[i], params);
            }
            viewHolder.dots[0].setImageDrawable(context.getResources().getDrawable(R.drawable.selected_round));
            viewHolder.ini_pager = true;
        } else {

        }


        viewHolder.nick.setText(listViewItemList.get(position).getNick());
        viewHolder.age.setText(listViewItemList.get(position).getAge());
        viewHolder.height.setText(listViewItemList.get(position).getHeight());
        viewHolder.weight.setText(listViewItemList.get(position).getWeight());
        viewHolder.like.setText(listViewItemList.get(position).getLike()[0] + "," + listViewItemList.get(position).getLike()[1] + "," + listViewItemList.get(position).getLike()[2]);
        viewHolder.job.setText(listViewItemList.get(position).getJob());

    }


    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }

    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        protected ViewPager viewPager;
        protected TextView nick, age, job, weight, height, like;
        protected LinearLayout indicator;
        protected RelativeLayout indicator_lay;
        protected Team_Info_Pager_Adapter adapter;
        protected ImageView[] dots;
        protected int dpi;
        protected DisplayMetrics dm;
        protected int width2;
        protected int height2;
        protected boolean ini_pager = false;

        public ViewHolder(View itemView) {
            super(itemView);
            Log.e("ViewHolder", "" + "??");
            indicator = (LinearLayout) itemView.findViewById(R.id.viewPagerCountDots);
            nick = (TextView) itemView.findViewById(R.id.team_info_nick);
            age = (TextView) itemView.findViewById(R.id.team_info_age);
            job = (TextView) itemView.findViewById(R.id.team_info_job);
            weight = (TextView) itemView.findViewById(R.id.team_info_weight);
            height = (TextView) itemView.findViewById(R.id.team_info_height);
            like = (TextView) itemView.findViewById(R.id.team_info_like);
            viewPager = (ViewPager) itemView.findViewById(R.id.team_info_img_viewpager);


            dm = context.getApplicationContext().getResources().getDisplayMetrics();
            width2 = dm.widthPixels;
            height2 = dm.heightPixels;
            dpi = context.getResources().getDisplayMetrics().densityDpi;

        }
    }

}
