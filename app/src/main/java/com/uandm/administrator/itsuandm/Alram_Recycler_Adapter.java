package com.uandm.administrator.itsuandm;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import java.util.ArrayList;

/**
 * Created by young on 2016-11-22.
 */

public class Alram_Recycler_Adapter extends RecyclerView.Adapter<Alram_Recycler_Adapter.ViewHolder> {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList//
    private ArrayList<Alram_ListItem> listViewItemList;
    private Context context;
    public static final int ITEM_TYPE_ONE = 1;
    public static final int ITEM_TYPE_TWO = 2;
    public static final int ITEM_TYPE_THREE = 3;
    public static final int ITEM_TYPE_FOUR = 4;
    //
    private Util_Dialog dialog;
    private RequestManager requestManager;

    int type;

    public Alram_Recycler_Adapter(ArrayList<Alram_ListItem> item, Context context,  RequestManager requestManager) {
        this.context = context;
        this.listViewItemList = item;
        this.requestManager = requestManager;
    }

    @Override
    public int getItemViewType(int position) {

        if (listViewItemList.get(position).getImg_type() == 1) {
            this.type = 1;
            return ITEM_TYPE_ONE;
        } else if (listViewItemList.get(position).getImg_type() == 2) {
            this.type = 2;
            return ITEM_TYPE_TWO;
        } else if (listViewItemList.get(position).getImg_type() == 3) {
            this.type = 3;
            return ITEM_TYPE_THREE;
        } else {
            this.type = 4;
            return ITEM_TYPE_FOUR;
        }


    }

    /**
     * 레이아웃을 만들어서 Holer에 저장
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view;


        if (viewType == ITEM_TYPE_ONE) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_alram_item_1, viewGroup, false);
            return new ViewHolder(view, ITEM_TYPE_ONE);
        } else if (viewType == ITEM_TYPE_TWO) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_alram_item_2, viewGroup, false);
            return new ViewHolder(view, ITEM_TYPE_TWO);
        } else if (viewType == ITEM_TYPE_THREE) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_alram_item_3, viewGroup, false);
            return new ViewHolder(view, ITEM_TYPE_THREE);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.l_alram_item_4, viewGroup, false);
            return new ViewHolder(view, ITEM_TYPE_FOUR);
        }

    }


    /**
     * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final int itemtype = getItemViewType(position);

        if (itemtype == ITEM_TYPE_ONE) {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
        } else if (itemtype == ITEM_TYPE_TWO) {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
        } else if (itemtype == ITEM_TYPE_THREE) {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
            requestManager.load(listViewItemList.get(position).getImg()[2]).asBitmap().into(viewHolder.iv_3);

        } else {
            requestManager.load(listViewItemList.get(position).getImg()[0]).asBitmap().into(viewHolder.iv_1);
            requestManager.load(listViewItemList.get(position).getImg()[1]).asBitmap().into(viewHolder.iv_2);
            requestManager.load(listViewItemList.get(position).getImg()[2]).asBitmap().into(viewHolder.iv_3);
            requestManager.load(listViewItemList.get(position).getImg()[3]).asBitmap().into(viewHolder.iv_4);
        }
        viewHolder.time.setText(listViewItemList.get(position).getTime());
        viewHolder.icon.setBackground(listViewItemList.get(position).getIcon());
        viewHolder.cmt.setText(listViewItemList.get(position).getCmt());

        //알람 클릭@@
        viewHolder.bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listViewItemList.get(position).getAlram_status().equals("0") && listViewItemList.get(position).getAlram_type().equals("0")) {
                    dialog = new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_lastpage), listViewItemList.get(position).getSender_nick() + "님의 팀 요청을\n 수락하시겠습니까?", "거절", "수락", "team_waiting");
                    dialog.put_data = listViewItemList.get(position).getSender_nick();
                    dialog.show();
                } else if (listViewItemList.get(position).getAlram_status().equals("1") && listViewItemList.get(position).getAlram_type().equals("0")) {
                    dialog = new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_lastpage), "이미 수락/거절을 하셨습니다.", "확인", "dismiss");
                    dialog.show();
                }
                else if (listViewItemList.get(position).getAlram_status().equals("1") && listViewItemList.get(position).getAlram_type().equals("0")) {
                    dialog = new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_lastpage), "이미 수락/거절을 하셨습니다.", "확인", "dismiss");
                    dialog.show();
                }
                else if (listViewItemList.get(position).getAlram_status().equals("1") && listViewItemList.get(position).getAlram_type().equals("0")) {
                    dialog = new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_lastpage), "이미 수락/거절을 하셨습니다.", "확인", "dismiss");
                    dialog.show();
                }




                else if (listViewItemList.get(position).getAlram_type().equals("10") && listViewItemList.get(position).getImg_type()==1) {
                    Intent to_status = new Intent(context, Page_Main.class);
                    to_status.putExtra("type","personal");
                    to_status.putExtra("from","alram_status");
                    to_status.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    to_status.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(to_status);
                } else if  (listViewItemList.get(position).getAlram_type().equals("10") && listViewItemList.get(position).getImg_type()>1) {
                    Intent to_status = new Intent(context, Page_Main.class);
                    to_status.putExtra("type","group");
                    to_status.putExtra("from","alram_status");
                    to_status.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    to_status.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(to_status);



                }else if (listViewItemList.get(position).getAlram_type().equals("55")&& listViewItemList.get(position).getAlram_status().equals("0")) {
                    Intent to_review = new Intent(context, Page_Review.class);
                    to_review.putExtra("alram_id",listViewItemList.get(position).getAlram_id());
                    context.startActivity(to_review);
                }else if (listViewItemList.get(position).getAlram_type().equals("55")&& listViewItemList.get(position).getAlram_status().equals("1")) {
                    dialog = new Util_Dialog(context, context.getResources().getDrawable(R.drawable.icon_lastpage), "이미 후기를 남기셨습니다", "확인", "dismiss");
                    dialog.show();
                }
                else if (listViewItemList.get(position).getAlram_type().equals("11") || listViewItemList.get(position).getAlram_type().equals("12")|| listViewItemList.get(position).getAlram_type().equals("13")) {
                    Intent intent1 = new Intent(context, Page_Main.class);
                    intent1.putExtra("from","alram");
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent1);
                }





            }
        });
//
    }


    @Override
    public int getItemCount() {
        return (null != listViewItemList ? listViewItemList.size() : 0);
    }


    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView icon;
        protected TextView time, cmt;
        protected Util_ImageView iv_1, iv_2, iv_3, iv_4;
        protected RelativeLayout bg;
        protected String sender_nick;


        public ViewHolder(View itemView, int type) {
            super(itemView);
                if (type == 1) {
                    iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                    iv_1.setType(1);
                    iv_2 = null;
                    iv_3 = null;
                    iv_4 = null;
                } else if (type == 2) {
                    iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                    iv_1.setType(2);
                    iv_2 = (Util_ImageView) itemView.findViewById(R.id.img2);
                    iv_2.setType(3);
                    iv_3 = null;
                    iv_4 = null;
                } else if (type == 3) {
                    iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                    iv_1.setType(4);
                    iv_2 = (Util_ImageView) itemView.findViewById(R.id.img2);
                    iv_2.setType(5);
                    iv_3 = (Util_ImageView) itemView.findViewById(R.id.img3);
                    iv_3.setType(6);
                    iv_4 = null;
                } else {
                    iv_1 = (Util_ImageView) itemView.findViewById(R.id.img1);
                    iv_1.setType(7);
                    iv_2 = (Util_ImageView) itemView.findViewById(R.id.img2);
                    iv_2.setType(9);
                    iv_3 = (Util_ImageView) itemView.findViewById(R.id.img3);
                    iv_3.setType(8);
                    iv_4 = (Util_ImageView) itemView.findViewById(R.id.img4);
                    iv_4.setType(10);
                }

            time = (TextView) itemView.findViewById(R.id.alram_time);
            cmt = (TextView) itemView.findViewById(R.id.alram_msg);
            icon = (ImageView) itemView.findViewById(R.id.alram_icon);
            bg = (RelativeLayout) itemView.findViewById(R.id.bg);

        }
    }

}

