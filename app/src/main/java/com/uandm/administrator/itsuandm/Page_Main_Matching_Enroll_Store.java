package com.uandm.administrator.itsuandm;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;

import com.tsengvn.typekit.TypekitContextWrapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;


import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;


import java.io.BufferedReader;
import java.io.InputStreamReader;


public class Page_Main_Matching_Enroll_Store extends AppCompatActivity {
    private ScrollView scrollView;
    private RecyclerView list_super, list_normal;
    private LinearLayout super_title, normal_title;
    private RelativeLayout no_list;

    private Enroll_Store_RecyclerAdapter adapter_super, adapter_normal;
    private ArrayList<Enroll_Store_Item> store_item_super, store_item_normal;
    private ArrayList<String> loc_1, loc_22;
    private Spinner spin_a, spin_b;
    private String post_date;

    private  RelativeLayout back;
    private Struct_Enroll struct_enroll;

    private String json, location, location_si;
    JSONArray jsonArray = null;
    JSONArray locations = null;

    private int int_scrollViewPos, int_TextView_lines;
    private int index = 0;
    private String post_loc_1 = "전체", post_loc_2 = "전체";

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.l_main_matching_enroll_store);
        spin_a = (Spinner) findViewById(R.id.spinner_a);
        spin_b = (Spinner) findViewById(R.id.spinner_b);

        Intent intent = getIntent();
        struct_enroll = (Struct_Enroll) intent.getSerializableExtra("Struct_Enroll");

        scrollView = (ScrollView) findViewById(R.id.main_scroll);
        list_super = (RecyclerView) findViewById(R.id.store_recycle_super);
        list_normal = (RecyclerView) findViewById(R.id.store_recycle_normal);
        super_title = (LinearLayout) findViewById(R.id.super_title);
        normal_title = (LinearLayout) findViewById(R.id.normal_title);
        no_list = (RelativeLayout) findViewById(R.id.no_list);
        back = (RelativeLayout)findViewById(R.id.team_list_back_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        store_item_super = new ArrayList<>();
        store_item_normal = new ArrayList<>();
        loc_1 = new ArrayList<>();
        loc_22 = new ArrayList<>();

        adapter_super = new Enroll_Store_RecyclerAdapter(store_item_super, Page_Main_Matching_Enroll_Store.this, struct_enroll);
        adapter_normal = new Enroll_Store_RecyclerAdapter(store_item_normal, Page_Main_Matching_Enroll_Store.this, struct_enroll);

        //슈퍼
        list_super.setOverScrollMode(View.OVER_SCROLL_NEVER);
        list_super.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(Page_Main_Matching_Enroll_Store.this, 2);
        list_super.setHasFixedSize(true);
        list_super.setLayoutManager(layoutManager);
        list_super.addItemDecoration(new Page_Main_Matching_Enroll_Store.GridSpacingItemDecoration(2, dpToPx(10), true));
        list_super.setItemAnimator(new DefaultItemAnimator());


        //일반
        list_normal.setOverScrollMode(View.OVER_SCROLL_NEVER);
        list_normal.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager2 = new GridLayoutManager(Page_Main_Matching_Enroll_Store.this, 2);
        list_normal.setHasFixedSize(true);
        list_normal.setLayoutManager(layoutManager2);
        list_normal.addItemDecoration(new Page_Main_Matching_Enroll_Store.GridSpacingItemDecoration(2, dpToPx(10), true));
        list_normal.setItemAnimator(new DefaultItemAnimator());


        try {
            location = new Util_PHP(Page_Main_Matching_Enroll_Store.this).execute("1", "getdata_loction", "http://175.126.38.139/php/getdata_location.php").get();
            post_date = Integer.toString(struct_enroll.getMonth()) + "-" + Integer.toString(struct_enroll.getDay());
            json = new Util_PHP(Page_Main_Matching_Enroll_Store.this).execute(post_loc_1, post_loc_2, Integer.toString(index), "store", "http://175.126.38.139/php/getdata_store.php").get();
        } catch (Exception e) {

        }
        re_location(location);
        rendering(json, 1);


        spin_a.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (spin_a.getSelectedItem().toString().equals("전체")) {
                    loc_22.clear();
                    loc_22.add("전체");
                    post_loc_1 = spin_a.getSelectedItem().toString();
                    post_loc_2 = "전체";
                    ArrayAdapter adapter2 = new ArrayAdapter(Page_Main_Matching_Enroll_Store.this, android.R.layout.simple_spinner_dropdown_item, loc_22);
                    spin_b.setAdapter(adapter2);
                    try {
                        index = 0;
                        json = new Util_PHP(Page_Main_Matching_Enroll_Store.this).execute(post_loc_1, post_loc_2, Integer.toString(index), "store", "http://175.126.38.139/php/getdata_store.php").get();
                        rendering(json, 0);
                    } catch (Exception e) {

                    }
                } else {
                    post_loc_1 = spin_a.getSelectedItem().toString();
                    try {
                        location = new Util_PHP(Page_Main_Matching_Enroll_Store.this).execute(spin_a.getSelectedItem().toString(), "location", "http://175.126.38.139/php/location.php").get();
                        reb_location(location);
                        index = 0;
                        json = new Util_PHP(Page_Main_Matching_Enroll_Store.this).execute(post_loc_1, post_loc_2, Integer.toString(index), "store", "http://175.126.38.139/php/getdata_store.php").get();
                        rendering(json, 0);
                    } catch (Exception e) {

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                post_loc_1 = "전체";
            }

        });

        spin_b.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                post_loc_1 = spin_a.getSelectedItem().toString();
                post_loc_2 = spin_b.getSelectedItem().toString();
                if (spin_b.getSelectedItem().toString().equals("전체")) {
                    post_loc_2=spin_b.getSelectedItem().toString();
                    post_loc_1=spin_a.getSelectedItem().toString();
                    try {
                        index = 0;
                        json = new Util_PHP(Page_Main_Matching_Enroll_Store.this).execute(post_loc_1, post_loc_2, Integer.toString(index), "store", "http://175.126.38.139/php/getdata_store.php").get();
                        rendering(json, 0);
                    } catch (Exception e) {

                    }
                } else {
                    try {
                        index = 0;
                        json = new Util_PHP(Page_Main_Matching_Enroll_Store.this).execute(post_loc_1, post_loc_2, Integer.toString(index), "store", "http://175.126.38.139/php/getdata_store.php").get();
                        rendering(json, 0);
                    } catch (Exception e) {

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        Detect Bottom ScrollView
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int_scrollViewPos = scrollView.getScrollY();
                int_TextView_lines = scrollView.getChildAt(0).getBottom() - scrollView.getHeight();
                if (int_TextView_lines == int_scrollViewPos) {
                    try {
                        json = new Util_PHP(Page_Main_Matching_Enroll_Store.this).execute(post_loc_1, post_loc_2, Integer.toString(index), toString(), "store", "http://175.126.38.139/php/getdata_store.php").get();
                        Log.e("store_", "" + json);
                    } catch (Exception e) {
                    }
                    rendering(json, 1);

                }

            }
        });

    }

    protected void re_location(String location) {
        try {
            JSONObject jsonObj = new JSONObject(location);
            locations = jsonObj.getJSONArray("result");
            loc_22.clear();
            loc_22.add("전체");
            for (int i = 0; i < locations.length(); i++) {
                JSONObject c = locations.getJSONObject(i);
                loc_1.add(c.getString("do"));
            }
            List<String> uniqueItems = new ArrayList<String>(new HashSet<String>(loc_1));
            uniqueItems.add(0,"전체");
            ArrayAdapter adapter = new ArrayAdapter(Page_Main_Matching_Enroll_Store.this, android.R.layout.simple_spinner_dropdown_item, uniqueItems);
            ArrayAdapter adapter2 = new ArrayAdapter(Page_Main_Matching_Enroll_Store.this, android.R.layout.simple_spinner_dropdown_item, loc_22);
            spin_a.setAdapter(adapter);
            spin_b.setAdapter(adapter2);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    protected void reb_location(String location) {
        try {
            JSONObject jsonObj = new JSONObject(location);
            locations = jsonObj.getJSONArray("result");
            loc_22.clear();
            loc_22.add("전체");
            for (int i = 0; i < locations.length(); i++) {
                JSONObject c = locations.getJSONObject(i);
                loc_22.add(c.getString("si"));
            }
            ArrayAdapter adapter2 = new ArrayAdapter(Page_Main_Matching_Enroll_Store.this, android.R.layout.simple_spinner_dropdown_item, loc_22);
            spin_b.setAdapter(adapter2);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column
            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }


    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    protected void rendering(String json, int type) {
        if (json.equals("no_list")) {
            no_list.setVisibility(View.VISIBLE);
            super_title.setVisibility(View.GONE);
            normal_title.setVisibility(View.GONE);
            list_normal.setVisibility(View.GONE);
            list_super.setVisibility(View.GONE);
            Log.e("error", "no_store_list");
        } else if (json.equals("no_list_more")) {
            Log.e("error", "아무것도 하지 않는다");
        } else if (json.equals("error")) {
            Log.e("error", "??");
        } else {
            try {
                JSONObject jsonObj2 = new JSONObject(json);
                jsonArray = jsonObj2.getJSONArray("result");
                if (type == 0) {
                    Log.e("typerrr", ""+type);
                    store_item_super.clear();
                    store_item_normal.clear();
                } else {

                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject c = jsonArray.getJSONObject(i);
                    if (c.getString("store_type").equals("1")) {
                        store_item_super.add(new Enroll_Store_Item(Page_Main_Matching_Enroll_Store.this, c.getString("photo_main"), c.getString("id"), c.getString("store_name"), c.getString("loc"), c.getString("store_type"), c.getString("index")));
                    } else {
                        store_item_normal.add(new Enroll_Store_Item(Page_Main_Matching_Enroll_Store.this, c.getString("photo_main"), c.getString("id"), c.getString("store_name"), c.getString("loc"), c.getString("store_type"), c.getString("index")));
                    }
                    index = index + 1;
                    Log.e("###", "" + index);
                }
                if (store_item_super.size() != 0 && store_item_normal.size() != 0) {
                    no_list.setVisibility(View.GONE);
                    list_super.setVisibility(View.VISIBLE);
                    super_title.setVisibility(View.VISIBLE);
                    list_normal.setVisibility(View.VISIBLE);
                    normal_title.setVisibility(View.VISIBLE);
                    list_super.setAdapter(adapter_super);
                    list_normal.setAdapter(adapter_normal);
                } else if (store_item_super.size() == 0 && store_item_normal.size() != 0) {
                    no_list.setVisibility(View.GONE);
                    list_super.setVisibility(View.GONE);
                    super_title.setVisibility(View.GONE);
                    list_normal.setVisibility(View.VISIBLE);
                    normal_title.setVisibility(View.VISIBLE);
                    list_super.setAdapter(adapter_super);
                    list_normal.setAdapter(adapter_normal);
                } else if (store_item_super.size() != 0 && store_item_normal.size() == 0) {
                    no_list.setVisibility(View.GONE);
                    list_super.setVisibility(View.VISIBLE);
                    super_title.setVisibility(View.VISIBLE);
                    list_normal.setVisibility(View.GONE);
                    normal_title.setVisibility(View.GONE);
                    list_super.setAdapter(adapter_super);
                    list_normal.setAdapter(adapter_normal);
                } else {
                    no_list.setVisibility(View.VISIBLE);
                    Log.e("error", "no_store_list");
                }
                adapter_super.notifyDataSetChanged();
                adapter_normal.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

}